﻿using CoreGraphics;
using Foundation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using UIKit;
using Universal.Common.Extensions;
using Universal.iOS.Extensions;
using Universal.iOS.Views;

namespace Universal.iOS.ViewControllers
{
    /// <summary>
    /// A single page <see cref="UIViewController"/> that renders everything on a single page using <see cref="UIViewController"/>s.
    /// </summary>
    public class SinglePageViewController : UIViewController
    {
        /// <summary>
        /// Raised when the active view controller is changed.
        /// The <see cref="NavigationStack" /> may not be in the correct state when this event is raised.
        /// </summary>
        public event EventHandler ActiveViewControllerChanged;
        /// <summary>
        /// Raised when the navigation stack is changed.
        /// </summary>
        public event EventHandler NavigationStackChanged;
        /// <summary>
        /// <see cref="Stack{T}"/> of <see cref="UIViewController"/> that contains the navigation history and the active <see cref="UIViewController"/> on top of the stack.
        /// </summary>
        public virtual Stack<UIViewController> NavigationStack { get; protected set; }
        private UIViewController mActiveViewController;
        /// <summary>
        /// The current <see cref="UIViewController"/> being displayed.
        /// </summary>
        public virtual UIViewController ActiveViewController
        {
            get => mActiveViewController;
            set
            {
                if (mActiveViewController != value)
                {
                    UIViewController oldViewController = mActiveViewController;
                    mActiveViewController = value;
                    OnActiveViewControllerChanged(oldViewController, value);
                }
            }
        }
        protected UIView ActiveViewContainer { get; set; }
        protected bool InitialViewControllerSet { get; set; }

        public override void ViewDidLoad()
        {
            base.ViewDidLoad();

            InitialViewControllerSet = false;
            ActiveViewContainer = FindActiveViewContainer();
        }

        public override void ViewDidAppear(bool animated)
        {
            View.RemoveAllConstraints();
            View.TranslatesAutoresizingMaskIntoConstraints = false;
            View.LeadingAnchor.ConstraintEqualTo(UIApplication.SharedApplication.KeyWindow.LeadingAnchor).Active = true;
            View.TrailingAnchor.ConstraintEqualTo(UIApplication.SharedApplication.KeyWindow.TrailingAnchor).Active = true;
            View.TopAnchor.ConstraintEqualTo(UIApplication.SharedApplication.KeyWindow.TopAnchor).Active = true;
            View.BottomAnchor.ConstraintEqualTo(UIApplication.SharedApplication.KeyWindow.BottomAnchor).Active = true;

            if (View.GetLayoutParameters() == null)
            {
                View.SetLayoutParameters(new ViewGroup.LayoutParameters(ViewGroup.LayoutParameters.MatchParent, ViewGroup.LayoutParameters.MatchParent));
            }

            if (!InitialViewControllerSet)
            {
                InitialViewControllerSet = true;
                UIViewController initialViewController = CreateInitialViewController();
                if (initialViewController != null)
                {
                    SetActiveViewController(initialViewController);
                }
            }
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="SinglePageViewController"/> class.
        /// </summary>
        public SinglePageViewController() : base() { Initialize(); }
        /// <summary>
        /// Initializes a new instance of the <see cref="SinglePageViewController"/> class.
        /// </summary>
        public SinglePageViewController(IntPtr handle) : base(handle) { Initialize(); }
        /// <summary>
        /// Initializes a new instance of the <see cref="SinglePageViewController"/> class.
        /// </summary>
        public SinglePageViewController(NSCoder coder) : base(coder) { Initialize(); }
        /// <summary>
        /// Initializes a new instance of the <see cref="SinglePageViewController"/> class.
        /// </summary>
        public SinglePageViewController(NSObjectFlag t) : base(t) { Initialize(); }
        /// <summary>
        /// Initializes a new instance of the <see cref="SinglePageViewController"/> class.
        /// </summary>
        public SinglePageViewController(string nibName, NSBundle bundle) : base(nibName, bundle) { Initialize(); }

        private void Initialize()
        {
            NavigationStack = new Stack<UIViewController>();
        }

        /// <summary>
        /// Override this to provide an initial <see cref="UIViewController"/> for the <see cref="SinglePageViewController"/>.
        /// </summary>
        protected virtual UIViewController CreateInitialViewController()
        {
            return null;
        }

        /// <summary>
        /// Override this to return a new <see cref="UIView"/> which will be the active view's container.
        /// </summary>
        /// <returns></returns>
        protected virtual UIView FindActiveViewContainer()
        {
            return View;
        }

        protected virtual void WillSetActiveViewController(UIViewController viewController)
        {
        }

        protected virtual void SetActiveViewControllerInternal(UIViewController viewController)
        {
            WillSetActiveViewController(viewController);
            // See https://developer.apple.com/library/archive/featuredarticles/ViewControllerPGforiPhoneOS/ImplementingaContainerViewController.html#//apple_ref/doc/uid/TP40007457-CH11-SW1
            AddChildViewController(viewController);
            viewController.View.Frame = new CGRect(viewController.View.Frame.Location, ActiveViewContainer.Frame.Size);
            ActiveViewContainer.AddSubview(viewController.View);
            DidSetActiveViewController(viewController);
            ActiveViewController = viewController;
        }

        protected virtual void DidSetActiveViewController(UIViewController viewController)
        {
            // This causes the child's method to fire twice, despite this being the documented way of doing things.
            viewController.DidMoveToParentViewController(this);
        }

        /// <summary>
        /// Set the active <see cref="UIViewController"/>, adding it on top of the navigation stack and calling appropriate lifecycle methods.
        /// </summary>
        /// <param name="viewController"></param>
        public virtual void SetActiveViewController(UIViewController viewController)
        {
            RemoveActiveViewController();
            SetActiveViewControllerInternal(viewController);
            NavigationStack.Push(viewController);
            OnNavigationStackChanged();
        }

        /// <summary>
        /// Sets the active <see cref="UIViewController"/>, attempting to construct it through reflection, adding it on top of the navigation stack and calling appropriate lifecycle methods.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        public virtual void SetActiveViewController<T>()
            where T : UIViewController
        {
            ConstructorInfo[] constructorInfos = typeof(T).GetConstructors();
            if (constructorInfos.Any(x => x.GetParameters().Length == 1 && x.GetParameters().First().ParameterType.IsAssignableFrom(GetType())))
            {
                T viewController = (T)Activator.CreateInstance(typeof(T), this);
                SetActiveViewController(viewController);
            }
            else if (constructorInfos.Any(x => x.GetParameters().Length == 0))
            {
                T viewController = Activator.CreateInstance<T>();
                SetActiveViewController(viewController);
            }
            else
            {
                throw new InvalidOperationException("No valid constructor found for the given class.");
            }
        }

        /// <summary>
        /// Removes the active <see cref="UIViewController"/> from the top of the navigation stack, loading the next <see cref="UIViewController"/> on top of the stack, if any, 
        /// or calls <see cref="OnBackPressed"/> if it is empty.
        /// </summary>
        public virtual void NavigateBack()
        {
            NavigationStack.Pop();
            OnNavigationStackChanged();
            RemoveActiveViewController();
            if (NavigationStack.TryPeek(out UIViewController viewController))
            {
                SetActiveViewControllerInternal(viewController);
            }
            else
            {
                OnExit();
            }
        }

        /// <summary>
        /// Navigates back to a given <see cref="UIViewController"/> instance.
        /// </summary>
        /// <param name="viewController"></param>
        public virtual void NavigateBackTo(UIViewController viewController)
        {
            if (NavigationStack.Skip(1).Contains(viewController))
            {
                while (NavigationStack.Peek(1) != viewController)
                {
                    NavigationStack.Pop();
                    OnNavigationStackChanged();
                }

                NavigateBack();
            }
            else
            {
                throw new InvalidOperationException("The specified view controller is not in the navigation stack history.");
            }
        }

        /// <summary>
        /// Navigates back to a given <see cref="UIViewController"/> instance base on the given type, constructing it and adding it on the stack if necessary.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        public virtual void NavigateBackToOrDefault<T>()
            where T : UIViewController
        {
            if (NavigationStack.Skip(1).Any(x => typeof(T).IsAssignableFrom(x.GetType())))
            {
                while (!typeof(T).IsAssignableFrom(NavigationStack.Peek(1).GetType()))
                {
                    NavigationStack.Pop();
                    OnNavigationStackChanged();
                }

                NavigateBack();
            }
            else
            {
                SetActiveViewController<T>();
            }
        }

        /// <summary>
        /// Override this to provide an action to perform when an on-screen back button is pressed.
        /// </summary>
        public virtual void OnBackPressed()
        {
            NavigateBack();
        }

        /// <summary>
        /// Override this to provide an action to perform when a user performs an action that would remove the last view controller from view.
        /// </summary>
        public virtual void OnExit()
        {
        }

        /// <summary>
        /// Removes the active <see cref="UIViewController"/>, if any. Does not remove it from the navigation stack.
        /// </summary>
        protected virtual void RemoveActiveViewController()
        {
            if (ActiveViewController != null)
            {
                ActiveViewController.WillMoveToParentViewController(null);
                ActiveViewController.View.RemoveFromSuperview();
                ActiveViewController.RemoveFromParentViewController();
                ActiveViewController = null;
            }
        }

        public override void ViewDidLayoutSubviews()
        {
            base.ViewDidLayoutSubviews();
            if (ActiveViewController != null)
            {
                ActiveViewController.View.Frame = new CGRect(ActiveViewController.View.Frame.Location, ActiveViewContainer.Frame.Size);
            }
        }


        /// <summary>
        /// Called when the active view controller changes.
        /// </summary>
        protected virtual void OnActiveViewControllerChanged(UIViewController oldViewController, UIViewController newViewController)
        {
            ActiveViewControllerChanged?.Invoke(this, new EventArgs());
        }

        /// <summary>
        /// Called when the navigation stack changes.
        /// </summary>
        protected virtual void OnNavigationStackChanged()
        {
            NavigationStackChanged?.Invoke(this, new EventArgs());
        }
    }
}