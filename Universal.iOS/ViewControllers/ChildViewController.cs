﻿using Foundation;
using System;
using UIKit;
using Universal.Common;

namespace Universal.iOS.ViewControllers
{
    /// <summary>
    /// A <see cref="UIViewController"/> with a strongly-typed <see cref="UIViewController.ParentViewController"/>.
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class ChildViewController<T> : UIViewController
        where T : UIViewController
    {
        public ChildViewController() : base() { }
        public ChildViewController(NSCoder coder) : base(coder) { }
        public ChildViewController(NSObjectFlag t) : base(t) { }
        public ChildViewController(IntPtr handle) : base(handle) { }
        public ChildViewController(string nibName, NSBundle bundle) : base(nibName, bundle) { }

        /// <summary>
        /// Gets the strongly-typed <see cref="UIViewController"/> parent of this <see cref="ChildViewController{T}"/>.
        /// </summary>
        public new virtual T ParentViewController
        {
            get
            {
                if (base.ParentViewController == null)
                {
                    return null;
                }
                else
                {
                    if (base.ParentViewController is T)
                    {
                        return base.ParentViewController as T;
                    }
                    else
                    {
                        throw new TypeMismatchException(typeof(T), base.ParentViewController.GetType());
                    }
                }
            }
        }
    }
}