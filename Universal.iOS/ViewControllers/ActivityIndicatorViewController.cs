﻿using CoreGraphics;
using UIKit;
using Universal.iOS.Widget;

namespace Universal.iOS.ViewControllers
{
    /// <summary>
    /// Simple <see cref="UIViewController" /> that wraps an <see cref="ActivityIndicatorView" />.
    /// </summary>
    public class ActivityIndicatorViewController : UIViewController
    {
        public override void LoadView()
        {
            View = new ActivityIndicatorView(new CGRect(0, 0, 50, 50));
        }
    }
}