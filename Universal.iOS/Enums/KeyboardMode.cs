﻿namespace Universal.iOS
{
    public enum KeyboardMode
    {
        /// <summary>
        /// Do not adjust the view when a keyboard comes into view.
        /// </summary>
        AdjustNone = 0,
        /// <summary>
        /// Pans the entire view up to ensure that they focused input stays in view.
        /// </summary>
        AdjustPan = 1,
        /// <summary>
        /// Resizes the view to match the dimensions remaining.
        /// </summary>
        AdjustResize = 2,
        /// <summary>
        /// Examines constraints on the first responder to determine whether to pan or resize.
        /// </summary>
        AdjustFirstResponder = 4
    };
}