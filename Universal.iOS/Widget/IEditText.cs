﻿namespace Universal.iOS.Widget
{
    /// <summary>
    /// Represents an editable text field.
    /// </summary>
    public interface IEditText
    {
        /// <summary>
        /// Gets or sets the text.
        /// </summary>
        string Text { get; set; }
        /// <summary>
        /// Gets or sets the placeholder text.
        /// </summary>
        string Placeholder { get; set; }
    }
}