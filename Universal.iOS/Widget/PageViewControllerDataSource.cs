﻿using System;
using System.Collections.Generic;
using System.Linq;
using UIKit;
using Universal.Common.Extensions;

namespace Universal.iOS.Widget
{
    /// <summary>
    /// <see cref="UIPageViewControllerDataSource"/> that takes an enumerable sequence of <see cref="UIViewController"/>s.
    /// </summary>
    public class PageViewControllerDataSource : UIPageViewControllerDataSource
    {
        protected IEnumerable<UIViewController> ViewControllers;

        public PageViewControllerDataSource(IEnumerable<UIViewController> viewControllers) : base()
        {
            ViewControllers = viewControllers;
        }

        public override UIViewController GetPreviousViewController(UIPageViewController pageViewController, UIViewController referenceViewController)
        {
            return ViewControllers.ElementAtOrDefault(ViewControllers.IndexOf(referenceViewController) - 1);
        }

        public override UIViewController GetNextViewController(UIPageViewController pageViewController, UIViewController referenceViewController)
        {
            return ViewControllers.ElementAtOrDefault(ViewControllers.IndexOf(referenceViewController) + 1);
        }

        public override nint GetPresentationCount(UIPageViewController pageViewController)
        {
            return ViewControllers.Count();
        }

        public override nint GetPresentationIndex(UIPageViewController pageViewController)
        {
            if (pageViewController.ViewControllers != null && pageViewController.ViewControllers.Any())
            {
                UIViewController presentationViewController = pageViewController.ViewControllers[0];

                int presentationIndex = ViewControllers.IndexOf(presentationViewController);
                if (presentationIndex != -1)
                {
                    return presentationIndex;
                }
            }

            return 0;
        }
    }
}