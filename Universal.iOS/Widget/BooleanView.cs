﻿using CoreGraphics;
using Foundation;
using System;
using System.ComponentModel;
using UIKit;
using Universal.iOS.Extensions;

namespace Universal.iOS.Widget
{
    [Register("BooleanView")]
    [DesignTimeVisible(true)]
    public class BooleanView : UIView
    {
        [Export("requiresConstraintBasedLayout")]
        public new bool RequiresConstraintBasedLayout()
        {
            return true;
        }

        protected bool? mValue;
        public bool? Value
        {
            get => mValue;
            set
            {
                if (value != mValue)
                {
                    mValue = value;

                    RenderButtons();
                    ValueChanged?.Invoke(this, new EventArgs());
                }
            }
        }

        public event EventHandler ValueChanged;

        public BooleanView() { Initialize(); }
        public BooleanView(NSCoder coder) : base(coder) { Initialize(); }
        public BooleanView(IntPtr ptr) : base(ptr) { Initialize(); }
        public BooleanView(CGRect frame) : base(frame) { Initialize(); }
        public BooleanView(NSObjectFlag t) : base(t) { Initialize(); }

        protected bool mConstraintsAdded;
        protected UILabel Label { get; set; }
        protected UIButton ButtonYes { get; set; }
        protected UIButton ButtonNo { get; set; }

        private void Initialize()
        {
            mConstraintsAdded = false;

            Label = new UILabel
            {
                TextColor = UIColor.DarkGray,
                Font = UIFont.SystemFontOfSize(17),
                Lines = 0,
                LineBreakMode = UILineBreakMode.TailTruncation,
                TranslatesAutoresizingMaskIntoConstraints = false
            };

            ButtonYes = new UIButton()
            {
                TranslatesAutoresizingMaskIntoConstraints = false
            };

            ButtonYes.Layer.BorderWidth = 1.0f;
            ButtonYes.Layer.BorderColor = UIColor.DarkGray.CGColor;
            ButtonYes.SetBackgroundImage(UIImage.FromBundle("baseline_check_black_24dp").ImageWithTint(UIColor.DarkGray), UIControlState.Normal);

            ButtonNo = new UIButton()
            {
                TranslatesAutoresizingMaskIntoConstraints = false
            };
            ButtonNo.Layer.BorderWidth = 1.0f;
            ButtonNo.Layer.BorderColor = UIColor.DarkGray.CGColor;
            ButtonNo.SetBackgroundImage(UIImage.FromBundle("baseline_clear_black_24dp").ImageWithTint(UIColor.DarkGray), UIControlState.Normal);

            AddSubview(Label);
            AddSubview(ButtonYes);
            AddSubview(ButtonNo);

            AddTouchUpEvents();
            SetNeedsUpdateConstraints();
        }

        protected virtual void AddTouchUpEvents()
        {
            ButtonYes.TouchUpInside += (sender, eventArgs) =>
            {
                if (Value == true)
                {
                    Value = null;
                }
                else
                {
                    Value = true;
                }
            };

            ButtonNo.TouchUpInside += (sender, eventArgs) =>
            {
                if (Value == false)
                {
                    Value = null;
                }
                else
                {
                    Value = false;
                }
            };
        }

        protected virtual void AddConstraints()
        {
            object[] viewsAndMetrics = new ViewMetricDictionary()
            {
                ["Label"] = Label,
                ["ButtonYes"] = ButtonYes,
                ["ButtonNo"] = ButtonNo
            }.ToArray();

            NSLayoutConstraint.ActivateConstraints(new NSLayoutConstraint[] 
            {
                ButtonYes.WidthAnchor.ConstraintGreaterThanOrEqualTo(24),
                ButtonYes.HeightAnchor.ConstraintGreaterThanOrEqualTo(24),
                ButtonYes.WidthAnchor.ConstraintEqualTo(ButtonYes.HeightAnchor)
            });

            NSLayoutConstraint.ActivateConstraints(new NSLayoutConstraint[]
            {
                ButtonNo.WidthAnchor.ConstraintGreaterThanOrEqualTo(24),
                ButtonNo.HeightAnchor.ConstraintGreaterThanOrEqualTo(24),
                ButtonNo.WidthAnchor.ConstraintEqualTo(ButtonNo.HeightAnchor)
            });

            NSLayoutConstraint.ActivateConstraints(
                NSLayoutConstraint.FromVisualFormat("H:|-15-[Label]-[ButtonYes]-5-[ButtonNo]-|", NSLayoutFormatOptions.SpacingEdgeToEdge, viewsAndMetrics));

            NSLayoutConstraint.ActivateConstraints(
                NSLayoutConstraint.FromVisualFormat("V:|-[Label]-|", NSLayoutFormatOptions.SpacingEdgeToEdge, viewsAndMetrics));

            NSLayoutConstraint.ActivateConstraints(
                NSLayoutConstraint.FromVisualFormat("V:|-[ButtonYes]-|", NSLayoutFormatOptions.SpacingEdgeToEdge, viewsAndMetrics));

            NSLayoutConstraint.ActivateConstraints(
                NSLayoutConstraint.FromVisualFormat("V:|-[ButtonNo]-|", NSLayoutFormatOptions.SpacingEdgeToEdge, viewsAndMetrics));
        }

        public override CGSize IntrinsicContentSize
        {
            get
            {
                return new CGSize()
                {
                    Height = 14 + (float)Math.Max(Label.IntrinsicContentSize.Height, 24),
                    Width = 30 + 10 + Label.IntrinsicContentSize.Width + 48
                };
            }
        }

        public override void LayoutSubviews()
        {
            base.LayoutSubviews();

            ButtonYes.Layer.CornerRadius = ButtonYes.Frame.Width / 2;
            ButtonNo.Layer.CornerRadius = ButtonNo.Frame.Width / 2;
        }

        public override void UpdateConstraints()
        {
            if (!mConstraintsAdded)
            {
                mConstraintsAdded = true;
                AddConstraints();
            }

            base.UpdateConstraints();
        }

        protected void RenderButtons()
        {
            if (Value == null)
            {
                ButtonYes.BackgroundColor = UIColor.Clear;
                ButtonNo.BackgroundColor = UIColor.Clear;
            }
            else if (Value == true)
            {
                ButtonYes.BackgroundColor = UIColor.Green;
                ButtonNo.BackgroundColor = UIColor.Clear;
            }
            else
            {
                ButtonYes.BackgroundColor = UIColor.Clear;
                ButtonNo.BackgroundColor = UIColor.Red;
            }
        }

        [Export("Text"), Browsable(true)]
        public string Text
        {
            get => Label.Text;
            set => Label.Text = value;
        }
    }
}

