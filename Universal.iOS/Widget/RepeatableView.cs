﻿using CoreGraphics;
using Foundation;
using MaterialComponents;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using UIKit;
using Universal.iOS.Extensions;

namespace Universal.iOS.Widget
{
    [Register("RepeatableView")]
    [DesignTimeVisible(true)]
    public class RepeatableView : UIView
    {
        
        public RepeatableView()
        {
            Initialize();
        }
        public RepeatableView(NSCoder coder)
            : base(coder)
        {
            Initialize(/* ? baseProperties: false ? */);
        }

        public RepeatableView(IntPtr ptr)
            : base(ptr)
        {
            Initialize(false);
        }

        public RepeatableView(CGRect frame)
            : base(frame)
        {
            Initialize();
        }

        public RepeatableViewSegment TopSegment  { get; set; }
        public RepeatableViewSegment TemplateSegment { get; set; }
        private List<UIView> AddedSegments { get; set; }


        private void Initialize(bool baseProperties = true)
        {
            AddedSegments = new List<UIView> { };

            UIView segmentLabel = new UILabel
            {
                TextColor = UIColor.DarkGray,
                Font = UIFont.SystemFontOfSize(17),
                Lines = 0,
                LineBreakMode = UILineBreakMode.TailTruncation,
                TranslatesAutoresizingMaskIntoConstraints = false,
                Text = "TOP SEGMENT"
            };

            TopSegment = new RepeatableViewSegment(segmentLabel)
            {
                ButtonIcon = "baseline_add_black_24dp",
                //TranslatesAutoresizingMaskIntoConstraints = false,
            };

            TopSegment.ActionButton.TouchUpInside += (sender, e) => AddNewView();
        }

        private void AddNewView()
        {
            UIView segmentMainView = new TextField
            {
                TextColor = UIColor.DarkGray,
                Font = UIFont.SystemFontOfSize(17),
                TranslatesAutoresizingMaskIntoConstraints = false,
            };

            RepeatableViewSegment viewSegment = new RepeatableViewSegment(segmentMainView)
            {
                ButtonIcon = "baseline_clear_black_24dp",
                //TranslatesAutoresizingMaskIntoConstraints = false,
            };

            AddedSegments.Add(viewSegment);

            viewSegment.ActionButton.TouchUpInside += (sender2, e2) =>
            {
                AddedSegments.Remove(viewSegment);
                SetNeedsLayout();
            };

            SetNeedsLayout();
        }

        private nfloat GetHeight()
        {
            var height = TopSegment.IntrinsicContentSize.Height;
            foreach(var view in AddedSegments)
            {
                height += view.IntrinsicContentSize.Height;
            }
            return height;
        }

        public override void LayoutSubviews()
        {
            base.LayoutSubviews();
            foreach(var v in Subviews)
            {
                v.RemoveFromSuperview();
            }

            Frame = new CGRect(Frame.X, Frame.Y, Frame.Width, GetHeight());
            TopSegment.Frame = new CGRect(0, 0, Frame.Width, TopSegment.IntrinsicContentSize.Height);// TopSegment.IntrinsicContentSize.Height);

            AddSubview(TopSegment);
            TopSegment.UpdateConstraints();

            var rollingHeight = TopSegment.IntrinsicContentSize.Height;
            foreach (var view in AddedSegments)
            {
                view.Frame = new CGRect(0, rollingHeight, Frame.Width, view.IntrinsicContentSize.Height);
                AddSubview(view);
                view.UpdateConstraints();
                rollingHeight += view.IntrinsicContentSize.Height;
            }
            SetNeedsUpdateConstraints();
        }

        public class RepeatableViewSegment : UIView
        {
            public RepeatableViewSegment(UIView mainView)
            {
                MainView = mainView;
                Initialize();
            }

            public RepeatableViewSegment()
            {
                Initialize();
            }
            public RepeatableViewSegment(NSCoder coder)
                : base(coder)
            {
                Initialize(/* ? baseProperties: false ? */);
            }

            public RepeatableViewSegment(IntPtr ptr)
                : base(ptr)
            {
                Initialize(false);
            }

            public RepeatableViewSegment(CGRect frame)
                : base(frame)
            {
                Initialize();
            }

            public UIView MainView { get; set; }
            public UIButton ActionButton { get; set; }
            
            public string ButtonIcon { get; set; }

            private void Initialize(bool baseProperties = true)
            {
                BackgroundColor = UIColor.White;

                if (MainView == null)
                {
                    MainView = new UILabel
                    {
                        TextColor = UIColor.DarkGray,
                        Font = UIFont.SystemFontOfSize(17),
                        Lines = 0,
                        LineBreakMode = UILineBreakMode.TailTruncation,
                        TranslatesAutoresizingMaskIntoConstraints = false,
                        Text = "SEGMENT PLACEHOLDER"
                    };
                }
                
                ActionButton = new UIButton()
                {
                    TranslatesAutoresizingMaskIntoConstraints = false
                };

                ActionButton.Layer.BorderWidth = 1.0f;
                ActionButton.Layer.BorderColor = UIColor.DarkGray.CGColor;

                AddSubview(MainView);
                AddSubview(ActionButton);
            }

            public override CGSize IntrinsicContentSize
            {
                get
                {
                    return new CGSize()
                    {
                        Height = 14 + (float)Math.Max(MainView.IntrinsicContentSize.Height, 24),
                        Width = 30 + 10 + MainView.IntrinsicContentSize.Width + 48
                    };
                }
            }

            public override void LayoutSubviews()
            {
                base.LayoutSubviews();
                
                ActionButton.SetBackgroundImage(UIImage.FromBundle(ButtonIcon).ImageWithTint(UIColor.DarkGray), UIControlState.Normal);
                ActionButton.Layer.CornerRadius = ActionButton.Frame.Width / 2;
            }

            protected virtual void AddConstraints()
            {
                NSLayoutConstraint.ActivateConstraints(new NSLayoutConstraint[]
                {
                ActionButton.WidthAnchor.ConstraintEqualTo(24),
                ActionButton.HeightAnchor.ConstraintEqualTo(24),
                ActionButton.WidthAnchor.ConstraintEqualTo(ActionButton.HeightAnchor)
                });

                object[] viewsAndMetrics = new ViewMetricDictionary()
                {
                    ["MainView"] = MainView,
                    ["ActionButton"] = ActionButton
                }.ToArray();

                NSLayoutConstraint.ActivateConstraints(
                    NSLayoutConstraint.FromVisualFormat("H:|-15-[MainView]-[ActionButton]-|", NSLayoutFormatOptions.SpacingEdgeToEdge, viewsAndMetrics));

                NSLayoutConstraint.ActivateConstraints(
                    NSLayoutConstraint.FromVisualFormat("V:|-[MainView]-|", NSLayoutFormatOptions.SpacingEdgeToEdge, viewsAndMetrics));

                NSLayoutConstraint.ActivateConstraints(
                    NSLayoutConstraint.FromVisualFormat("V:|-[ActionButton]", NSLayoutFormatOptions.SpacingEdgeToEdge, viewsAndMetrics));
            }

            public override void UpdateConstraints()
            {
                AddConstraints();
                base.UpdateConstraints();
            }
        }
    }
}
