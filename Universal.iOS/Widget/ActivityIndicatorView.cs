﻿using CoreGraphics;
using Foundation;
using System;
using UIKit;

namespace Universal.iOS.Widget
{
    /// <summary>
    /// A <see cref="UIView" /> suitable to be used to indicate activity with.
    /// </summary>
    public class ActivityIndicatorView : UIView
    {
        public UIView ContentView { get; protected set; }
        protected nfloat mBackgroundAlpha;
        public nfloat BackgroundAlpha
        {
            get => mBackgroundAlpha;
            set
            {
                if (mBackgroundAlpha != value)
                {
                    mBackgroundAlpha = value;
                    BackgroundView.BackgroundColor = ResolveBackgroundColor();
                    SetNeedsLayout();
                }
            }

        }
        protected UIView BackgroundView { get; set; }
        protected UIActivityIndicatorView IndicatorView { get; set; }

        public ActivityIndicatorView() : base() { Initialize(); }
        public ActivityIndicatorView(NSCoder coder) : base(coder) { Initialize(); }
        public ActivityIndicatorView(NSObjectFlag t) : base(t) { Initialize(); }
        public ActivityIndicatorView(CGRect frame) : base(frame) { Initialize(); }
        public ActivityIndicatorView(IntPtr handle) : base(handle) { Initialize(); }

        public override CGSize SizeThatFits(CGSize size)
        {
            return new CGSize(50, 50);
        }

        private void Initialize()
        {
            mBackgroundAlpha = 0.4f;
            BackgroundView = new UIView() { BackgroundColor = ResolveBackgroundColor() };

            ContentView = BackgroundView;

            ContentView.Frame = Frame;
            ContentView.AutoresizingMask = UIViewAutoresizing.FlexibleWidth | UIViewAutoresizing.FlexibleHeight;

            AddSubview(ContentView);
            IndicatorView = new UIActivityIndicatorView(new CGRect(0, 0, 50, 50));
            if (TraitCollection.UserInterfaceStyle == UIUserInterfaceStyle.Dark)
            {
                IndicatorView.ActivityIndicatorViewStyle = UIActivityIndicatorViewStyle.Gray;
            }
            else
            {
                IndicatorView.ActivityIndicatorViewStyle = UIActivityIndicatorViewStyle.WhiteLarge;
            }
            IndicatorView.StartAnimating();
            BackgroundView.AddSubview(IndicatorView);
        }

        public override void LayoutSubviews()
        {
            base.LayoutSubviews();

            if (IndicatorView.Center.X != BackgroundView.Center.X || IndicatorView.Center.Y != BackgroundView.Center.Y)
            {
                IndicatorView.Center = BackgroundView.Center;
            }
        }

        public override void TraitCollectionDidChange(UITraitCollection previousTraitCollection)
        {
            base.TraitCollectionDidChange(previousTraitCollection);

            if (TraitCollection.HasDifferentColorAppearanceComparedTo(previousTraitCollection))
            {
                BackgroundView.BackgroundColor = ResolveBackgroundColor();
                if (TraitCollection.UserInterfaceStyle == UIUserInterfaceStyle.Dark)
                {
                    IndicatorView.ActivityIndicatorViewStyle = UIActivityIndicatorViewStyle.Gray;
                }
                else
                {
                    IndicatorView.ActivityIndicatorViewStyle = UIActivityIndicatorViewStyle.WhiteLarge;
                }

                SetNeedsLayout();
            }
        }

        protected virtual UIColor ResolveBackgroundColor()
        {
            UIUserInterfaceStyle inverseUserInterfaceStyle = TraitCollection.UserInterfaceStyle;

            if (inverseUserInterfaceStyle == UIUserInterfaceStyle.Light)
            {
                inverseUserInterfaceStyle = UIUserInterfaceStyle.Dark;
            }
            else if (inverseUserInterfaceStyle == UIUserInterfaceStyle.Dark)
            {
                inverseUserInterfaceStyle = UIUserInterfaceStyle.Light;
            }

            UIColor result = UIColor.SystemBackgroundColor.GetResolvedColor(UITraitCollection.FromUserInterfaceStyle(inverseUserInterfaceStyle));
            return result.ColorWithAlpha(mBackgroundAlpha);
        }
    }
}