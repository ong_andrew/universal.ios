﻿using CoreGraphics;
using UIKit;
using Universal.iOS.Extensions;

namespace Universal.iOS.Widget
{
    internal class RadioCircle : UIView
    {
        protected UIColor mControlNormalColor;
        public virtual UIColor ControlNormalColor
        {
            get => mControlNormalColor;
            set
            {
                if (mControlNormalColor != value)
                {
                    mControlNormalColor = value;
                    SetNeedsDisplay();
                }
            }
        }

        protected bool mChecked;
        public bool Checked
        {
            get => mChecked;
            set
            {
                if (mChecked != value)
                {
                    mChecked = value;
                    SetNeedsDisplay();
                }
            }
        }
        protected double Size { get; set; }

        public RadioCircle() : this(24) { }
        public RadioCircle(int radius) : base() 
        {
            mControlNormalColor = UIColor.LabelColor;
            Size = radius; 
            Opaque = false; 
        }

        public override void Draw(CGRect rect)
        {
            CGContext context = UIGraphics.GetCurrentContext();
            context.ClearRect(rect);

            double scaleFactor = Size / 24;

            UIBezierPath path = new UIBezierPath();

            if (Checked)
            {
                path.MoveTo(new CGPoint(12, 7).Scale(scaleFactor));
                path.AddCurveToPointRelative(new CGPoint(-5, 5).Scale(scaleFactor), new CGPoint(-2.76, 0).Scale(scaleFactor), new CGPoint(-5, 2.24).Scale(scaleFactor));
                path.AddSmoothCurveToPointRelative(new CGPoint(5, 5).Scale(scaleFactor), new CGPoint(2.24, 5).Scale(scaleFactor));
                path.AddSmoothCurveToPointRelative(new CGPoint(5, -5).Scale(scaleFactor), new CGPoint(5, -2.24).Scale(scaleFactor));
                path.AddSmoothCurveToPointRelative(new CGPoint(-5, -5).Scale(scaleFactor), new CGPoint(-2.24, -5).Scale(scaleFactor));
                path.ClosePath();

                path.MoveToRelative(new CGPoint(0, -5).Scale(scaleFactor));
                path.AddCurveToPoint(new CGPoint(2, 12).Scale(scaleFactor), new CGPoint(6.48, 2).Scale(scaleFactor), new CGPoint(2, 6.48).Scale(scaleFactor));
                path.AddSmoothCurveToPointRelative(new CGPoint(10, 10).Scale(scaleFactor), new CGPoint(4.48, 10).Scale(scaleFactor));
                path.AddSmoothCurveToPointRelative(new CGPoint(10, -10).Scale(scaleFactor), new CGPoint(10, -4.48).Scale(scaleFactor));
                path.AddSmoothCurveToPoint(new CGPoint(12, 2).Scale(scaleFactor), new CGPoint(17.52, 2).Scale(scaleFactor));
                path.ClosePath();

                path.MoveToRelative(new CGPoint(0, 18).Scale(scaleFactor));
                path.AddCurveToPointRelative(new CGPoint(-8, -8).Scale(scaleFactor), new CGPoint(-4.42, 0).Scale(scaleFactor), new CGPoint(-8, -3.58).Scale(scaleFactor));
                path.AddSmoothCurveToPointRelative(new CGPoint(8, -8).Scale(scaleFactor), new CGPoint(3.58, -8).Scale(scaleFactor));
                path.AddSmoothCurveToPointRelative(new CGPoint(8, 8).Scale(scaleFactor), new CGPoint(8, 3.58).Scale(scaleFactor));
                path.AddSmoothCurveToPointRelative(new CGPoint(-8, 8).Scale(scaleFactor), new CGPoint(-3.58, 8).Scale(scaleFactor)); 
                path.ClosePath();
            }
            else
            {
                path.MoveTo(new CGPoint(12, 2).Scale(scaleFactor));
                path.AddCurveToPoint(new CGPoint(2, 12).Scale(scaleFactor), new CGPoint(6.48, 2).Scale(scaleFactor), new CGPoint(2, 6.48).Scale(scaleFactor));
                path.AddSmoothCurveToPointRelative(new CGPoint(10, 10).Scale(scaleFactor), new CGPoint(4.48, 10).Scale(scaleFactor));
                path.AddSmoothCurveToPointRelative(new CGPoint(10, -10).Scale(scaleFactor), new CGPoint(10, -4.48).Scale(scaleFactor));
                path.AddSmoothCurveToPoint(new CGPoint(12, 2).Scale(scaleFactor), new CGPoint(17.52, 2).Scale(scaleFactor));
                path.ClosePath();

                path.MoveToRelative(new CGPoint(0, 18).Scale(scaleFactor));
                path.AddCurveToPointRelative(new CGPoint(-8, -8).Scale(scaleFactor), new CGPoint(-4.42, 0).Scale(scaleFactor), new CGPoint(-8, -3.58).Scale(scaleFactor));
                path.AddSmoothCurveToPointRelative(new CGPoint(8, -8).Scale(scaleFactor), new CGPoint(3.58, -8).Scale(scaleFactor));
                path.AddSmoothCurveToPointRelative(new CGPoint(8, 8).Scale(scaleFactor), new CGPoint(8, 3.58).Scale(scaleFactor));
                path.AddSmoothCurveToPointRelative(new CGPoint(-8, 8).Scale(scaleFactor), new CGPoint(-3.58, 8).Scale(scaleFactor));
                path.ClosePath();
            }

            ControlNormalColor.SetFill();
            path.Fill();
        }

        public override CGSize SizeThatFits(CGSize size)
        {
            return IntrinsicContentSize;
        }

        public override CGSize IntrinsicContentSize
        {
            get
            {
                return new CGSize(Size, Size);
            }
        }
    }
}