﻿using CoreGraphics;
using Foundation;
using System;
using System.ComponentModel;
using System.Linq;
using UIKit;
using Universal.iOS.Extensions;
using Universal.iOS.Views;

namespace Universal.iOS.Widget
{
    /// <summary>
    /// A <see cref="UIScrollView"/> that automatically resizes the <see cref="UIScrollView.ContentSize"/> property to match the subviews.
    /// </summary>
    [Register("ScrollView")]
    [DesignTimeVisible(true)]
    public class ScrollView : UIScrollView, IViewGroup
    {
        public ScrollView() { Initialize(); }
        public ScrollView(NSCoder coder) : base(coder) { Initialize(); }
        public ScrollView(IntPtr ptr) : base(ptr) { Initialize(); }
        public ScrollView(CGRect frame) : base(frame) { Initialize(); }
        public ScrollView(NSObjectFlag t) : base(t) { Initialize(); }

        private void Initialize() { }

        private bool HasPersistentInsets { get; set;}

        private UIEdgeInsets persistentInsets;
        public UIEdgeInsets PersistentInsets
        {
            private get => persistentInsets;
            set
            {
                ContentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentBehavior.Never;
                HasPersistentInsets = true;
                persistentInsets = value;
            }
        }

        public override UIEdgeInsets ContentInset
        {
            get
            {
                if (!HasPersistentInsets)
                {
                    return base.ContentInset;
                }
                else
                {
                    return PersistentInsets;
                }
            }

            set
            {
                if (!HasPersistentInsets)
                {
                    base.ContentInset = value;
                }
                else
                {
                    //do nothing
                }
            }
        }

        public override UIEdgeInsets AdjustedContentInset
        {
            get
            {
                if (!HasPersistentInsets)
                {
                    return base.AdjustedContentInset;
                }
                else
                {
                    return new UIEdgeInsets(PersistentInsets.Top, PersistentInsets.Left, PersistentInsets.Bottom, PersistentInsets.Right);
                }
            }
        }

        public override void LayoutSubviews()
        {
            base.LayoutSubviews();
            foreach (UIView subview in Subviews)
            {
                ViewGroup.LayoutParameters layoutParameters = subview.GetLayoutParameters();

                if (layoutParameters != null)
                {
                    nfloat width = 0;
                    nfloat height = 0;

                    if (layoutParameters.Width >= 0)
                    {
                        width = layoutParameters.Width;
                    }
                    else if (layoutParameters.Width == ViewGroup.LayoutParameters.MatchParent)
                    {
                        width = Frame.Size.Width;
                    }
                    else if (layoutParameters.Width == ViewGroup.LayoutParameters.WrapContent)
                    {
                        width = subview.SizeThatFits(Frame.Size).Width;
                    }

                    if (layoutParameters.Height >= 0)
                    {
                        height = layoutParameters.Height;
                    }
                    else if (layoutParameters.Height == ViewGroup.LayoutParameters.MatchParent)
                    {
                        height = Frame.Size.Height;
                    }
                    else if (layoutParameters.Height == ViewGroup.LayoutParameters.WrapContent)
                    {
                        height = subview.SizeThatFits(Frame.Size).Height;
                    }

                    subview.SetFrameSizeIfNeeded(new CGSize(width, height));
                }
            }

            //todo this only uses the one subview max top to bottom and left to right, it does not compare any max bottom to any max top
            var contentSize = new CGSize(Subviews.Any() ? Subviews.Max(x => x.Frame.Right - Math.Max(0, x.Frame.Left)) : 0, Subviews.Any() ? Subviews.Max(x => x.Frame.Bottom - Math.Max(0, x.Frame.Top)) : 0);
            ContentSize = new CGSize(contentSize.Width, contentSize.Height);
            //hack: properties not working???
            //ContentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentBehavior.Never;
            //ContentInset = PersistentInsets;
            //Console.WriteLine(ContentInset);
            //Console.WriteLine(AdjustedContentInset);
        }

        /// <summary>
        /// Scrolls the <see cref="ScrollView"/> to the left.
        /// </summary>
        public void ScrollToLeft()
        {
            ScrollToLeft(false);
        }

        /// <summary>
        /// Scrolls the <see cref="ScrollView"/> to the left.
        /// </summary>
        /// <param name="animated"></param>
        public void ScrollToLeft(bool animated)
        {
            SetContentOffset(new CGPoint(-ContentInset.Left, ContentOffset.Y), animated);
        }

        /// <summary>
        /// Scrolls the <see cref="ScrollView"/> to the right.
        /// </summary>
        public void ScrollToRight()
        {
            ScrollToRight(false);
        }

        /// <summary>
        /// Scrolls the <see cref="ScrollView"/> to the right.
        /// </summary>
        /// <param name="animated"></param>
        public void ScrollToRight(bool animated)
        {
            SetContentOffset(new CGPoint(ContentSize.Width - Bounds.Width + ContentInset.Right, ContentOffset.Y), animated);
        }

        /// <summary>
        /// Scrolls the <see cref="ScrollView"/> to the top.
        /// </summary>
        public void ScrollToTop()
        {
            ScrollToTop(false);
        }

        /// <summary>
        /// Scrolls the <see cref="ScrollView"/> to the top.
        /// </summary>
        /// <param name="animated"></param>
        public void ScrollToTop(bool animated)
        {
            SetContentOffset(new CGPoint(ContentOffset.X, -ContentInset.Top), animated);
        }
        /// <summary>
        /// Scrolls the <see cref="ScrollView"/> to the bottom.
        /// </summary>

        public void ScrollToBottom()
        {
            ScrollToBottom(false);
        }

        /// <summary>
        /// Scrolls the <see cref="ScrollView"/> to the bottom.
        /// </summary>
        /// <param name="animated"></param>
        public void ScrollToBottom(bool animated)
        {
            SetContentOffset(new CGPoint(ContentOffset.X, ContentSize.Height - Bounds.Height + ContentInset.Bottom), animated);
        }

        /// <summary>
        /// Adds the given subview to this <see cref="ScrollView" /> with the given layout parameters.
        /// </summary>
        /// <param name="view"></param>
        /// <param name="layoutParameters"></param>
        public virtual void AddSubview(UIView view, ViewGroup.LayoutParameters layoutParameters)
        {
            if (layoutParameters is null)
            {
                throw new ArgumentNullException(nameof(layoutParameters));
            }

            view.SetLayoutParameters(layoutParameters);
            AddSubview(view);
        }

        public override void SetNeedsLayout()
        {
            base.SetNeedsLayout();
            if (Superview is IViewGroup)
            {
                Superview.SetNeedsLayout();
            }
        }
    }
}