﻿using System.Collections.Generic;
using System.Linq;

namespace Universal.iOS.Widget
{
    /// <summary>
    /// Class facilitating construction of metrics and view key-value pairs for constraints.
    /// </summary>
    public class ViewMetricDictionary : Dictionary<string, object>
    {
        public object[] ToArray()
        {
            return this.SelectMany(x => new object[] { x.Key, x.Value }).ToArray();
        }
    }
}