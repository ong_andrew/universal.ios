﻿using CoreGraphics;
using Foundation;
using System;
using System.ComponentModel;
using UIKit;

namespace Universal.iOS.Widget
{
    /// <summary>
    /// An <see cref="EditText" /> that implements a <see cref="UIDatePicker" /> for its input method.
    /// </summary>
    [Register("TimePickerEditText")]
    [DesignTimeVisible(true)]
    public class TimePickerEditText : EditText
    {
        private TimeSpan? mTime;
        /// <summary>
        /// Gets or sets the time of the <see cref="TimePickerEditText" />.
        /// </summary>
        public TimeSpan? Time
        {
            get => mTime;
            set
            {
                if (mTime != value)
                {
                    mTime = value;
                    SetTextFromTime();
                }
            }
        }
        protected string mFormat;
        /// <summary>
        /// Gets or sets the format of the <see cref="TimePickerEditText" />.
        /// </summary>
        public string Format
        {
            get => mFormat;
            set
            {
                if (mFormat != value)
                {
                    mFormat = value;
                    SetTextFromTime();
                }
            }
        }

        protected UIDatePicker DatePicker { get; set; }
        protected UIBarButtonItem DoneButton { get; set; }

        public TimePickerEditText() : base() { Initialize(); }
        public TimePickerEditText(CGRect frame) : base(frame) { Initialize(); }
        public TimePickerEditText(IntPtr handle) : base(handle) { Initialize(); }
        public TimePickerEditText(NSCoder coder) : base(coder) { Initialize(); }
        public TimePickerEditText(NSObjectFlag t) : base(t) { Initialize(); }

        private void Initialize()
        {
            DatePicker = new UIDatePicker();
            DatePicker.SizeToFit();
            DatePicker.AutoresizingMask = UIViewAutoresizing.FlexibleHeight | UIViewAutoresizing.FlexibleWidth;
            DatePicker.Mode = UIDatePickerMode.Time;

            UIToolbar toolbar = new UIToolbar();
            toolbar.BarStyle = UIBarStyle.Default;
            toolbar.Translucent = true;
            toolbar.TintColor = null;
            toolbar.SizeToFit();

            DoneButton = new UIBarButtonItem(title: "Done", style: UIBarButtonItemStyle.Done, target: this, action: new ObjCRuntime.Selector("dismissPicker"));
            toolbar.SetItems(new UIBarButtonItem[] { DoneButton }, animated: false);

            InputView = DatePicker;
            InputAccessoryView = toolbar;
        }

        protected virtual void SetTextFromTime()
        {
            Text = Time.HasValue ? Time.Value.ToString(Format ?? "hh\\:mm") : string.Empty;
            SetNeedsLayout();
            Superview?.SetNeedsLayout();
        }

        [EditorBrowsable(EditorBrowsableState.Never)]
        [Export("dismissPicker")]
        public void DismissPicker()
        {
            Time = ((DateTime)DatePicker.Date).ToLocalTime().TimeOfDay;

            SetTextFromTime();

            ResignFirstResponder();
        }
    }
}