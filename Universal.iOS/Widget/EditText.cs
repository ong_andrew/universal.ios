﻿using CoreAnimation;
using CoreGraphics;
using Foundation;
using System;
using System.ComponentModel;
using UIKit;
using Universal.Common.Extensions;

namespace Universal.iOS.Widget
{
    /// <summary>
    /// A <see cref="UITextField" /> that implements floating labels and underline.
    /// </summary>
    [Register("EditText")]
    [DesignTimeVisible(true)]
    public class EditText : UITextField, IEditText, IUITextFieldDelegate
    {
        private nfloat mUnderlineThickness;
        /// <summary>
        /// Gets or sets the thickness of the line to draw when in the normal state.
        /// </summary>
        public nfloat UnderlineThickness
        {
            get => mUnderlineThickness;
            set
            {
                if (mUnderlineThickness != value)
                {
                    mUnderlineThickness = value;
                    SetNeedsLayout();
                }
            }
        }
        private nfloat mUnderlineThicknessSelected;
        /// <summary>
        /// Gets or sets the thickness of the line to draw when in the selected state.
        /// </summary>
        public nfloat UnderlineThicknessSelected
        {
            get => mUnderlineThicknessSelected;
            set
            {
                if (mUnderlineThicknessSelected != value)
                {
                    mUnderlineThicknessSelected = value;
                    SetNeedsLayout();
                }
            }
        }
        private nfloat mFloatingLabelHeight;
        /// <summary>
        /// Gets or sets the height of the floating hint label.
        /// </summary>
        public virtual nfloat FloatingLabelHeight
        {
            get => mFloatingLabelHeight;
            set
            {
                if (mFloatingLabelHeight != value)
                {
                    mFloatingLabelHeight = value;
                    SetNeedsLayout();
                }
            }
        }
        private UIColor mControlNormalColor;
        /// <summary>
        /// Gets or sets the color to use when the control is in the normal state.
        /// </summary>
        public virtual UIColor ControlNormalColor
        {
            get => mControlNormalColor;
            set
            {
                if (mControlNormalColor != value)
                {
                    mControlNormalColor = value;
                    SetNeedsLayout();
                }
            }
        }
        private UIColor mAccentColor;
        /// <summary>
        /// Gets or sets the accent color to use.
        /// </summary>
        public virtual UIColor AccentColor
        {
            get => mAccentColor;
            set
            {
                if (mAccentColor != value)
                {
                    mAccentColor = value;
                    SetNeedsLayout();
                }
            }
        }
        private UIColor mPlaceholderColor;
        /// <summary>
        /// Gets or sets the placeholder color.
        /// </summary>
        public virtual UIColor PlaceholderColor
        {
            get => mPlaceholderColor;
            set
            {
                if (mPlaceholderColor != value)
                {
                    mPlaceholderColor = value;
                    SetNeedsLayout();
                }
            }
        }

        protected CALayer UnderlineLayer { get; private set; }
        protected CATextLayer HintLayer { get; private set; }

        private string mHint;
        /// <summary>
        /// Gets or sets the hint text.
        /// </summary>
        public virtual string Hint
        {
            get => mHint;
            set
            {
                if (mHint != value)
                {
                    mHint = value;
                    HintLayer.String = value;
                    if (!IsFirstResponder)
                    {
                        base.Placeholder = value;
                    }
                }
            }
        }
        public override string Placeholder
        {
            get => Hint;
            set
            {
                Hint = value;
            }
        }

        private bool mShowFloatingLabel;
        /// <summary>
        /// Gets or sets a value that determines if the floating label should be visible.
        /// </summary>
        public bool ShowFloatingLabel
        {
            get => mShowFloatingLabel;
            set
            {
                if (mShowFloatingLabel != value)
                {
                    mShowFloatingLabel = value;
                    SetNeedsLayout();
                }
            }
        }

        private bool mReturnEndsEditiing;
        /// <summary>
        /// Gets or sets a value indicating if the return key should end editing.
        /// </summary>
        public bool ReturnEndsEditing
        {
            get => mReturnEndsEditiing;
            set
            {
                if (mReturnEndsEditiing != value)
                {
                    mReturnEndsEditiing = value;
                }
            }
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="EditText" /> class.
        /// </summary>
        public EditText() : base() { Initialize(); }
        /// <summary>
        /// Initializes a new instance of the <see cref="EditText" /> class.
        /// </summary>
        /// <param name="frame"></param>
        public EditText(CGRect frame) : base(frame) { Initialize(); }
        /// <summary>
        /// Initializes a new instance of the <see cref="EditText" /> class.
        /// </summary>
        /// <param name="handle"></param>
        public EditText(IntPtr handle) : base(handle) { Initialize(); }
        /// <summary>
        /// Initializes a new instance of the <see cref="EditText" /> class.
        /// </summary>
        /// <param name="coder"></param>
        public EditText(NSCoder coder) : base(coder) { Initialize(); }
        /// <summary>
        /// Initializes a new instance of the <see cref="EditText" /> class.
        /// </summary>
        /// <param name="t"></param>
        public EditText(NSObjectFlag t) : base(t) { Initialize(); }

        private void Initialize()
        {
            mReturnEndsEditiing = false;
            mUnderlineThickness = 1.4f;
            mUnderlineThicknessSelected = 2.0f;
            mFloatingLabelHeight = UIFont.SystemFontOfSize(UIFont.SmallSystemFontSize).LineHeight;
            mControlNormalColor = UIColor.LabelColor;
            mPlaceholderColor = UIColor.LabelColor.ColorWithAlpha(0.25f);
            mAccentColor = UIColor.SystemBlueColor;
            mShowFloatingLabel = true;

            UnderlineLayer = new CALayer();
            UnderlineLayer.BackgroundColor = ControlNormalColor.CGColor;
            Layer.AddSublayer(UnderlineLayer);

            HintLayer = new CATextLayer();
            HintLayer.ContentsScale = UIScreen.MainScreen.Scale;
            HintLayer.String = Hint;
            Layer.AddSublayer(HintLayer);

            EditingDidBegin += (sender, eventArgs) =>
            {
                if (ShowFloatingLabel)
                {
                    HintLayer.Hidden = false;
                }
                HintLayer.FontSize = UIFont.SmallSystemFontSize;
                HintLayer.Frame = new CGRect(0, 0, Frame.Width, FloatingLabelHeight);

                HintLayer.ForegroundColor = AccentColor.CGColor;
                UnderlineLayer.BackgroundColor = AccentColor.CGColor;
                UnderlineLayer.Frame = new CGRect(0, FloatingLabelHeight + EditingRect(Bounds).Height, Frame.Width, UnderlineThicknessSelected);
                base.Placeholder = string.Empty;
            };

            EditingDidEnd += (sender, eventArgs) =>
            {
                if (!Text.IsNullOrEmpty())
                {
                    if (ShowFloatingLabel)
                    {
                        HintLayer.Hidden = false;
                    }
                    HintLayer.FontSize = UIFont.SmallSystemFontSize;
                    HintLayer.Frame = new CGRect(0, 0, Frame.Width, FloatingLabelHeight);
                    base.Placeholder = mHint;
                }
                else
                {
                    HintLayer.Hidden = true;
                    HintLayer.FontSize = Font.PointSize;
                    HintLayer.Frame = PlaceholderRect(Bounds);
                }

                HintLayer.ForegroundColor = ControlNormalColor.CGColor;
                UnderlineLayer.BackgroundColor = ControlNormalColor.CGColor;
                UnderlineLayer.Frame = new CGRect(0, FloatingLabelHeight + EditingRect(Bounds).Height, Frame.Width, UnderlineThickness);
            };

            Delegate = this;
        }

        public override CGRect PlaceholderRect(CGRect forBounds)
        {
            return EditingRect(forBounds);
        }

        public override CGRect TextRect(CGRect forBounds)
        {
            CGRect baseEditingRect = base.EditingRect(forBounds);
            return new CGRect(new CGPoint(baseEditingRect.X, baseEditingRect.Y + FloatingLabelHeight), new CGSize(baseEditingRect.Width, baseEditingRect.Height - Math.Max(UnderlineThickness, UnderlineThicknessSelected) - FloatingLabelHeight));
        }

        public override CGRect EditingRect(CGRect forBounds)
        {
            return TextRect(forBounds);
        }

        public override void DrawPlaceholder(CGRect rect)
        {
            PlaceholderColor.SetFill();
            Placeholder.DrawString(rect, Font, UILineBreakMode.Clip, TextAlignment);
        }

        public override void LayoutSubviews()
        {
            base.LayoutSubviews();

            if (!Text.IsNullOrEmpty() || IsFirstResponder)
            {
                if (ShowFloatingLabel)
                {
                    HintLayer.Hidden = false;
                }
                else
                {
                    HintLayer.Hidden = true;
                }
                HintLayer.FontSize = UIFont.SmallSystemFontSize;
                HintLayer.Frame = new CGRect(0, 0, Frame.Width, FloatingLabelHeight);
                base.Placeholder = string.Empty;
            }
            else
            {
                HintLayer.Hidden = true;
                HintLayer.FontSize = Font.PointSize;
                HintLayer.Frame = PlaceholderRect(Bounds);
                base.Placeholder = Hint;
            }

            if (IsFirstResponder)
            {
                HintLayer.ForegroundColor = AccentColor.CGColor;
                UnderlineLayer.Frame = new CGRect(0, FloatingLabelHeight + EditingRect(Bounds).Height, Frame.Width, UnderlineThicknessSelected);
                UnderlineLayer.BackgroundColor = AccentColor.CGColor;
            }
            else
            {
                HintLayer.ForegroundColor = ControlNormalColor.CGColor;
                UnderlineLayer.Frame = new CGRect(0, FloatingLabelHeight + EditingRect(Bounds).Height, Frame.Width, UnderlineThickness);
                UnderlineLayer.BackgroundColor = ControlNormalColor.CGColor;
            }
        }

        public override CGSize SizeThatFits(CGSize size)
        {
            CGSize originalSizeThatFits = base.SizeThatFits(size);

            return new CGSize(originalSizeThatFits.Width, originalSizeThatFits.Height + FloatingLabelHeight + Math.Max(UnderlineThickness, UnderlineThicknessSelected));
        }

        /// <summary>
        /// Asks the delegate if the text field should process the pressing of the return button.
        /// </summary>
        /// <param name="textField">The text field whose return button was pressed.</param>
        /// <returns></returns>
        [Export("textFieldShouldReturn:")]
        public virtual bool TextFieldShouldShouldReturn(UITextField textField)
        {
            if (mReturnEndsEditiing)
            {
                ResignFirstResponder();
            }

            return false;
        }
    }
}