﻿using CoreGraphics;
using UIKit;
using Universal.iOS.Extensions;
using Universal.iOS.Views;

namespace Universal.iOS.Widget
{
    /// <summary>
    /// A <see cref="UITableViewCell"/> that does not participate in auto-layout but participates in the <see cref="ViewGroup" /> functionality.
    /// </summary>
    public class TableViewCell : UITableViewCell
    {
        public override CGRect Bounds
        {
            get => base.Bounds;
            set
            {
                if (Superview != null)
                {
                    base.Bounds = new CGRect(value.Location, new CGSize(Superview.Bounds.Width, value.Height));
                    ContentView.Bounds = new CGRect(ContentView.Bounds.Location, new CGSize(Superview.Bounds.Width, ContentView.Bounds.Height));
                    ContentView.SetNeedsLayout();

                    foreach (ViewGroup viewGroup in ContentView.Children<ViewGroup>())
                    {
                        ViewGroup.LayoutParameters layoutParameters = viewGroup.GetLayoutParameters();
                        if (layoutParameters != null)
                        {
                            if (layoutParameters.Width == ViewGroup.LayoutParameters.MatchParent)
                            {
                                viewGroup.Bounds = new CGRect(viewGroup.Bounds.Location, new CGSize(Superview.Bounds.Width, viewGroup.Bounds.Height));
                            }
                        }
                    }
                }
                else
                {
                    base.Bounds = value;
                }
            }
        }

        public override CGRect Frame
        {
            get => base.Frame;
            set
            {
                if (Superview != null)
                {
                    base.Frame = new CGRect(value.Location, new CGSize(Superview.Frame.Width, value.Height));
                    ContentView.Frame = new CGRect(ContentView.Frame.Location, new CGSize(Superview.Frame.Width, ContentView.Frame.Height));
                    ContentView.SetNeedsLayout();

                    foreach (ViewGroup viewGroup in ContentView.Children<ViewGroup>())
                    {
                        ViewGroup.LayoutParameters layoutParameters = viewGroup.GetLayoutParameters();
                        if (layoutParameters != null)
                        {
                            if (layoutParameters.Width == ViewGroup.LayoutParameters.MatchParent)
                            {
                                viewGroup.Frame = new CGRect(viewGroup.Frame.Location, new CGSize(Superview.Frame.Width, viewGroup.Frame.Height));
                            }
                        }
                    }
                }
                else
                {
                    base.Frame = value;
                }
            }
        }

        public override void MovedToSuperview()
        {
            if (Superview != null)
            {
                Frame = new CGRect(new CGPoint(0, 0), new CGSize(Superview.Frame.Width, Frame.Height));
            }
        }

        public override void LayoutSubviews()
        {
            foreach (ViewGroup viewGroup in ContentView.Children<ViewGroup>())
            {
                ViewGroup.LayoutParameters layoutParameters = viewGroup.GetLayoutParameters();
                if (layoutParameters != null)
                {
                    if (layoutParameters.Height == ViewGroup.LayoutParameters.WrapContent)
                    {
                        CGSize sizeThatFits = viewGroup.SizeThatFits(ContentView.Frame.Size);
                        viewGroup.Frame = new CGRect(viewGroup.Frame.Location, new CGSize(Superview.Frame.Width, sizeThatFits.Height));
                    }
                }
            }
        }

        public override CGSize SizeThatFits(CGSize size)
        {
            double requiredWidth = 0;
            double requiredHeight = 0;

            foreach (UIView view in ContentView.Subviews)
            {
                CGSize sizeThatFitsSubview = view.SizeThatFits(Frame.Size);
                double currentViewRequiredWidth = sizeThatFitsSubview.Width + view.Frame.X;
                double currentViewRequiredHeight = sizeThatFitsSubview.Height + view.Frame.Y;

                if (currentViewRequiredWidth > requiredWidth)
                {
                    requiredWidth = currentViewRequiredWidth;
                }

                if (currentViewRequiredHeight > requiredHeight)
                {
                    requiredHeight = currentViewRequiredHeight;
                }
            }

            return new CGSize(requiredWidth, requiredHeight);
        }
    }
}