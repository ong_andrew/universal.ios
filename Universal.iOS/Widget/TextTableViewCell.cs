﻿using CoreGraphics;
using Foundation;
using System;
using UIKit;

namespace Universal.iOS.Widget
{
    /// <summary>
    /// A <see cref="UITableViewCell"/> that can store an item and displays it through the <see cref="UITableViewCell.TextLabel" />.
    /// </summary>
    /// <typeparam name="TItem"></typeparam>
    public class TextTableViewCell<TItem> : UITableViewCell
    {
        protected TItem mItem;
        public TItem Item
        {
            get => mItem;
            set
            {
                mItem = value;
                Render();
            }
        }

        public TextTableViewCell() : base() { Initialize(); }
        public TextTableViewCell(CGRect frame) : base(frame) { Initialize(); }
        public TextTableViewCell(NSCoder coder) : base(coder) { Initialize(); }
        public TextTableViewCell(NSObjectFlag t) : base(t) { Initialize(); }
        public TextTableViewCell(IntPtr handle) : base(handle) { Initialize(); }
        public TextTableViewCell(UITableViewCellStyle style, NSString reuseIdentifier) : base(style, reuseIdentifier) { Initialize(); }
        public TextTableViewCell(UITableViewCellStyle style, string reuseIdentifier) : base(style, reuseIdentifier) { Initialize(); }

        public void Initialize()
        {
            mProjection = (obj) => obj.ToString();
        }

        protected Func<TItem, string> mProjection;
        public Func<TItem, string> Projection
        {
            get => mProjection;
            set
            {
                mProjection = value;
                Render();
            }
        }

        protected void Render()
        {
            if (mProjection != null && Item != null)
            {
                TextLabel.Text = mProjection(Item);
            }
            else
            {
                TextLabel.Text = string.Empty;
            }
        }
    }
}