﻿using System;
using System.Timers;
using UIKit;

namespace Universal.iOS.Widget
{
    /// <summary>
    /// Enumerates values used by <see cref="Toast"/>.
    /// </summary>
    public enum ToastLength
    {
        /// <summary>
        /// Show the view or notification for a short period of time.
        /// </summary>
        Short,
        /// <summary>
        /// Show the view or notification for a long period of time.
        /// </summary>
        Long,
        /// <summary>
        /// Show the view or notification indefinitely until <see cref="Toast.Hide"/> is called.
        /// </summary>
        Indefinite
    }

    /// <summary>
    /// A compact notification toast that appears on the application's <see cref="UIWindow"/>.
    /// </summary>
    public class Toast : UIView
    {
        private static nint mTag = 243581439;

        public string Text { get; set; }
        public ToastLength Duration { get; set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="Toast"/> class.
        /// </summary>
        public Toast() : base() { }
        /// <summary>
        /// Initializes a new instance of the <see cref="Toast"/> class with the given message.
        /// </summary>
        /// <param name="text"></param>
        /// <param name="duration"></param>
        public Toast(string text, ToastLength duration = ToastLength.Short) : base()
        {
            Text = text;
            Duration = duration;
        }

        /// <summary>
        /// Shows the <see cref="Toast"/>.
        /// </summary>
        public void Show()
        {
            TranslatesAutoresizingMaskIntoConstraints = false;

            UIWindow uiWindow = UIApplication.SharedApplication.KeyWindow;

            UIView residualView = uiWindow.ViewWithTag(mTag);
            if (residualView != null)
            {
                residualView.RemoveFromSuperview();
            }

            foreach (UIView uiView in Subviews)
            {
                uiView.RemoveFromSuperview();
            }

            BackgroundColor = UIColor.FromRGBA(0, 0, 0, 187);
            Tag = mTag;
            Layer.CornerRadius = 10;
            Layer.MasksToBounds = true;

            UILabel uiLabel = new TextView();
            uiLabel.Text = Text;
            uiLabel.TextColor = UIColor.White;
            uiLabel.TextAlignment = UITextAlignment.Center;
            uiLabel.TranslatesAutoresizingMaskIntoConstraints = false;
            uiLabel.SizeToFit();

            AddSubview(uiLabel);

            uiWindow.AddSubview(this);
            CenterXAnchor.ConstraintEqualTo(uiWindow.CenterXAnchor).Active = true;
            BottomAnchor.ConstraintEqualTo(uiWindow.BottomAnchor, - uiWindow.Frame.Height / 5).Active = true;
            WidthAnchor.ConstraintEqualTo(uiLabel.WidthAnchor, 1.0f, 12).Active = true;
            HeightAnchor.ConstraintEqualTo(uiLabel.HeightAnchor, 1.0f, 12).Active = true;

            uiLabel.CenterXAnchor.ConstraintEqualTo(CenterXAnchor).Active = true;
            uiLabel.CenterYAnchor.ConstraintEqualTo(CenterYAnchor).Active = true;
            uiLabel.WidthAnchor.ConstraintLessThanOrEqualTo(uiWindow.WidthAnchor, 0.8f).Active = true;

            double duration = 1000;

            if (Duration == ToastLength.Short)
            {
                duration = 1000;
            }
            else if (Duration == ToastLength.Long)
            {
                duration = 2000;
            }
            else
            {
                return;
            }

            Timer timer = new Timer();
            timer.Interval = duration;
            timer.AutoReset = false;
            timer.Elapsed += (sender, eventArgs) =>
            {
                InvokeOnMainThread(() =>
                {
                    Hide(0.75);
                });
            };

            timer.Start();
        }

        /// <summary>
        /// Hides the <see cref="Toast"/>.
        /// </summary>
        public void Hide()
        {
            Alpha = 0.0f;
        }

        /// <summary>
        /// Hides the <see cref="Toast"/> over the given duration.
        /// </summary>
        public void Hide(double duration)
        {
            BeginAnimations("Toast");
            SetAnimationDuration(duration);
            Alpha = 0.0f;
            CommitAnimations();
        }
    }
}