﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Reflection;
using CoreGraphics;
using Foundation;
using UIKit;
using Universal.iOS.Extensions;

namespace Universal.iOS.Widget
{
    [Register("LinearRepeatableView")]
    [DesignTimeVisible(true)]
    public class LinearRepeatableView : LinearLayout
    {
        public LinearRepeatableView() : base() { Initialize(); }
        public LinearRepeatableView(CGRect frame) : base(frame) { Initialize(); }
        public LinearRepeatableView(IntPtr handle) : base(handle) { Initialize(); }
        public LinearRepeatableView(NSCoder coder) : base(coder) { Initialize(); }
        public LinearRepeatableView(NSObjectFlag t) : base(t) { Initialize(); }

        protected UIButton AddSection { get; set; }
        protected UILabel AddSectionLabel { get; set; }
        protected UIView SectionTemplate { get; set; }

        protected List<UIButton> SegmentButtons { get; set; }

        public Func<UIView> SegmentViewFactory { get; set; }

        public string Text
        {
            get => AddSectionLabel.Text;
            set
            {
                AddSectionLabel.Text = value;
            }
        }

        private void Initialize(bool baseProperties = true)
        {
            Orientation = Orientation.Vertical;

            SegmentButtons = new List<UIButton> { };

            AddSectionLabel = new UILabel
            {
                TextColor = UIColor.DarkGray,
                Font = UIFont.SystemFontOfSize(17),
                Lines = 0,
                LineBreakMode = UILineBreakMode.TailTruncation,
                TranslatesAutoresizingMaskIntoConstraints = false,
                Text = "Add Section"
            };

            AddSection = new UIButton();
            Assembly thisAssembly = Assembly.GetExecutingAssembly();
            string assemblyName = thisAssembly.GetName().Name;

            AddSection.Layer.BorderWidth = 1.0f;
            AddSection.Layer.BorderColor = UIColor.DarkGray.CGColor;
            AddSection.SetBackgroundImage(UIImage.FromResource(thisAssembly, assemblyName + ".Resources." + "baseline_add_black_24dp.png").ImageWithTint(UIColor.DarkGray), UIControlState.Normal);

            LinearLayout TopSegment = new LinearLayout();
            TopSegment.Orientation = Orientation.Horizontal;

            TopSegment.AddSubview(AddSectionLabel, new LayoutParameters(0, LayoutParameters.WrapContent, 1));
            TopSegment.AddSubview(AddSection, new LayoutParameters(LayoutParameters.WrapContent, LayoutParameters.WrapContent));

            AddSection.TouchUpInside += (s, e) =>
            {
                AddSegment();
            };

            AddSubview(TopSegment, new LayoutParameters(LayoutParameters.MatchParent, LayoutParameters.WrapContent));
        }

        private void AddSegment()
        {
            //LinearLayout TopSegment = new LinearLayout();
            //TopSegment.Orientation = Orientation.Horizontal;
            if(SegmentViewFactory == null)
            {
                SegmentViewFactory = Factory;
                UIView Factory()
                {
                    UITextField label = new UITextField
                    {
                        TextColor = UIColor.DarkGray,
                        Font = UIFont.SystemFontOfSize(17),
                        //Lines = 0,
                        //LineBreakMode = UILineBreakMode.TailTruncation,
                        //TranslatesAutoresizingMaskIntoConstraints = false,
                        //Placeholder = PlaceholderText
                    };
                    return label;
                }
            }

            LinearLayout Segment = new LinearLayout();
            Segment.Orientation = Orientation.Horizontal;
            Segment.Gravity = Views.GravityFlags.CenterVertical;

            UIButton removeSection = new UIButton();
            Assembly thisAssembly = Assembly.GetExecutingAssembly();
            string assemblyName = thisAssembly.GetName().Name;

            removeSection.Layer.BorderWidth = 1.0f;
            removeSection.Layer.BorderColor = UIColor.DarkGray.CGColor;
            removeSection.SetBackgroundImage(UIImage.FromResource(thisAssembly, assemblyName + ".Resources." + "baseline_clear_black_24dp.png").ImageWithTint(UIColor.DarkGray), UIControlState.Normal);

            UIView segmentSubView = SegmentViewFactory();
            Segment.AddSubview(segmentSubView, new LayoutParameters(0, LayoutParameters.WrapContent, 1));
            Segment.AddSubview(removeSection, new LayoutParameters(LayoutParameters.WrapContent, LayoutParameters.WrapContent));

            SegmentButtons.Add(removeSection);

            removeSection.TouchUpInside += (s, e) =>
            {
                Segment.RemoveFromSuperview();
                SegmentButtons.Remove(removeSection);
            };

            AddSubview(Segment, new LayoutParameters(LayoutParameters.MatchParent, LayoutParameters.WrapContent));
        }

        public override void LayoutSubviews()
        {
            base.LayoutSubviews();

            AddSection.Layer.CornerRadius = AddSection.Layer.Frame.Width / 2;
            foreach (var btn in SegmentButtons)
            {
                btn.Layer.CornerRadius = btn.Layer.Frame.Width / 2;   
            }
        }
    }
}
