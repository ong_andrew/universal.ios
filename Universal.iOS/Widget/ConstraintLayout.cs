﻿using CoreGraphics;
using Foundation;
using System;
using System.Collections.Generic;
using System.Linq;
using UIKit;
using Universal.Common;
using Universal.iOS.Extensions;
using Universal.iOS.Views;

namespace Universal.iOS.Widget
{
    public class ConstraintLayout : ViewGroup
    {
        public ConstraintLayout() : base() { Initialize(); }
        public ConstraintLayout(NSCoder coder) : base(coder) { Initialize(); }
        public ConstraintLayout(IntPtr ptr) : base(ptr) { Initialize(); }
        public ConstraintLayout(CGRect frame) : base(frame) { Initialize(); }
        public ConstraintLayout(NSObjectFlag t) : base(t) { Initialize(); }

        private void Initialize()
        {
            TranslatesAutoresizingMaskIntoConstraints = false;
        }

        public override void UpdateConstraints()
        {
            base.UpdateConstraints();

            foreach (UIView subview in Subviews)
            {
                subview.TranslatesAutoresizingMaskIntoConstraints = false;

                LayoutParameters layoutParameters = GetLayoutParameters(subview);

                PerformConstraintSetUpdate(subview, layoutParameters.ToLayoutConstraints(this, subview));
            }
        }

        public override void AddSubview(UIView view)
        {
            if (view.GetLayoutParameters() == null)
            {
                if (view.Frame.Height != 0 || view.Frame.Width != 0)
                {
                    view.SetLayoutParameters(new ConstraintLayout.LayoutParameters((int)view.Frame.Width, (int)view.Frame.Height));
                }
                else
                {
                    view.SetLayoutParameters(new LayoutParameters(ViewGroup.LayoutParameters.WrapContent, ViewGroup.LayoutParameters.WrapContent));
                }
            }

            if (!(view.GetLayoutParameters() is LayoutParameters))
            {
                throw new TypeMismatchException(typeof(LayoutParameters), view.GetLayoutParameters().GetType());
            }

            base.AddSubview(view);
        }

        public virtual void AddSubview(UIView view, ConstraintLayout.LayoutParameters layoutParameters)
        {
            if (layoutParameters is null)
            {
                throw new ArgumentNullException(nameof(layoutParameters));
            }

            view.SetLayoutParameters(layoutParameters);
            AddSubview(view);
        }

        public override CGSize SizeThatFitsSubviews(CGSize size)
        {
            throw new NotImplementedException();
        }

        public override void LayoutSubviews()
        {
            base.LayoutSubviews();

            Console.WriteLine(string.Join("\n", Subviews.Select(x => x.Frame)));
        }

        protected virtual LayoutParameters GetLayoutParameters(UIView uiView)
        {
            ViewGroup.LayoutParameters layoutParameters = uiView.GetLayoutParameters();

            if (layoutParameters is LayoutParameters)
            {
                return (LayoutParameters)layoutParameters;
            }
            else if (layoutParameters == null)
            {
                throw new LayoutParametersNotFoundException("No layout parameters found for the given child view.");
            }
            else
            {
                throw new InvalidLayoutParametersException($"Layout parameters must be of type {typeof(LayoutParameters)}, found one of type {layoutParameters.GetType()}.");
            }
        }

        protected void PerformConstraintSetUpdate(UIView view, IEnumerable<NSLayoutConstraint> constraints)
        {
            List<NSLayoutConstraint> constraintsToAdd = new List<NSLayoutConstraint>();
            List<NSLayoutConstraint> constraintsToRemove = new List<NSLayoutConstraint>();

            foreach (NSLayoutConstraint existingConstraint in view.Constraints)
            {
                if (!existingConstraint.Active || !constraints.Any(x => 
                    x.FirstItem == existingConstraint.FirstItem &&
                    x.SecondItem == existingConstraint.SecondItem &&
                    x.FirstAttribute == existingConstraint.FirstAttribute &&
                    x.SecondAttribute == existingConstraint.SecondAttribute &&
                    x.Relation == existingConstraint.Relation &&
                    x.Multiplier == existingConstraint.Multiplier &&
                    x.Constant == existingConstraint.Constant))
                {
                    existingConstraint.Active = false;
                    constraintsToRemove.Add(existingConstraint);
                }
            }

            foreach (NSLayoutConstraint newConstraint in constraints)
            {
                NSLayoutConstraint existingConstraint = view.Constraints.FirstOrDefault(x =>
                    x.FirstItem == newConstraint.FirstItem &&
                    x.SecondItem == newConstraint.SecondItem &&
                    x.FirstAttribute == newConstraint.FirstAttribute &&
                    x.SecondAttribute == newConstraint.SecondAttribute &&
                    x.Relation == newConstraint.Relation &&
                    x.Multiplier == newConstraint.Multiplier &&
                    x.Constant == newConstraint.Constant &&
                    x.Active);

                if (existingConstraint != null)
                {
                    newConstraint.Active = true;
                    constraintsToAdd.Add(newConstraint);
                }
            }

            view.RemoveConstraints(constraintsToRemove.ToArray());
            view.AddConstraints(constraintsToAdd.ToArray());
        }

        public new class LayoutParameters : ViewGroup.LayoutParameters
        {
            protected UIView mTopToTop;
            public UIView TopToTop { get => mTopToTop; set { if (mTopToTop != value) { mTopToTop = value; NotifyParametersChanged(); } } }
            protected UIView mTopToBottom;
            public UIView TopToBottom { get => mTopToBottom; set { if (mTopToBottom != value) { mTopToBottom = value; NotifyParametersChanged(); } } }
            protected UIView mBottomToTop;
            public UIView BottomToTop { get => mBottomToTop; set { if (mBottomToTop != value) { mBottomToTop = value; NotifyParametersChanged(); } } }
            protected UIView mBottomToBottom;
            public UIView BottomToBottom { get => mBottomToBottom; set { if (mBottomToBottom != value) { mBottomToBottom = value; NotifyParametersChanged(); } } }
            protected UIView mLeftToLeft;
            public UIView LeftToLeft { get => mLeftToLeft; set { if (mLeftToLeft != value) { mLeftToLeft = value; NotifyParametersChanged(); } } }
            protected UIView mLeftToRight;
            public UIView LeftToRight { get => mLeftToRight; set { if (mLeftToRight != value) { mLeftToRight = value; NotifyParametersChanged(); } } }
            protected UIView mRightToLeft;
            public UIView RightToLeft { get => mRightToLeft; set { if (mRightToLeft != value) { mRightToLeft = value; NotifyParametersChanged(); } } }
            protected UIView mRightToRight;
            public UIView RightToRight { get => mRightToRight; set { if (mRightToRight != value) { mRightToRight = value; NotifyParametersChanged(); } } }

            public LayoutParameters(int width, int height) : base(width, height) { }

            public IEnumerable<NSLayoutConstraint> ToLayoutConstraints(ConstraintLayout parent, UIView view)
            {
                List<NSLayoutConstraint> result = new List<NSLayoutConstraint>();

                if (TopToTop != null)
                {
                    result.Add(NSLayoutConstraint.Create(view, NSLayoutAttribute.Top, NSLayoutRelation.Equal, TopToTop, NSLayoutAttribute.Top, 1, MarginTop));
                }
                
                if (TopToBottom != null)
                {
                    result.Add(NSLayoutConstraint.Create(view, NSLayoutAttribute.Top, NSLayoutRelation.Equal, TopToBottom, NSLayoutAttribute.Bottom, 1, MarginTop));
                }

                if (BottomToTop != null)
                {
                    result.Add(NSLayoutConstraint.Create(view, NSLayoutAttribute.Bottom, NSLayoutRelation.Equal, BottomToTop, NSLayoutAttribute.Top, 1, -MarginBottom));
                }

                if (BottomToBottom != null)
                {
                    result.Add(NSLayoutConstraint.Create(view, NSLayoutAttribute.Bottom, NSLayoutRelation.Equal, BottomToBottom, NSLayoutAttribute.Bottom, 1, -MarginBottom));
                }

                if (LeftToLeft != null)
                {
                    result.Add(NSLayoutConstraint.Create(view, NSLayoutAttribute.Left, NSLayoutRelation.Equal, LeftToLeft, NSLayoutAttribute.Left, 1, MarginLeft));
                }

                if (LeftToRight != null)
                {
                    result.Add(NSLayoutConstraint.Create(view, NSLayoutAttribute.Left, NSLayoutRelation.Equal, LeftToRight, NSLayoutAttribute.Right, 1, MarginLeft));
                }

                if (RightToLeft != null)
                {
                    result.Add(NSLayoutConstraint.Create(view, NSLayoutAttribute.Right, NSLayoutRelation.Equal, RightToLeft, NSLayoutAttribute.Left, 1, -MarginRight));
                }

                if (RightToRight != null)
                {
                    result.Add(NSLayoutConstraint.Create(view, NSLayoutAttribute.Right, NSLayoutRelation.Equal, RightToRight, NSLayoutAttribute.Right, 1, -MarginRight));
                }

                if (Width >= 0)
                {
                    result.Add(NSLayoutConstraint.Create(view, NSLayoutAttribute.Width, NSLayoutRelation.Equal, 1, Width));
                }
                else if (Width == ViewGroup.LayoutParameters.MatchParent)
                {
                    result.Add(NSLayoutConstraint.Create(view, NSLayoutAttribute.Width, NSLayoutRelation.Equal, parent, NSLayoutAttribute.Width, 1, -MarginLeft - MarginRight));
                }
                else if (Width == ViewGroup.LayoutParameters.WrapContent)
                {
                    CGSize measuredSize = parent.MeasureViewSize(parent.Frame.Size, view);
                    result.Add(NSLayoutConstraint.Create(view, NSLayoutAttribute.Width, NSLayoutRelation.Equal, 1, measuredSize.Width));
                }

                if (Height >= 0)
                {
                    result.Add(NSLayoutConstraint.Create(view, NSLayoutAttribute.Height, NSLayoutRelation.Equal, 1, Height));
                }
                else if (Height == ViewGroup.LayoutParameters.MatchParent)
                {
                    result.Add(NSLayoutConstraint.Create(view, NSLayoutAttribute.Height, NSLayoutRelation.Equal, parent, NSLayoutAttribute.Height, 1, -MarginTop - MarginBottom));
                }
                else if (Height == ViewGroup.LayoutParameters.WrapContent)
                {
                    CGSize measuredSize = parent.MeasureViewSize(parent.Frame.Size, view);
                    result.Add(NSLayoutConstraint.Create(view, NSLayoutAttribute.Height, NSLayoutRelation.Equal, 1, measuredSize.Height));
                }

                return result;
            }
        }
    }
}