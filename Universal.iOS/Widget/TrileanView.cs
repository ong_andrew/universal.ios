﻿using CoreGraphics;
using Foundation;
using System;
using System.ComponentModel;
using System.Reflection;
using UIKit;
using Universal.Common.Forms;
using Universal.iOS.Extensions;
using Universal.iOS.Views;

namespace Universal.iOS.Widget
{
    [Register("TrileanView")]
    [DesignTimeVisible(true)]
    public class TrileanView : LinearLayout
    { 
        public event EventHandler ValueChanged;

        protected Trilean? mValue;
        public Trilean? Value
        {
            get => mValue;
            set
            {
                if (mValue != value)
                {
                    Trilean? oldValue = mValue;
                    mValue = value;

                    RenderButtons();
                    ValueChanged?.Invoke(this, new EventArgs());
                }
            }
        }

        /// <summary>
        /// Gets or sets the text.
        /// </summary>
        public virtual string Text
        {
            get => Label.Text;
            set
            {
                if (Label.Text != value)
                {
                    Label.Text = value;
                    SetNeedsLayout();
                }
            }
        }

        /// <summary>
        /// Gets or sets the text color.
        /// </summary>
        public virtual UIColor TextColor
        {
            get => Label.TextColor;
            set
            {
                if (Label.TextColor != value)
                {
                    Label.TextColor = value;
                }
            }
        }

        protected UIColor mControlNormalColor;
        /// <summary>
        /// Gets or sets the color to use when the control is in the normal state.
        /// </summary>
        public virtual UIColor ControlNormalColor
        {
            get => mControlNormalColor;
            set
            {
                if (mControlNormalColor != value)
                {
                    mControlNormalColor = value;
                    RenderButtons();
                }
            }
        }

        protected UILabel Label { get; set; }
        protected UIButton ButtonYes { get; set; }
        protected UIButton ButtonNo { get; set; }
        protected UIButton ButtonNA { get; set; }

        public TrileanView() : base() { Initialize(); }
        public TrileanView(CGRect frame) : base(frame) { Initialize(); }
        public TrileanView(IntPtr handle) : base(handle) { Initialize(); }
        public TrileanView(NSCoder coder) : base(coder) { Initialize(); }
        public TrileanView(NSObjectFlag t) : base(t) { Initialize(); }

        private void Initialize()
        {
            mControlNormalColor = UIColor.LabelColor;
            Orientation = Orientation.Horizontal;
            Gravity = GravityFlags.CenterVertical;

            Label = new TextView()
            {
                Font = UIFont.PreferredBody
            };

            ButtonYes = new UIButton();
            ButtonYes.Layer.BorderWidth = 1.0f;
            ButtonNo = new UIButton();
            ButtonNo.Layer.BorderWidth = 1.0f;
            ButtonNA = new UIButton();
            ButtonNA.Layer.BorderWidth = 1.0f;

            AddSubview(Label, new LinearLayout.LayoutParameters(0, ViewGroup.LayoutParameters.WrapContent, 1)
            {
                MarginBottom = 2,
                MarginLeft = 2,
                MarginTop = 2,
                MarginRight = 2
            });
            AddSubview(ButtonYes, new LinearLayout.LayoutParameters(ViewGroup.LayoutParameters.WrapContent, ViewGroup.LayoutParameters.WrapContent)
            {
                MarginBottom = 2,
                MarginLeft = 2,
                MarginTop = 2,
                MarginRight = 2,
                Gravity = GravityFlags.CenterVertical
            });
            AddSubview(ButtonNo, new LinearLayout.LayoutParameters(ViewGroup.LayoutParameters.WrapContent, ViewGroup.LayoutParameters.WrapContent)
            {
                MarginBottom = 2,
                MarginLeft = 2,
                MarginTop = 2,
                MarginRight = 2,
                Gravity = GravityFlags.CenterVertical
            });
            AddSubview(ButtonNA, new LinearLayout.LayoutParameters(ViewGroup.LayoutParameters.WrapContent, ViewGroup.LayoutParameters.WrapContent)
            {
                MarginBottom = 2,
                MarginLeft = 2,
                MarginTop = 2,
                MarginRight = 2,
                Gravity = GravityFlags.CenterVertical
            });

            ButtonYes.TouchUpInside += (sender, eventArgs) =>
            {
                if (Value == Trilean.True)
                {
                    Value = null;
                }
                else
                {
                    Value = Trilean.True;
                }
            };

            ButtonNo.TouchUpInside += (sender, eventArgs) =>
            {
                if (Value == Trilean.False)
                {
                    Value = null;
                }
                else
                {
                    Value = Trilean.False;
                }
            };

            ButtonNA.TouchUpInside += (sender, eventArgs) =>
            {
                if (Value == Trilean.Indeterminate)
                {
                    Value = null;
                }
                else
                {
                    Value = Trilean.Indeterminate;
                }
            };
            RenderButtons();
        }

        protected void RenderButtons()
        {
            Assembly thisAssembly = Assembly.GetExecutingAssembly();
            string assemblyName = thisAssembly.GetName().Name;

            ButtonYes.Layer.BorderColor = ControlNormalColor.CGColor;
            ButtonYes.SetBackgroundImage(UIImage.FromResource(thisAssembly, assemblyName + ".Resources." + "baseline_check_black_24dp.png").ImageWithTint(ControlNormalColor), UIControlState.Normal);

            ButtonNo.Layer.BorderColor = ControlNormalColor.CGColor;
            ButtonNo.SetBackgroundImage(UIImage.FromResource(thisAssembly, assemblyName + ".Resources." + "baseline_clear_black_24dp.png").ImageWithTint(ControlNormalColor), UIControlState.Normal);

            ButtonNA.Layer.BorderColor = ControlNormalColor.CGColor;
            ButtonNA.SetBackgroundImage(UIImage.FromResource(thisAssembly, assemblyName + ".Resources." + "baseline_remove_black_24dp.png").ImageWithTint(ControlNormalColor), UIControlState.Normal);

            if (Value == null)
            {
                ButtonYes.BackgroundColor = UIColor.Clear;
                ButtonNo.BackgroundColor = UIColor.Clear;
                ButtonNA.BackgroundColor = UIColor.Clear;
            }
            else if (Value == Trilean.True)
            {
                ButtonYes.BackgroundColor = UIColor.Green;
                ButtonNo.BackgroundColor = UIColor.Clear;
                ButtonNA.BackgroundColor = UIColor.Clear;
            }
            else if (Value == Trilean.False)
            {
                ButtonYes.BackgroundColor = UIColor.Clear;
                ButtonNo.BackgroundColor = UIColor.Red;
                ButtonNA.BackgroundColor = UIColor.Clear;
            }
            else
            {
                ButtonYes.BackgroundColor = UIColor.Clear;
                ButtonNo.BackgroundColor = UIColor.Clear;
                ButtonNA.BackgroundColor = UIColor.DarkGray;
            }
        }

        public override void LayoutSubviews()
        {
            base.LayoutSubviews();
            ButtonYes.Layer.CornerRadius = ButtonYes.Layer.Frame.Width / 2;
            ButtonNo.Layer.CornerRadius = ButtonNo.Layer.Frame.Width / 2;
            ButtonNA.Layer.CornerRadius = ButtonNA.Layer.Frame.Width / 2;
        }
    }
}
