﻿using CoreGraphics;
using Foundation;
using System;
using System.ComponentModel;
using System.Reflection;
using UIKit;
using Universal.Common.Forms;
using Universal.iOS.Extensions;
using Universal.iOS.Views;

namespace Universal.iOS.Widget
{
    /// <summary>
    /// A simple expandable <see cref="Widget.TrileanView" />.
    /// </summary>
    [Register("ExpandableTrileanView")]
    [DesignTimeVisible(true)]
    public class ExpandableTrileanView : LinearLayout
    {
        /// <summary>
        /// Raised when the underlying <see cref="Widget.TrileanView" />'s value is changed or if the comment is changed.
        /// </summary>
        public event EventHandler ValueChanged;

        /// <summary>
        /// Gets or sets the text on the <see cref="ExpandableTrileanView" />.
        /// </summary>
        public string Text
        {
            get => TrileanView.Text;
            set
            {
                TrileanView.Text = value;
            }
        }

        /// <summary>
        /// Gets or sets the text color.
        /// </summary>
        public virtual UIColor TextColor
        {
            get => TrileanView.TextColor;
            set
            {
                TrileanView.TextColor = value;
            }
        }

        protected UIColor mControlNormalColor;
        /// <summary>
        /// Gets or sets the color to use when the control is in the normal state.
        /// </summary>
        public virtual UIColor ControlNormalColor
        {
            get => mControlNormalColor;
            set
            {
                if (mControlNormalColor != value)
                {
                    mControlNormalColor = value;
                    TrileanView.ControlNormalColor = value;
                    TextField.ControlNormalColor = value;
                    RenderButtons();
                }
            }
        }

        /// <summary>
        /// Gets or sets the color to use for accents.
        /// </summary>
        public virtual UIColor AccentColor
        {
            get => TextField.AccentColor;
            set => TextField.AccentColor = value;
        }


        /// <summary>
        /// Gets or sets the color to use for placeholder in the text section.
        /// </summary>
        public virtual UIColor PlaceholderColor
        {
            get => TextField.PlaceholderColor;
            set => TextField.PlaceholderColor = value;
        }

        /// <summary>
        /// Gets or sets the comment on the expandable section of the <see cref="ExpandableTrileanView" />.
        /// </summary>
        public string Comment
        {
            get => TextField.Text;
            set
            {
                if (TextField.Text != value)
                {
                    TextField.Text = value;
                    ValueChanged?.Invoke(this, new EventArgs());
                }
            }
        }

        public Trilean? Value
        {
            get => TrileanView.Value;
            set
            {
                if (TrileanView.Value != value)
                {
                    TrileanView.Value = value;
                    ValueChanged?.Invoke(this, new EventArgs());
                }
            }
        }

        protected bool mIsExpanded;
        /// <summary>
        /// Gets or sets a value indicating if the <see cref="ExpandableTrileanView" /> is expanded.
        /// </summary>
        public bool IsExpanded
        {
            get => mIsExpanded;
            set
            {
                if (mIsExpanded != value)
                {
                    mIsExpanded = value;
                    if (mIsExpanded)
                    {
                        AddSubview(TextField, new LinearLayout.LayoutParameters(ViewGroup.LayoutParameters.MatchParent, ViewGroup.LayoutParameters.WrapContent)
                        {
                            MarginBottom = 2,
                            MarginLeft = 2,
                            MarginTop = 2,
                            MarginRight = 2,
                        });
                    }
                    else
                    {
                        TextField.RemoveFromSuperview();
                    }

                    SetNeedsLayout();
                }
            }
        }

        /// <summary>
        /// Gets or sets a flag that indicates if return should end editing.
        /// </summary>
        public bool ReturnEndsEditing
        {
            get => TextField.ReturnEndsEditing;
            set
            {
                TextField.ReturnEndsEditing = value;
            }
        }

        protected LinearLayout TopContainer { get; set; }
        protected LinearLayout BottomContainer { get; set; }
        protected TrileanView TrileanView { get; set; }
        protected EditText TextField { get; set; }
        protected UIButton ButtonComment { get; set; }

        public ExpandableTrileanView() : base() { Initialize(); }
        public ExpandableTrileanView(CGRect frame) : base(frame) { Initialize(); }
        public ExpandableTrileanView(IntPtr handle) : base(handle) { Initialize(); }
        public ExpandableTrileanView(NSCoder coder) : base(coder) { Initialize(); }
        public ExpandableTrileanView(NSObjectFlag t) : base(t) { Initialize(); }

        private void Initialize()
        {
            Orientation = Orientation.Vertical;
            Gravity = GravityFlags.CenterVertical;

            Assembly thisAssembly = Assembly.GetExecutingAssembly();
            string assemblyName = thisAssembly.GetName().Name;

            ButtonComment = new UIButton();

            ButtonComment.SetBackgroundImage(UIImage.FromResource(thisAssembly, assemblyName + ".Resources." + "baseline_comment_black_24dp.png").ImageWithTint(UIColor.DarkGray), UIControlState.Normal);

            TrileanView = new TrileanView();

            TopContainer = new LinearLayout();
            TopContainer.Gravity = GravityFlags.CenterVertical;
            TopContainer.Orientation = Orientation.Horizontal;

            BottomContainer = new LinearLayout();
            BottomContainer.Gravity = GravityFlags.CenterVertical;
            BottomContainer.Orientation = Orientation.Horizontal;

            TopContainer.AddSubview(TrileanView, new LinearLayout.LayoutParameters(0, ViewGroup.LayoutParameters.WrapContent, 1)
            {
                MarginBottom = 2,
                MarginLeft = 2,
                MarginTop = 2,
                MarginRight = 2
            });

            TopContainer.AddSubview(ButtonComment, new LinearLayout.LayoutParameters(ViewGroup.LayoutParameters.WrapContent, ViewGroup.LayoutParameters.WrapContent)
            {
                MarginBottom = 2,
                MarginLeft = 2,
                MarginTop = 2,
                MarginRight = 2,
                Gravity = GravityFlags.CenterVertical
            });

            TextField = new EditText()
            {
                Placeholder = "Comments"
            };

            AddSubview(TopContainer, new LinearLayout.LayoutParameters(ViewGroup.LayoutParameters.MatchParent, ViewGroup.LayoutParameters.WrapContent));

            ButtonComment.TouchUpInside += (sender, eventArgs) =>
            {
                InvokeOnMainThread(() => { IsExpanded = !IsExpanded; });
            };
            TrileanView.ValueChanged += (object sender, EventArgs e) =>
            {
                ValueChanged?.Invoke(this, new EventArgs());
            };

            ControlNormalColor = UIColor.LabelColor;
        }

        protected virtual void RenderButtons()
        {
            Assembly thisAssembly = Assembly.GetExecutingAssembly();
            string assemblyName = thisAssembly.GetName().Name;
            ButtonComment.SetBackgroundImage(UIImage.FromResource(thisAssembly, assemblyName + ".Resources." + "baseline_comment_black_24dp.png").ImageWithTint(ControlNormalColor), UIControlState.Normal);
        }
    }
}
