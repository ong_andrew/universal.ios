﻿using AudioToolbox;
using CoreGraphics;
using Foundation;
using MaterialComponents;
using System;
using System.ComponentModel;
using System.Threading;
using System.Threading.Tasks;
using UIKit;

namespace Universal.iOS.Widget
{
    [Register("MovableFloatingActionButton")]
    [DesignTimeVisible(true)]
    public class MovableFloatingActionButton : FloatingButton
    {
        /// <summary>
        /// When set to true, requires the user to long press on the button to initiate a move.
        /// </summary>
        public bool RequireLongPress { get; set; }
        /// <summary>
        /// When set to true and long press is required, vibrates the device when a long press is detected.
        /// </summary>
        public bool VibrateOnLongPress { get; set; }
        /// <summary>
        /// Gets or sets a value in pixels indicating the drag tolerance when long presses are not required.
        /// </summary>
        public nfloat DragTolerance { get; set; }        
        /// <summary>
        /// Raised when the <see cref="MovableFloatingActionButton"/> is moved.
        /// </summary>
        public event EventHandler Moved;
        private nfloat mDownRawX;
        private nfloat mDownRawY;
        private nfloat mXDifferential;
        private nfloat mYDifferential;
        private bool mLongPressed;
        private bool mPressed;
        private CancellationTokenSource mCancellationTokenSource;

        public MovableFloatingActionButton() : base() { Initialize(); }
        public MovableFloatingActionButton(CGRect frame) : base(frame) { Initialize(); }
        public MovableFloatingActionButton(IntPtr handle) : base(handle) { Initialize(); }
        public MovableFloatingActionButton(NSCoder coder) : base(coder) { Initialize(); }
        public MovableFloatingActionButton(NSObjectFlag t) : base(t) { Initialize(); }

        private void Initialize()
        {
            DragTolerance = 10f;
            mPressed = false;
        }

        public override void TouchesBegan(NSSet touches, UIEvent evt)
        {
            CGPoint point = ((UITouch)evt.AllTouches.AnyObject).LocationInView(UIApplication.SharedApplication.KeyWindow);

            mDownRawX = point.X;
            mDownRawY = point.Y;
            mXDifferential = Frame.X - mDownRawX;
            mYDifferential = Frame.Y - mDownRawY;

            Pressed = true;
            if (RequireLongPress)
            {
                mCancellationTokenSource = new CancellationTokenSource();
                Task.Run(async () => { await Task.Delay(500, mCancellationTokenSource.Token); if (!mCancellationTokenSource.Token.IsCancellationRequested) { OnLongPressed(); } });
            }
        }

        public override void TouchesMoved(NSSet touches, UIEvent evt)
        {
            if (!RequireLongPress || (RequireLongPress && mLongPressed))
            {
                CGPoint point = ((UITouch)evt.AllTouches.AnyObject).LocationInView(UIApplication.SharedApplication.KeyWindow);

                nfloat viewWidth = Frame.Width;
                nfloat viewHeight = Frame.Height;

                UIView viewParent = Superview;
                nfloat parentWidth = viewParent.Frame.Width;
                nfloat parentHeight = viewParent.Frame.Height;

                nfloat newX = point.X + mXDifferential;
                newX = (nfloat)Math.Max(0, newX);
                newX = (nfloat)Math.Min(parentWidth - viewWidth, newX);

                nfloat newY = point.Y + mYDifferential;
                newY = (nfloat)Math.Max(0, newY);
                newY = (nfloat)Math.Min(parentHeight - viewHeight, newY);

                if (!RequireLongPress && Selected && Math.Abs(point.X - mDownRawX) > DragTolerance && Math.Abs(point.Y - mDownRawY) > DragTolerance)
                {
                    Pressed = false;
                }

                BeginAnimations("MovableFloatingActionButton");
                CGRect frame = Frame;
                frame.X = newX;
                frame.Y = newY;
                Frame = frame;
                SetAnimationDuration(0);
                CommitAnimations();

                Moved?.Invoke(this, new EventArgs());
            }
        }

        public override void TouchesEnded(NSSet touches, UIEvent evt)
        {
            if (RequireLongPress)
            {
                if (mCancellationTokenSource != null)
                {
                    mCancellationTokenSource.Cancel();
                }
            }

            CGPoint point = ((UITouch)evt.AllTouches.AnyObject).LocationInView(UIApplication.SharedApplication.KeyWindow);

            nfloat upRawX = point.X;
            nfloat upRawY = point.Y;

            nfloat upXDifferential = upRawX - mDownRawX;
            nfloat upYDifferential = upRawY - mDownRawY;

            if (RequireLongPress && !mLongPressed)
            {
                if (Pressed)
                {
                    Pressed = false;
                }
                mLongPressed = false;
                SendActionForControlEvents(UIControlEvent.TouchUpInside);
            }
            else if (!RequireLongPress && Math.Abs(upXDifferential) < DragTolerance && Math.Abs(upYDifferential) < DragTolerance)
            {
                if (Pressed)
                {
                    Pressed = false;
                }
                mLongPressed = false;
                SendActionForControlEvents(UIControlEvent.TouchUpInside);
            }
            else
            {
                if (Pressed)
                {
                    Pressed = false;
                }
                mLongPressed = false;
            }
        }

        protected virtual void OnLongPressed()
        {
            mLongPressed = true;
            if (Pressed)
            {
                Pressed = false;
            }

            if (RequireLongPress && VibrateOnLongPress)
            {
                SystemSound.Vibrate.PlayAlertSound();
            }
        }
        public bool Pressed
        {
            get => mPressed;
            set
            {
                if (mPressed != value)
                {
                    if (value)
                    {
                        InvokeOnMainThread(() =>
                        {
                            BeginAnimations("MovableFloatingActionButtonPressed");
                            Alpha = 0.75f;
                            SetAnimationDuration(0.2);
                            CommitAnimations();
                        });
                    }
                    else
                    {
                        InvokeOnMainThread(() =>
                        {
                            BeginAnimations("MovableFloatingActionButtonNotPressed");
                            Alpha = 1.0f;
                            SetAnimationDuration(0.2);
                            CommitAnimations();
                        });
                    }
                }
                mPressed = value;
            }
        }
    }
}