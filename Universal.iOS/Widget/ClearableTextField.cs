﻿using CoreGraphics;
using Foundation;
using System;
using System.ComponentModel;
using UIKit;

namespace Universal.iOS.Widget
{
    [Register("ClearableTextField")]
    [DesignTimeVisible(true)]
    public class ClearableTextField : UITextField
    {
        /// <summary>
        /// Invoked when the <see cref="Clear()"/> method is used to clear the contents.
        /// </summary>
        public event EventHandler<EventArgs> TextCleared;

        public ClearableTextField() : base() { Initialize(); }
        public ClearableTextField(CGRect frame) : base(frame) { Initialize(); }
        public ClearableTextField(IntPtr handle) : base(handle) { Initialize(); }
        public ClearableTextField(NSCoder coder) : base(coder) { Initialize(); }
        public ClearableTextField(NSObjectFlag t) : base(t) { Initialize(); }

        private void Initialize()
        {
            ClearButtonMode = UITextFieldViewMode.WhileEditing;
        }

        public virtual void Clear()
        {
            Text = string.Empty;
            TextCleared?.Invoke(this, new EventArgs());
        }
    }
}