﻿using AudioToolbox;
using AVFoundation;
using CoreGraphics;
using Foundation;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Threading.Tasks;
using UIKit;
using Universal.Common;
using Universal.Common.Audio;
//using Universal.Common.Extensions;

namespace Universal.iOS.Widget
{
    [Register("AudioRecorderFloatingActionButton")]
    [DesignTimeVisible(true)]
    public class AudioRecorderFloatingActionButton : ToggleFloatingActionButton
    {
        protected bool mStartedRecording;
        protected bool mDetectSilence;
        /// <summary>
        /// Gets or sets a value indicating if the <see cref="Silence"/> event should be fired.
        /// </summary>
        public bool DetectSilence
        {
            get => mDetectSilence;
            set
            {
                mDetectSilence = value;
                if (mDetectSilence && mSilenceInterval == 0)
                {
                    mSilenceInterval = 2;
                }
            }
        }
        protected double mSilenceInterval;
        /// <summary>
        /// Sets the duration in seconds to consider silence.
        /// </summary>
        public double SilenceInterval
        {
            get => mSilenceInterval;
            set
            {
                mSilenceInterval = value;
                if (mSilenceDuration > 0 && !DetectSilence)
                {
                    DetectSilence = true;
                }
            }
        }
        protected bool mStopOnSilence;
        /// <summary>
        /// Gets or sets a flag indicating if the recording should be stopped if silence is detected.
        /// </summary>
        public bool StopOnSilence
        {
            get => mStopOnSilence;
            set
            {
                mStopOnSilence = value;
                if (mStopOnSilence && !DetectSilence)
                {
                    DetectSilence = true;
                }
            }
        }

        protected List<PcmChunk> SilenceChunkBuffer { get; set; }
        protected PcmChunker PcmChunker { get; set; }

        protected List<double> mRmsValues { get; set; }
        protected double mSilenceDuration { get; set; }
        protected double mElapsedDuration { get; set; }
        protected bool mSilenceFired { get; set; }

        /// <summary>
        /// Raised when audio recording is started.
        /// </summary>
        public event EventHandler RecordingStarted;
        /// <summary>
        /// Raised when audio data is available.
        /// </summary>
        public event EventHandler<AudioRecordedEventArgs> AudioRecorded;
        /// <summary>
        /// Raised when an audio data buffer is available.
        /// </summary>
        public event EventHandler<BufferAvailableEventArgs> BufferAvailable;
        /// <summary>
        /// Raised when a silence-based stop is triggered.
        /// </summary>
        public event EventHandler<EventArgs> Silence;

        private MemoryStream mAudioInputMemoryStream;
        protected MemoryStream AudioInputMemoryStream
        {
            get => mAudioInputMemoryStream;
            set
            {
                mAudioInputMemoryStream = value;
                AudioInputSynchronizedStream = Stream.Synchronized(mAudioInputMemoryStream);
            }
        }
        protected Stream AudioInputSynchronizedStream { get; private set; }
        protected InputAudioQueue InputAudioQueue { get; set; }

        public virtual int BufferSize { get; set; } = 1024;
        public virtual int BufferCount { get; set; } = 16;

        protected int mSampleRate;
        /// <summary>
        /// Sample rate of the recorded audio.
        /// </summary>
        public int SampleRate
        {
            get => mSampleRate;
            protected set
            {
                mSampleRate = value;
            }
        }

        protected int mBitDepth;
        /// <summary>
        /// Bit depth of the recorded audio, ie. bits per channel.
        /// </summary>
        public int BitDepth
        {
            get => mBitDepth;
            protected set
            {
                mBitDepth = value;
            }
        }
        
        protected int mChannels;
        /// <summary>
        /// Number of channels in the recorded audio.
        /// </summary>
        public int Channels
        {
            get => mChannels;
            protected set
            {
                mChannels = value;
            }
        }

        public AudioRecorderFloatingActionButton() : base() { Initialize(); }
        public AudioRecorderFloatingActionButton(CGRect frame) : base(frame) { Initialize(); }
        public AudioRecorderFloatingActionButton(IntPtr handle) : base(handle) { Initialize(); }
        public AudioRecorderFloatingActionButton(NSCoder coder) : base(coder) { Initialize(); }
        public AudioRecorderFloatingActionButton(NSObjectFlag t) : base(t) { Initialize(); }

        private void Initialize()
        {
            mStartedRecording = false;

            SilenceChunkBuffer = new List<PcmChunk>();
            mRmsValues = new List<double>();

            ImageNormal = UIImage.FromBundle("baseline_mic_black_24dp").ImageWithRenderingMode(UIImageRenderingMode.AlwaysTemplate);
            ImageToggled = UIImage.FromBundle("baseline_stop_black_24dp").ImageWithRenderingMode(UIImageRenderingMode.AlwaysTemplate);

            if (ImageNormal == null || ImageToggled == null)
            {
                throw new InitializationException("Could not load image assets.");
            }

            ToggleChanged += (sender, eventArgs) =>
            {
                if (eventArgs.Toggled)
                {
                    if (AVAudioSession.SharedInstance().RecordPermission == AVAudioSessionRecordPermission.Granted)
                    {
                        StartRecording();
                    }
                    else
                    {
                        AVAudioSession.SharedInstance().RequestRecordPermission((granted) =>
                        {
                            if (!granted)
                            { 
                                Toggled = false;
                            }
                        });
                    }
                }
                else
                {
                    if (mStartedRecording)
                    {
                        mStartedRecording = false;
                        StopRecording();
                        Task.Run(async () =>
                        {
                            using (MemoryStream outputStream = new MemoryStream())
                            {
                                AudioInputMemoryStream.Position = 0;
                                await AudioInputSynchronizedStream.CopyToAsync(outputStream);
                                AudioRecorded?.Invoke(this, new AudioRecordedEventArgs(outputStream.ToArray()));
                            }
                        });
                    }
                }
            };

            BufferAvailable += OnBufferAvailable;
            Silence += OnSilence;
        }

        protected virtual void OnBufferAvailable(object sender, BufferAvailableEventArgs eventArgs)
        {
            if (DetectSilence)
            {
                lock (SilenceChunkBuffer)
                {
                    SilenceChunkBuffer.AddRange(PcmChunker.ProcessBytes(eventArgs.Buffer));
                    double availableDuration = SilenceChunkBuffer.Count * PcmChunker.DurationPerChunk;
                    if (availableDuration > 0.1)
                    {
                        mElapsedDuration += availableDuration;

                        double rms = Math.Sqrt(SilenceChunkBuffer.Average(x => Math.Pow(x.NormalizedMeanLevel, 2)));
                        lock (mRmsValues)
                        {
                            mRmsValues.Add(rms);
                        }

                        double bottom = mRmsValues.Min();
                        double top = mRmsValues.OrderBy(x => x).ElementAtPercentileInterpolated(0.9);
                        double range = top - bottom;

                        if (rms < bottom + (0.3 * range))
                        {
                            mSilenceDuration += availableDuration;
                        }
                        else
                        {
                            mSilenceDuration = 0;
                        }

                        if (mSilenceDuration > SilenceInterval && !mSilenceFired)
                        {
                            mSilenceFired = true;
                            Silence?.Invoke(this, new EventArgs());
                        }

                        SilenceChunkBuffer.Clear();
                    }
                }
            }
        }

        protected virtual void OnSilence(object sender, EventArgs eventArgs)
        {
            if (StopOnSilence)
            {
                Toggled = false;
            }
        }

        protected virtual void StartRecording()
        {
            mStartedRecording = true;
            AudioInputMemoryStream = new MemoryStream();
            AVAudioSession session = AVAudioSession.SharedInstance();
            if (session.Category != AVAudioSession.CategoryRecord && session.Category != AVAudioSession.CategoryPlayAndRecord)
            {
                NSError setCategoryError = session.SetCategory(AVAudioSession.CategoryRecord);
                if (setCategoryError != null)
                {
                    throw new InitializationException($"Could not set audio session category: {setCategoryError.LocalizedDescription}");
                }
            }
            
            NSError setActiveError = session.SetActive(true);
            if (setActiveError != null)
            {
                throw new InitializationException($"Could not set audio session to active: {setActiveError.LocalizedDescription}");
            }

            if (!session.InputAvailable)
            {
                throw new InitializationException("No input channels available.");
            }

            AudioStreamBasicDescription audioStreamBasicDescription = new AudioStreamBasicDescription()
            {
                SampleRate = AudioSession.CurrentHardwareSampleRate,
                Format = AudioFormatType.LinearPCM,
                FormatFlags = AudioFormatFlags.LinearPCMIsSignedInteger | AudioFormatFlags.LinearPCMIsPacked,
                BytesPerPacket = 2,
                FramesPerPacket = 1,
                BytesPerFrame = 2,
                ChannelsPerFrame = 1,
                BitsPerChannel = 16,
                Reserved = 0
            };

            SampleRate = (int)audioStreamBasicDescription.SampleRate;
            BitDepth = audioStreamBasicDescription.BitsPerChannel;
            Channels = audioStreamBasicDescription.ChannelsPerFrame;

            PcmChunker = new PcmChunker(SampleRate, BitDepth, Channels);
            mElapsedDuration = 0;
            mSilenceDuration = 0;
            mSilenceFired = false;

            InputAudioQueue = new InputAudioQueue(audioStreamBasicDescription);
            for (int count = 0; count < BufferCount; count++)
            {
                IntPtr bufferPointer;
                InputAudioQueue.AllocateBuffer(BufferSize, out bufferPointer);
                InputAudioQueue.EnqueueBuffer(bufferPointer, BufferSize, null);
            }

            InputAudioQueue.InputCompleted += (sender, eventArgs) =>
            {
                byte[] buffer = new byte[(int)eventArgs.Buffer.AudioDataByteSize];
                Marshal.Copy(eventArgs.Buffer.AudioData, buffer, 0, buffer.Length);
                AudioInputSynchronizedStream.Write(buffer, 0, buffer.Length);
                BufferAvailable?.Invoke(this, new BufferAvailableEventArgs(buffer));

                InputAudioQueue.EnqueueBuffer(eventArgs.IntPtrBuffer, BufferSize, null);
            };

            AudioQueueStatus status = InputAudioQueue.Start();
            int attemptsToStart = 0;
            while (status != AudioQueueStatus.Ok && attemptsToStart < 1000)
            {
                status = InputAudioQueue.Start();
                attemptsToStart++;
            }

            if (status == AudioQueueStatus.Ok)
            {
                RecordingStarted?.Invoke(this, new EventArgs());
            }
            else
            {
                throw new InvalidOperationException($"Could not start audio input queue: {status}");
            }
        }

        protected virtual void StopRecording()
        {
            if (InputAudioQueue == null)
            {
                throw new InvalidOperationException("InputAudioQueue is null.");
            }

            AudioQueueStatus audioQueueStatus = InputAudioQueue.Stop(true);
            if (audioQueueStatus != AudioQueueStatus.Ok)
            {
                throw new InvalidOperationException($"Could not stop input audio queue: {audioQueueStatus}");
            }
        }

        public class AudioRecordedEventArgs : EventArgs
        {
            public byte[] AudioBytes { get; protected set; }
            public AudioRecordedEventArgs(byte[] audioBytes)
            {
                AudioBytes = audioBytes;
            }
        }

        public class BufferAvailableEventArgs : EventArgs
        {
            public byte[] Buffer { get; protected set; }
            public BufferAvailableEventArgs(byte[] buffer)
            {
                Buffer = buffer;
            }
        }
    }
}