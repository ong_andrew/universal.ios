﻿using CoreGraphics;
using Foundation;
using System;
using UIKit;

namespace Universal.iOS.Widget
{
    /// <summary>
    /// A <see cref="UIButton" /> with convenience methods.
    /// </summary>
    public class Button : UIButton
    {
        /// <summary>
        /// Gets or sets the button text.
        /// </summary>
        public string Text
        {
            get => Title(UIControlState.Normal);
            set
            {
                SetTitle(value, UIControlState.Normal);
            }
        }

        /// <summary>
        /// Gets or sets the button text color.
        /// </summary>
        public UIColor TextColor
        {
            get => TitleColor(UIControlState.Normal);
            set
            {
                SetTitleColor(value, UIControlState.Normal);
            }
        }

        /// <summary>
        /// Gets or sets the text shadow color.
        /// </summary>
        public UIColor TextShadowColor
        {
            get => TitleShadowColor(UIControlState.Normal);
            set
            {
                SetTitleShadowColor(value, UIControlState.Normal);
            }
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Button" /> class.
        /// </summary>
        public Button() : base() { Initialize(); }
        /// <summary>
        /// Initializes a new instance of the <see cref="Button" /> class.
        /// </summary>
        /// <param name="frame"></param>
        public Button(CGRect frame) : base(frame) { Initialize(); }
        /// <summary>
        /// Initializes a new instance of the <see cref="Button" /> class.
        /// </summary>
        /// <param name="handle"></param>
        public Button(IntPtr handle) : base(handle) { Initialize(); }
        /// <summary>
        /// Initializes a new instance of the <see cref="Button" /> class.
        /// </summary>
        /// <param name="coder"></param>
        public Button(NSCoder coder) : base(coder) { Initialize(); }
        /// <summary>
        /// Initializes a new instance of the <see cref="Button" /> class.
        /// </summary>
        /// <param name="t"></param>
        public Button(NSObjectFlag t) : base(t) { Initialize(); }
        /// <summary>
        /// Initializes a new instance of the <see cref="Button" /> class.
        /// </summary>
        /// <param name="type"></param>
        public Button(UIButtonType type) : base(type) { Initialize(); }

        private void Initialize()
        {
            TextColor = UIColor.SystemBlueColor;
        }
    }
}