﻿using Foundation;
using System;
using UIKit;
using Universal.iOS.Views;

namespace Universal.iOS.Widget
{
    /// <summary>
    /// A <see cref="IUITableViewDelegate"/> that calculates dynamic heights based on <see cref="ViewGroup"/> functionality.
    /// </summary>
    public class TableViewDelegate : NSObject, IUITableViewDelegate
    {
        [Export("tableView:heightForRowAtIndexPath:")]
        public virtual nfloat GetHeightForRow(UITableView tableView, NSIndexPath indexPath)
        {
            UITableViewCell tableViewCell = tableView.Source.GetCell(tableView, indexPath);

            if (tableViewCell is TableViewCell customCell)
            {
                return customCell.SizeThatFits(customCell.Frame.Size).Height;
            }
            else
            {
                return 54;
            }
        }
    }
}