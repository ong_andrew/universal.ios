﻿using CoreGraphics;
using Foundation;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Threading.Tasks;
using UIKit;
using Universal.Common.Extensions;

namespace Universal.iOS.Widget
{
    /// <summary>
    /// A <see cref="ClearableEditText" /> that pops up with a fixed set of filterable selections when a user taps on it.
    /// </summary>
    [Register("PopupFilterableEditText")]
    [DesignTimeVisible(true)]
    public class PopupFilterableEditText : ClearableEditText
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="PopupFilterableEditText" /> class.
        /// </summary>
        public PopupFilterableEditText() : base() { Initialize(); }
        /// <summary>
        /// Initializes a new instance of the <see cref="PopupFilterableEditText" /> class.
        /// </summary>
        /// <param name="frame"></param>
        public PopupFilterableEditText(CGRect frame) : base(frame) { Initialize(); }
        /// <summary>
        /// Initializes a new instance of the <see cref="PopupFilterableEditText" /> class.
        /// </summary>
        /// <param name="handle"></param>
        public PopupFilterableEditText(IntPtr handle) : base(handle) { Initialize(); }
        /// <summary>
        /// Initializes a new instance of the <see cref="PopupFilterableEditText" /> class.
        /// </summary>
        /// <param name="coder"></param>
        public PopupFilterableEditText(NSCoder coder) : base(coder) { Initialize(); }
        /// <summary>
        /// Initializes a new instance of the <see cref="PopupFilterableEditText" /> class.
        /// </summary>
        /// <param name="t"></param>
        public PopupFilterableEditText(NSObjectFlag t) : base(t) { Initialize(); }

        protected PopupView ContentView { get; set; }
        protected bool ShouldCheckText { get; set; }
        protected bool IsShowingPopup
        {
            get
            {
                return ContentView.Superview != null;
            }
        }
        protected object mValue;
        /// <summary>
        /// Gets or sets the value currently set.
        /// </summary>
        public object Value
        {
            get => mValue;
            set
            {
                if (value == null)
                {
                    mValue = value;
                    SetTextUnchecked(string.Empty);
                }
                else if (Elements != null && Elements.Contains(value))
                {
                    mValue = value;
                    SetTextUnchecked(Projection(mValue));
                }
                else
                {
                    throw new InvalidOperationException("Cannot set the given selection as it is not contained in the element list.");
                }
            }
        }

        protected IEnumerable<object> mElements;
        /// <summary>
        /// Gets or sets the elements being used by the <see cref="PopupFilterableEditText"/>.
        /// </summary>
        public IEnumerable<object> Elements
        {
            get => mElements;
            set
            {
                mElements = value;
                if (Elements != null)
                {
                    if (Value != null)
                    {
                        if (!Elements.Contains(Value))
                        {
                            Value = null;
                        }
                    }
                }
                else
                {
                    if (Value != null)
                    {
                        Value = null;
                    }
                }

                if (IsShowingPopup)
                {
                    ComputeVisibleElements(ContentView.TextField.Text);
                    ContentView.TableView.ReloadData();
                }
            }
        }
        protected Func<object, string> mProjection;
        /// <summary>
        /// Sets the projection function used to map the elements to strings.
        /// When not set, defaults to the <see cref="Object.ToString()"/> method.
        /// </summary>
        public Func<object, string> Projection
        {
            protected get => mProjection;
            set
            {
                mProjection = value;
                ConfigureTableViewSource(mProjection);
            }
        }
        protected List<object> VisibleElements { get; set; }

        /// <summary>
        /// Raised when a user selects an item from the list.
        /// </summary>
        public event EventHandler<ItemSelectedEventArgs> ItemSelected;
        /// <summary>
        /// Raised when a user clears the current selection.
        /// </summary>
        public event EventHandler SelectionCleared;

        private void Initialize()
        {
            Text = string.Empty;

            ShouldCheckText = true;

            ContentView = new PopupView();

            ContentView.ButtonCancel.TouchUpInside += (sender, eventArgs) =>
            {
                OnCancelled();
            };

            VisibleElements = new List<object>();
            mProjection = x => x.ToString();

            ConfigureTableViewSource(mProjection);

            ContentView.TextField.EditingChanged += (sender, eventArgs) =>
            {
                ComputeVisibleElements(ContentView.TextField.Text);
                ContentView.TableView.ReloadData();
            };

            EditingDidBegin += (sender, eventArgs) =>
            {
                OnShow();
            };
            EditingChanged += (sender, eventArgs) =>
            {
                if (ShouldCheckText)
                {
                    if (Text.IsNullOrEmpty())
                    {
                        mValue = null;
                    }
                    else
                    {
                        if (Elements == null || !Elements.Any(x => Projection(x) == Text))
                        {
                            throw new InvalidOperationException("Cannot set the given text as it is not contained in the element list.");
                        }
                        else
                        {
                            mValue = Elements.First(x => Projection(x) == Text);
                        }
                    }
                }
            };

            TextCleared += (sender, eventArgs) =>
            {
                OnSelectionCleared();
            };
        }

        protected virtual void OnCancelled()
        {
            if (IsShowingPopup)
            {
                DismissPopup();
            }
        }

        protected virtual void OnShow()
        {
            if (!IsShowingPopup)
            {
                ShowPopup();
            }
        }

        protected virtual void OnFocusGained()
        {
            if (!IsShowingPopup)
            {
                ShowPopup();
            }
        }

        protected virtual void OnFocusLost()
        {
            if (IsShowingPopup)
            {
                DismissPopup();
            }
        }

        protected virtual void OnItemSelected(object selection)
        {
            if (IsShowingPopup)
            {
                DismissPopup();
            }

            SetTextUnchecked(Projection(selection));
            mValue = selection;
            ItemSelected?.Invoke(this, new ItemSelectedEventArgs(selection));
        }

        protected virtual void OnSelectionCleared()
        {
            SetTextUnchecked(string.Empty);
            mValue = null;
            SelectionCleared?.Invoke(this, new EventArgs());
        }

        protected virtual void ConfigureTableViewSource(Func<object, string> projection)
        {
            RowActionTableViewSource<TextTableViewCell<object>, object> tableViewSource = new RowActionTableViewSource<TextTableViewCell<object>, object>(VisibleElements,
                (reuseIdentifier) =>
                {
                    return new TextTableViewCell<object>(UITableViewCellStyle.Default, reuseIdentifier) { Projection = projection };
                },
                (tableViewCell, item) =>
                {
                    tableViewCell.Item = item;
                });
            tableViewSource.OnRowSelected += (tableView, indexPath) =>
            {
                OnItemSelected(((TextTableViewCell<object>)tableView.CellAt(indexPath)).Item);
            };

            ContentView.TableView.Source = tableViewSource;
        }

        protected virtual void ShowPopup()
        {
            UIWindow uiWindow = UIApplication.SharedApplication.KeyWindow;
            uiWindow.AddSubview(ContentView);

            ContentView.TranslatesAutoresizingMaskIntoConstraints = false;
            ContentView.HeightAnchor.ConstraintEqualTo(uiWindow.HeightAnchor).Active = true;
            ContentView.WidthAnchor.ConstraintEqualTo(uiWindow.WidthAnchor).Active = true;
            ContentView.CenterXAnchor.ConstraintEqualTo(uiWindow.CenterXAnchor).Active = true;
            ContentView.CenterYAnchor.ConstraintEqualTo(uiWindow.CenterYAnchor).Active = true;

            ContentView.TextField.Placeholder = Placeholder;
            ContentView.TextField.Text = Text;
            ContentView.TextField.AccentColor = AccentColor;
            ContentView.TextField.ControlNormalColor = ControlNormalColor;
            ContentView.TextField.ShowFloatingLabel = ShowFloatingLabel;

            if (CanResignFirstResponder)
            {
                ResignFirstResponder();
            }

            Task.Run(async () =>
            {
                await Task.Delay(100);
                InvokeOnMainThread(() =>
                {
                    if (ContentView.TextField.CanBecomeFirstResponder)
                    {
                        ContentView.TextField.BecomeFirstResponder();
                    }
                });
            });
            ComputeVisibleElements(ContentView.TextField.Text);
            ContentView.TableView.ReloadData();
        }

        protected virtual void DismissPopup()
        {
            ContentView.RemoveFromSuperview();
        }

        protected virtual void ComputeVisibleElements(string currentSearchText = null)
        {
            VisibleElements.Clear();
            if (Elements != null)
            {
                if (currentSearchText.IsNullOrEmpty())
                {
                    VisibleElements.AddRange(Elements);
                }
                else
                {
                    VisibleElements.AddRange(Elements.Where(x => Projection(x).Contains(currentSearchText, StringComparison.InvariantCultureIgnoreCase)));
                }
            }
        }

        protected virtual void SetTextUnchecked(string text)
        {
            ShouldCheckText = false;
            Text = text;
            ShouldCheckText = true;
        }

        /// <summary>
        /// Sets the value of the selection to the given object, triggering any listeners.
        /// </summary>
        /// <param name="selection"></param>
        public virtual void Select(object selection)
        {
            Value = selection;
            ItemSelected?.Invoke(this, new ItemSelectedEventArgs(selection));
        }

        public override void Clear()
        {
            Text = string.Empty;
            OnSelectionCleared();
        }

        public class PopupView : UIView
        {
            public UIView ContentViewGroup { get; set; }
            public EditText TextField { get; set; }
            public UITableView TableView { get; set; }
            public UIButton ButtonCancel { get; set; }

            public PopupView() { Initialize(); }

            protected virtual void Initialize()
            {
                TranslatesAutoresizingMaskIntoConstraints = false;
                BackgroundColor = UIColor.FromRGBA(0, 0, 0, 128);

                ContentViewGroup = new UIView();

                AddSubview(ContentViewGroup);

                ContentViewGroup.BackgroundColor = UIColor.SystemBackgroundColor;
                ContentViewGroup.TranslatesAutoresizingMaskIntoConstraints = false;
                ContentViewGroup.WidthAnchor.ConstraintEqualTo(WidthAnchor, 0.75f).Active = true;
                ContentViewGroup.HeightAnchor.ConstraintEqualTo(HeightAnchor, 0.8f).Active = true;
                ContentViewGroup.CenterXAnchor.ConstraintEqualTo(CenterXAnchor).Active = true;
                ContentViewGroup.CenterYAnchor.ConstraintEqualTo(CenterYAnchor).Active = true;

                TextField = new ClearableEditText()
                {
                    Text = string.Empty,
                    ReturnEndsEditing = true
                };
                TextField.TranslatesAutoresizingMaskIntoConstraints = false;
                TextField.SizeToFit();

                TableView = new UITableView();
                TableView.TranslatesAutoresizingMaskIntoConstraints = false;

                ButtonCancel = new UIButton();
                ButtonCancel.SetTitle("Cancel", UIControlState.Normal);
                ButtonCancel.Font = UIFont.BoldSystemFontOfSize(15f);
                ButtonCancel.SetTitleColor(UIColors.DefaultButtonText, UIControlState.Normal);
                ButtonCancel.TranslatesAutoresizingMaskIntoConstraints = false;
                ButtonCancel.SizeToFit();

                ContentViewGroup.AddSubviews(TextField, TableView, ButtonCancel);

                TextField.TopAnchor.ConstraintEqualTo(ContentViewGroup.TopAnchor, 10f).Active = true;
                TextField.LeftAnchor.ConstraintEqualTo(ContentViewGroup.LeftAnchor, 10f).Active = true;
                TextField.RightAnchor.ConstraintEqualTo(ContentViewGroup.RightAnchor, -10f).Active = true;

                ButtonCancel.BottomAnchor.ConstraintEqualTo(ContentViewGroup.BottomAnchor, -10f).Active = true;
                ButtonCancel.RightAnchor.ConstraintEqualTo(ContentViewGroup.RightAnchor, -20f).Active = true;

                TableView.TopAnchor.ConstraintEqualTo(TextField.BottomAnchor, 10f).Active = true;
                TableView.LeftAnchor.ConstraintEqualTo(ContentViewGroup.LeftAnchor, 10f).Active = true;
                TableView.RightAnchor.ConstraintEqualTo(ContentViewGroup.RightAnchor, -10f).Active = true;
                TableView.BottomAnchor.ConstraintEqualTo(ButtonCancel.TopAnchor, -10f).Active = true;

                ContentViewGroup.LayoutIfNeeded();
            }
        }

        public class ItemSelectedEventArgs : EventArgs
        {
            public object Value { get; private set; }
            public ItemSelectedEventArgs(object value)
            {
                Value = value;
            }
        }
    }

    /// <summary>
    /// A <see cref="ClearableEditText" /> that pops up with a fixed set of filterable selections when a user taps on it.
    /// </summary>
    public class PopupFilterableEditText<T> : PopupFilterableEditText
    {
        public PopupFilterableEditText() : base() { Initialize(); }
        public PopupFilterableEditText(CGRect frame) : base(frame) { Initialize(); }
        public PopupFilterableEditText(IntPtr handle) : base(handle) { Initialize(); }
        public PopupFilterableEditText(NSCoder coder) : base(coder) { Initialize(); }
        public PopupFilterableEditText(NSObjectFlag t) : base(t) { Initialize(); }

        private void Initialize() { }

        /// <summary>
        /// Gets the user's current selection.
        /// </summary>
        public new T Value
        {
            get => (T)mValue;
            set
            {
                if (value == null)
                {
                    mValue = value;
                    SetTextUnchecked(string.Empty);
                }
                else if (Elements != null && Elements.Contains(value))
                {
                    mValue = value;
                    SetTextUnchecked(Projection((T)mValue));
                }
                else
                {
                    throw new InvalidOperationException("Cannot set the given selection as it does not belong in the element list.");
                }
            }
        }
        /// <summary>
        /// Gets or sets the elements used in this <see cref="PopupFilterableEditText{T}"/>.
        /// </summary>
        public new IEnumerable<T> Elements { get => base.Elements.Cast<T>(); set => base.Elements = value.Cast<object>(); }
        /// <summary>
        /// Sets the projection function used to map the elements to strings.
        /// </summary>
        public new Func<T, string> Projection
        {
            protected get => x => mProjection(x);
            set
            {
                mProjection = x => value((T)x);
                ConfigureTableViewSource(mProjection);
            }
        }

        /// <summary>
        /// Raised when a user selects an item.
        /// </summary>
        public new event EventHandler<ItemSelectedEventArgs<T>> ItemSelected;

        protected override void OnItemSelected(object selection)
        {
            base.OnItemSelected(selection);
            ItemSelected?.Invoke(this, new ItemSelectedEventArgs<T>((T)selection));
        }

        public class ItemSelectedEventArgs<X> : EventArgs
        {
            public X Value { get; private set; }
            public ItemSelectedEventArgs(X value)
            {
                Value = value;
            }
        }

        public override void Select(object selection)
        {
            base.Select(selection);
            ItemSelected?.Invoke(this, new ItemSelectedEventArgs<T>((T)selection));
        }

        /// <summary>
        /// Sets the value of the selection to the given object, triggering any listeners.
        /// </summary>
        /// <param name="selection"></param>
        public void Select(T selection)
        {
            Select((object)selection);
        }
    }
}