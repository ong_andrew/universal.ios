﻿using CoreGraphics;
using Foundation;
using System;
using System.ComponentModel;
using UIKit;
using Universal.Common.Extensions;
using Universal.iOS.Views;

namespace Universal.iOS.Widget
{
    /// <summary>
    /// A classic check box control widget that can be toggled on and off.
    /// </summary>
    [Register("CheckBox")]
    [DesignTimeVisible(true)]
    public class CheckBox : LinearLayout
    {
        protected UIColor mControlNormalColor;
        /// <summary>
        /// Gets or sets the color to be used when the control is in the normal state.
        /// </summary>
        public virtual UIColor ControlNormalColor
        {
            get => mControlNormalColor;
            set
            {
                if (mControlNormalColor != value)
                {
                    mControlNormalColor = value;
                    CheckBoxSquare.ControlNormalColor = value;
                }
            }
        }

        /// <summary>
        /// Fired when the <see cref="CheckBox" />'s state is changed by a touch gesture.
        /// </summary>
        public event EventHandler ValueChangedByTouch;

        /// <summary>
        /// Gets or sets a value indicating if this <see cref="CheckBox" /> is touchable.
        /// </summary>
        public virtual bool Checked
        {
            get => CheckBoxSquare.Checked;
            set
            {
                if (Checked != value)
                {
                    CheckBoxSquare.Checked = value;
                }
            }
        }

        internal CheckBoxSquare CheckBoxSquare { get; set; }
        public UILabel Label { get; set; }

        public string Text
        {
            get => Label.Text;
            set
            {
                Label.Text = value;
                if (value.IsNullOrEmpty())
                {
                    if (Label.Superview != null)
                    {
                        Label.RemoveFromSuperview();
                    }
                }
                else
                {
                    if (Label.Superview == null)
                    {
                        AddSubview(Label, new LinearLayout.LayoutParameters(0, ViewGroup.LayoutParameters.WrapContent, 1)
                        {
                            Gravity = GravityFlags.CenterVertical,
                            MarginBottom = 2,
                            MarginLeft = 2,
                            MarginTop = 2,
                            MarginRight = 2
                        });
                    }
                }
            }
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="CheckBox"/> class.
        /// </summary>
        public CheckBox() { Initialize(); }
        /// <summary>
        /// Initializes a new instance of the <see cref="CheckBox"/> class.
        /// </summary>
        /// <param name="coder"></param>
        public CheckBox(NSCoder coder) : base(coder) { Initialize(); }
        /// <summary>
        /// Initializes a new instance of the <see cref="CheckBox"/> class.
        /// </summary>
        /// <param name="ptr"></param>
        public CheckBox(IntPtr ptr) : base(ptr) { Initialize(); }
        /// <summary>
        /// Initializes a new instance of the <see cref="CheckBox"/> class.
        /// </summary>
        /// <param name="frame"></param>
        public CheckBox(CGRect frame) : base(frame) { Initialize(); }
        /// <summary>
        /// Initializes a new instance of the <see cref="CheckBox"/> class.
        /// </summary>
        /// <param name="t"></param>
        public CheckBox(NSObjectFlag t) : base(t) { Initialize(); }

        private void Initialize()
        {
            Orientation = Orientation.Horizontal;
            CheckBoxSquare = new CheckBoxSquare();
            AddSubview(CheckBoxSquare, new LinearLayout.LayoutParameters(ViewGroup.LayoutParameters.WrapContent, ViewGroup.LayoutParameters.WrapContent)
            {
                Gravity = GravityFlags.CenterVertical,
                MarginBottom = 2,
                MarginLeft = 2,
                MarginRight = 2,
                MarginTop = 2
            });
            Label = new TextView();

            AddGestureRecognizer(new UITapGestureRecognizer(() =>
            {
                Checked = !Checked;
                ValueChangedByTouch?.Invoke(this, new EventArgs());
            }));

            ControlNormalColor = UIColor.LabelColor;
        }
    }
}