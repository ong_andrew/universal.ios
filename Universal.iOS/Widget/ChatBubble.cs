﻿using CoreGraphics;
using Foundation;
using System;
using UIKit;
using Universal.iOS.Extensions;
using Universal.iOS.Views;

namespace Universal.iOS.Widget
{
    /// <summary>
    /// A view that represents a chat bubble with a tail.
    /// </summary>
    public class ChatBubble : UIView, IViewGroup
    {
        protected UIColor mControlNormalColor;
        public virtual UIColor ControlNormalColor
        {
            get => mControlNormalColor;
            set
            {
                if (mControlNormalColor != value)
                {
                    mControlNormalColor = value;
                }
            }
        }

        protected nfloat mCornerRadius;
        /// <summary>
        /// Gets or sets the corner radius for the content of the <see cref="ChatBubble" />.
        /// </summary>
        public nfloat CornerRadius
        {
            get => mCornerRadius;
            set
            {
                if (mCornerRadius != value)
                {
                    mCornerRadius = value;
                    ContentViewContainer.Layer.CornerRadius = CornerRadius;
                }
            }
        }

        public virtual nfloat PaddingLeft { get => ContentViewContainer.PaddingLeft; set => ContentViewContainer.PaddingLeft = value; }
        public virtual nfloat PaddingRight { get => ContentViewContainer.PaddingRight; set => ContentViewContainer.PaddingRight = value; }
        public virtual nfloat PaddingTop { get => ContentViewContainer.PaddingTop; set => ContentViewContainer.PaddingTop = value; }
        public virtual nfloat PaddingBottom { get => ContentViewContainer.PaddingBottom; set => ContentViewContainer.PaddingBottom = value; }

        protected GravityFlags mGravity;
        /// <summary>
        /// Gets or sets the gravity of the <see cref="ChatBubble" />.
        /// </summary>
        public GravityFlags Gravity
        {
            get => mGravity;
            set
            {
                if (mGravity != value)
                {
                    mGravity = value;
                    ChatBubbleTail.Gravity = value;
                }
            }
        }
        internal virtual ChatBubbleTriangle ChatBubbleTail { get; set; }
        protected LinearLayout ContentViewContainer { get; set; }

        protected UIView mContentView;
        /// <summary>
        /// Gets or sets the content view displayed by the <see cref="ChatBubble" />.
        /// </summary>
        public virtual UIView ContentView
        {
            get => mContentView;
            set
            {
                if (mContentView != value)
                {
                    UIView oldView = mContentView;

                    if (oldView != null)
                    {
                        mContentView.RemoveFromSuperview();
                    }

                    mContentView = value;

                    if (mContentView != null)
                    {
                        ContentViewContainer.AddSubview(mContentView, new LinearLayout.LayoutParameters(ViewGroup.LayoutParameters.WrapContent, ViewGroup.LayoutParameters.WrapContent));
                    }

                    SetNeedsLayout();
                }
            }
        }

        /// <summary>
        /// Sets the background color of the <see cref="ChatBubble" />.
        /// </summary>
        public override UIColor BackgroundColor
        {
            get => ChatBubbleTail.BackgroundColor;
            set
            {
                ChatBubbleTail.BackgroundColor = value;
                ContentViewContainer.BackgroundColor = value;
            }
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ChatBubble" /> class.
        /// </summary>
        public ChatBubble() { Initialize(); }
        /// <summary>
        /// Initializes a new instance of the <see cref="ChatBubble" /> class.
        /// </summary>
        /// <param name="frame"></param>
        public ChatBubble(CGRect frame) : base(frame) { Initialize(); }
        /// <summary>
        /// Initializes a new instance of the <see cref="ChatBubble" /> class.
        /// </summary>
        /// <param name="coder"></param>
        public ChatBubble(NSCoder coder) : base(coder) { Initialize(); }
        /// <summary>
        /// Initializes a new instance of the <see cref="ChatBubble" /> class.
        /// </summary>
        /// <param name="handle"></param>
        public ChatBubble(IntPtr handle) : base(handle) { Initialize(); }
        /// <summary>
        /// Initializes a new instance of the <see cref="ChatBubble" /> class.
        /// </summary>
        /// <param name="t"></param>
        public ChatBubble(NSObjectFlag t) : base(t) { Initialize(); }

        private void Initialize()
        {
            ChatBubbleTail = new ChatBubbleTriangle(8);
            AddSubview(ChatBubbleTail);
            ContentViewContainer = new LinearLayout();
            AddSubview(ContentViewContainer);
            ContentViewContainer.SetLayoutParameters(new ViewGroup.LayoutParameters(ViewGroup.LayoutParameters.WrapContent, ViewGroup.LayoutParameters.WrapContent));

            base.BackgroundColor = UIColor.FromRGBA(0, 0, 0, 0);
            BackgroundColor = UIColor.LabelColor;
            Gravity = GravityFlags.Left;
        }

        public override void LayoutSubviews()
        {
            CGSize contentViewContainerInitialSize = ContentViewContainer.Frame.Size;

            if (Gravity == GravityFlags.Right)
            {
                ChatBubbleTail.Frame = new CGRect(new CGPoint(Frame.Width - ChatBubbleTail.Frame.Width, (Frame.Height - ChatBubbleTail.Frame.Height) / 2.0), ChatBubbleTail.Frame.Size);
                ContentViewContainer.Frame = new CGRect(new CGPoint(0, 0), new CGSize(Frame.Size.Width - ChatBubbleTail.Frame.Width, Frame.Size.Height));
            }
            else
            {
                // Draw left configuration by default.
                ChatBubbleTail.Frame = new CGRect(new CGPoint(0, (Frame.Height - ChatBubbleTail.Frame.Height) / 2.0), ChatBubbleTail.Frame.Size);
                ContentViewContainer.Frame = new CGRect(new CGPoint(ChatBubbleTail.Frame.Width, 0), new CGSize(Frame.Size.Width - ChatBubbleTail.Frame.Width, Frame.Size.Height));
            }

            if (ContentViewContainer.Frame.Width != contentViewContainerInitialSize.Width || ContentViewContainer.Frame.Height != contentViewContainerInitialSize.Height)
            {
                ContentViewContainer.SetNeedsLayout();
                ContentView.SetNeedsLayout();
            }
        }

        public override CGSize IntrinsicContentSize
        {
            get
            {
                nfloat width = ChatBubbleTail.IntrinsicContentSize.Width;
                nfloat height = ChatBubbleTail.IntrinsicContentSize.Height;

                CGSize containerIntrinsicContentSize = ContentViewContainer.IntrinsicContentSize;
                width += containerIntrinsicContentSize.Width;
                if (containerIntrinsicContentSize.Height > height)
                {
                    height = containerIntrinsicContentSize.Height;
                }

                return new CGSize(width, height);
            }
        }

        public override CGSize SizeThatFits(CGSize size)
        {
            CGSize chatBubbleTailSize = ChatBubbleTail.SizeThatFits(size);
            CGSize contentViewSize = ContentViewContainer.SizeThatFits(size);

            return new CGSize(chatBubbleTailSize.Width + contentViewSize.Width, contentViewSize.Height > chatBubbleTailSize.Height ? contentViewSize.Height : chatBubbleTailSize.Height);
        }

        public override void SetNeedsLayout()
        {
            base.SetNeedsLayout();
            if (Superview is IViewGroup)
            {
                Superview.SetNeedsLayout();
            }
        }

        internal class ChatBubbleTriangle : UIView
        {
            protected GravityFlags mGravity;
            public GravityFlags Gravity
            {
                get => mGravity;
                set
                {
                    if (value != GravityFlags.Left && value != GravityFlags.Right)
                    {
                        throw new NotImplementedException($"Unsupported gravity setting: {value}");
                    }

                    if (mGravity != value)
                    {
                        mGravity = value;
                        SetNeedsDisplay();
                    }
                }
            }
            protected UIColor mBackgroundColor;
            public override UIColor BackgroundColor
            {
                get => mBackgroundColor;
                set
                {
                    if (mBackgroundColor != value)
                    {
                        mBackgroundColor = value;
                        SetNeedsDisplay();
                    }
                }
            }
            public double Width { get; protected set; }
            public double Height { get; protected set; }

            public ChatBubbleTriangle() : this(24) { }
            public ChatBubbleTriangle(int size) : this(size, size) { }
            public ChatBubbleTriangle(int width, int height) : base(new CGRect(new CGPoint(0, 0), new CGSize(width, height)))
            {
                Width = width;
                Height = height;
                Initialize();
            }

            private void Initialize()
            {
                base.BackgroundColor = UIColor.FromRGBA(0, 0, 0, 0);
                mBackgroundColor = UIColor.LabelColor;
                mGravity = GravityFlags.Left;
            }

            public override void Draw(CGRect rect)
            {
                CGContext context = UIGraphics.GetCurrentContext();
                context.ClearRect(rect);

                double scaleX = Width / 24.0;
                double scaleY = Height / 24.0;

                if (Gravity == GravityFlags.Left)
                {
                    UIBezierPath path = new UIBezierPath();

                    path.MoveTo(new CGPoint(0, 12).Scale(scaleX, scaleY));
                    path.AddLineTo(new CGPoint(24, 0).Scale(scaleX, scaleY));
                    path.AddLineTo(new CGPoint(24, 24).Scale(scaleX, scaleY));
                    path.ClosePath();

                    BackgroundColor.SetFill();
                    path.Fill();
                }
                else if (Gravity == GravityFlags.Right)
                {
                    UIBezierPath path = new UIBezierPath();

                    path.MoveTo(new CGPoint(24, 12).Scale(scaleX, scaleY));
                    path.AddLineTo(new CGPoint(0, 24).Scale(scaleX, scaleY));
                    path.AddLineTo(new CGPoint(0, 0).Scale(scaleX, scaleY));
                    path.ClosePath();

                    BackgroundColor.SetFill();
                    path.Fill();
                }
            }

            public override CGSize SizeThatFits(CGSize size)
            {
                return IntrinsicContentSize;
            }

            public override CGSize IntrinsicContentSize => new CGSize(Width, Height);
        }
    }
}