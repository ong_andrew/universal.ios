﻿using CoreGraphics;
using Foundation;
using System;
using System.ComponentModel;
using UIKit;

namespace Universal.iOS.Widget
{
    /// <summary>
    /// An <see cref="EditText" /> that can be quickly cleared.
    /// </summary>
    [Register("ClearableEditText")]
    [DesignTimeVisible(true)]
    public class ClearableEditText : EditText
    {
        /// <summary>
        /// Invoked when the <see cref="Clear()"/> method is used to clear the contents.
        /// </summary>
        public event EventHandler<EventArgs> TextCleared;

        public ClearableEditText() : base() { Initialize(); }
        public ClearableEditText(CGRect frame) : base(frame) { Initialize(); }
        public ClearableEditText(IntPtr handle) : base(handle) { Initialize(); }
        public ClearableEditText(NSCoder coder) : base(coder) { Initialize(); }
        public ClearableEditText(NSObjectFlag t) : base(t) { Initialize(); }

        private void Initialize()
        {
            ClearButtonMode = UITextFieldViewMode.WhileEditing;
        }

        public virtual void Clear()
        {
            Text = string.Empty;
            TextCleared?.Invoke(this, new EventArgs());
        }
    }
}