﻿using CoreGraphics;
using Foundation;
using System;
using System.ComponentModel;
using UIKit;

namespace Universal.iOS.Widget
{
    /// <summary>
    /// A <see cref="UITextField" /> that implements a <see cref="UIDatePicker" /> for its input method.
    /// </summary>
    [Register("DatePickerTextField")]
    [DesignTimeVisible(true)]
    public class DatePickerTextField : UITextField, IUIPickerViewDelegate
    {
        protected DateTime? mDate;
        /// <summary>
        /// Gets or sets the date of the <see cref="DatePickerTextField" />.
        /// </summary>
        public DateTime? Date
        {
            get => mDate;
            set
            {
                if (mDate != value)
                {
                    mDate = value;
                    SetTextFromDate();
                }
            }
        }
        protected string mFormat;
        /// <summary>
        /// Gets or sets the format of the <see cref="DatePickerTextField" />.
        /// </summary>
        public string Format
        {
            get => mFormat;
            set
            {
                if (mFormat != value)
                {
                    mFormat = value;
                    SetTextFromDate();
                }
            }
        }

        protected UIDatePicker DatePicker { get; set; }
        protected UIBarButtonItem DoneButton { get; set; }

        public DatePickerTextField() : base() { Initialize(); }
        public DatePickerTextField(CGRect frame) : base(frame) { Initialize(); }
        public DatePickerTextField(IntPtr handle) : base(handle) { Initialize(); }
        public DatePickerTextField(NSCoder coder) : base(coder) { Initialize(); }
        public DatePickerTextField(NSObjectFlag t) : base(t) { Initialize(); }

        private void Initialize()
        {
            DatePicker = new UIDatePicker();
            DatePicker.SizeToFit();
            DatePicker.AutoresizingMask = UIViewAutoresizing.FlexibleHeight | UIViewAutoresizing.FlexibleWidth;
            DatePicker.Mode = UIDatePickerMode.Date;

            UIToolbar toolbar = new UIToolbar();
            toolbar.BarStyle = UIBarStyle.Default;
            toolbar.Translucent = true;
            toolbar.TintColor = null;
            toolbar.SizeToFit();

            DoneButton = new UIBarButtonItem(title: "Done", style: UIBarButtonItemStyle.Done, target: this, action: new ObjCRuntime.Selector("dismissPicker"));
            toolbar.SetItems(new UIBarButtonItem[] { DoneButton }, animated: false);

            InputView = DatePicker;
            InputAccessoryView = toolbar;
        }

        protected virtual void SetTextFromDate()
        {
            Text = mDate.HasValue ? mDate.Value.ToString(Format ?? "dd-MMM-yyyy") : string.Empty;
            SetNeedsLayout();
            Superview?.SetNeedsLayout();
        }

        [EditorBrowsable(EditorBrowsableState.Never)]
        [Export("dismissPicker")]
        public void DismissPicker()
        {
            Date = ((DateTime)DatePicker.Date).ToLocalTime().Date;

            SetTextFromDate();

            ResignFirstResponder();
        }
    }
}