﻿using CoreGraphics;
using Foundation;
using System;
using System.ComponentModel;
using UIKit;
using Universal.Common.Extensions;
using Universal.iOS.Views;

namespace Universal.iOS.Widget
{
    /// <summary>
    /// A classic radio button widget that can be toggled on.
    /// </summary>
    [Register("RadioButton")]
    [DesignTimeVisible(true)]
    public class RadioButton : LinearLayout
    {
        protected UIColor mControlNormalColor;
        /// <summary>
        /// Gets or sets the color to be used when the control is in the normal state.
        /// </summary>
        public virtual UIColor ControlNormalColor
        {
            get => mControlNormalColor;
            set
            {
                if (mControlNormalColor != value)
                {
                    mControlNormalColor = value;
                    RadioCircle.ControlNormalColor = value;
                }
            }
        }

        /// <summary>
        /// Fired when the <see cref="RadioButton" /> is checked by a touch gesture.
        /// </summary>
        public event EventHandler CheckedByTouch;

        /// <summary>
        /// Gets or sets a value indicating if this <see cref="RadioButton" /> is touchable.
        /// </summary>
        public virtual bool Checked
        {
            get => RadioCircle.Checked;
            set
            {
                if (Checked != value)
                {
                    RadioCircle.Checked = value;
                }
            }
        }

        internal RadioCircle RadioCircle { get; set; }
        protected UILabel Label { get; set; }

        /// <summary>
        /// Gets or sets the text on the <see cref="RadioButton" />.
        /// </summary>
        public string Text
        {
            get => Label.Text;
            set
            {
                Label.Text = value;
                if (value.IsNullOrEmpty())
                {
                    if (Label.Superview != null)
                    {
                        Label.RemoveFromSuperview();
                    }
                }
                else
                {
                    if (Label.Superview == null)
                    {
                        AddSubview(Label, new LinearLayout.LayoutParameters(0, ViewGroup.LayoutParameters.WrapContent, 1)
                        {
                            Gravity = GravityFlags.CenterVertical,
                            MarginBottom = 2,
                            MarginLeft = 2,
                            MarginTop = 2,
                            MarginRight = 2
                        });
                    }
                }
            }
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="RadioButton"/> class.
        /// </summary>
        public RadioButton() { Initialize(); }
        /// <summary>
        /// Initializes a new instance of the <see cref="RadioButton"/> class.
        /// </summary>
        /// <param name="coder"></param>
        public RadioButton(NSCoder coder) : base(coder) { Initialize(); }
        /// <summary>
        /// Initializes a new instance of the <see cref="RadioButton"/> class.
        /// </summary>
        /// <param name="ptr"></param>
        public RadioButton(IntPtr ptr) : base(ptr) { Initialize(); }
        /// <summary>
        /// Initializes a new instance of the <see cref="RadioButton"/> class.
        /// </summary>
        /// <param name="frame"></param>
        public RadioButton(CGRect frame) : base(frame) { Initialize(); }
        /// <summary>
        /// Initializes a new instance of the <see cref="RadioButton"/> class.
        /// </summary>
        /// <param name="t"></param>
        public RadioButton(NSObjectFlag t) : base(t) { Initialize(); }

        private void Initialize()
        {
            Orientation = Orientation.Horizontal;
            RadioCircle = new RadioCircle();
            AddSubview(RadioCircle, new LinearLayout.LayoutParameters(ViewGroup.LayoutParameters.WrapContent, ViewGroup.LayoutParameters.WrapContent)
            {
                Gravity = GravityFlags.CenterVertical,
                MarginBottom = 2,
                MarginLeft = 2,
                MarginRight = 2,
                MarginTop = 2
            });
            Label = new TextView();

            AddGestureRecognizer(new UITapGestureRecognizer(() =>
            {
                if (!Checked)
                {
                    Checked = true;
                    CheckedByTouch?.Invoke(this, new EventArgs());
                }
            }));

            ControlNormalColor = UIColor.LabelColor;
        }
    }
}