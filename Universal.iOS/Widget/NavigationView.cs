﻿using CoreGraphics;
using Foundation;
using System;
using UIKit;
using Universal.iOS.Views;

namespace Universal.iOS.Widget
{
    /// <summary>
    /// A slide-in view that provides side-bar navigation.
    /// </summary>
    public class NavigationView : ContentViewGroup
    {
        /// <summary>
        /// Gets or sets the duration to use for animating the slide-in and slide-out effect.
        /// </summary>
        public nfloat AnimationDuration { get; set; }

        /// <summary>
        /// Gets a value indicating if the <see cref="NavigationView" /> is being shown.
        /// </summary>
        public bool IsShowing
        {
            get => Superview != null;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="NavigationView" /> class.
        /// </summary>
        public NavigationView() : base() { Initialize(); }
        /// <summary>
        /// Initializes a new instance of the <see cref="NavigationView" /> class.
        /// </summary>
        /// <param name="handle"></param>
        public NavigationView(IntPtr handle) : base(handle) { Initialize(); }
        /// <summary>
        /// Initializes a new instance of the <see cref="NavigationView" /> class.
        /// </summary>
        /// <param name="coder"></param>
        public NavigationView(NSCoder coder) : base(coder) { Initialize(); }
        /// <summary>
        /// Initializes a new instance of the <see cref="NavigationView" /> class.
        /// </summary>
        /// <param name="t"></param>
        public NavigationView(NSObjectFlag t) : base(t) { Initialize(); }

        private void Initialize()
        {
            AnimationDuration = 0.5f;
            Frame = new CGRect(new CGPoint(0, 0), new CGSize(UIScreen.MainScreen.Bounds.Size.Width * 2, UIScreen.MainScreen.Bounds.Size.Height));
            UITapGestureRecognizer tapGestureRecognizer = new UITapGestureRecognizer(() =>
            {
                Hide();
            });

            tapGestureRecognizer.ShouldReceiveTouch = new UITouchEventArgs((recognizer, touch) =>
            {
                if (ContentView != null)
                {
                    return !(touch.View.IsDescendantOfView(ContentView));
                }
                else
                {
                    return true;
                }
            });

            AddGestureRecognizer(tapGestureRecognizer);
        }

        /// <summary>
        /// Shows the <see cref="NavigationView" /> as a side-bar overlay, returning a value if it was shown.
        /// </summary>
        /// <returns></returns>
        public virtual bool Show()
        {
            return Show(true);
        }

        /// <summary>
        /// Shows the <see cref="NavigationView" /> as a side-bar overlay, returning a value if it was shown.
        /// </summary>
        /// <param name="animated"></param>
        /// <returns></returns>
        public virtual bool Show(bool animated)
        {
            if (IsShowing)
            {
                return false;
            }

            BackgroundColor = UIColor.Black.ColorWithAlpha(0f);

            if (ContentView != null)
            {
                nfloat contentViewWidth = 0;
                nfloat contentViewHeight = UIScreen.MainScreen.Bounds.Size.Height;
                CGSize sizeThatFitsContentView = ContentView.SizeThatFits(UIScreen.MainScreen.Bounds.Size);

                contentViewWidth = sizeThatFitsContentView.Width < 300 ? 300 : sizeThatFitsContentView.Width;
                contentViewWidth = UIScreen.MainScreen.Bounds.Size.Width * 0.7f < contentViewWidth ? UIScreen.MainScreen.Bounds.Size.Width * 0.7f : contentViewWidth;

                CGRect contentViewFrame = new CGRect(0, 0, contentViewWidth, contentViewHeight);

                ContentView.Frame = contentViewFrame;

                if (animated)
                {
                    CGRect startingViewFrame = new CGRect(new CGPoint(-contentViewWidth, 0), Frame.Size);
                    Frame = startingViewFrame;
                    UIApplication.SharedApplication.KeyWindow.AddSubview(this);

                    Animate(AnimationDuration, () =>
                    {
                        Frame = new CGRect(new CGPoint(0, 0), Frame.Size);
                        BackgroundColor = UIColor.Black.ColorWithAlpha(0.4f);
                    });
                }
                else
                {
                    Frame = new CGRect(new CGPoint(0, 0), Frame.Size);
                    BackgroundColor = UIColor.Black.ColorWithAlpha(0.4f);
                }
            }
            else
            {
                throw new InvalidOperationException($"The {nameof(ContentView)} property must be set.");
            }

            return true;
        }

        /// <summary>
        /// Hides the <see cref="NavigationView" /> previously shown as a overlay, returning a value if it was hidden.
        /// </summary>
        /// <returns></returns>
        public virtual bool Hide()
        {
            return Hide(true);
        }

        /// <summary>
        /// Hides the <see cref="NavigationView" /> previously shown as a overlay, returning a value if it was hidden.
        /// </summary>
        /// <param name="animated"></param>
        /// <returns></returns>
        public virtual bool Hide(bool animated)
        {
            if (!IsShowing)
            {
                return false;
            }

            if (ContentView != null)
            {
                if (animated)
                {
                    Frame = new CGRect(new CGPoint(0, 0), Frame.Size);

                    Animate(AnimationDuration, () =>
                    {
                        Frame = new CGRect(new CGPoint(-ContentView.Frame.Width, 0), Frame.Size);
                        BackgroundColor = UIColor.Black.ColorWithAlpha(0);
                    },
                    () =>
                    {
                        RemoveFromSuperview();
                    });
                }
                else
                {
                    RemoveFromSuperview();
                }
            }
            else
            {
                throw new InvalidOperationException($"The {nameof(ContentView)} property must be set.");
            }

            return true;
        }
    }
}