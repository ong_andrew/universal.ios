﻿namespace Universal.iOS.Widget
{
    /*
    public class GridLayout : ViewGroup
    {
        public enum Alignment
        {
            NoGravity = 0,
            CenterHorizontal = 1,
            Left = 2,
            Right = 4,
            CenterVertical = 16,
            Center = CenterHorizontal | CenterVertical,
            Top = 32,
            Bottom = 64,
            Baseline = 128,
            FillHorizontal = 256,
            FillVertical = 512,
            Fill = FillHorizontal | FillVertical,
            ClipHorizontal = 1024,
            ClipVertical = 2048
        }

        protected Orientation mOrientation;
        public Orientation Orientation
        {
            get => mOrientation;
            set
            {
                if (mOrientation != value)
                {
                    mOrientation = value;
                    SetNeedsLayout();
                }
            }
        }

        protected int mRowCount;
        public int RowCount
        {
            get => mRowCount;
            set
            {
                if (mRowCount != value)
                {
                    mRowCount = value;
                    SetNeedsLayout();
                }
            }
        }

        protected int mColumnCount;
        public int ColumnCount
        {
            get => mColumnCount;
            set
            {
                if (mColumnCount != value)
                {
                    mColumnCount = value;
                    SetNeedsLayout();
                }
            }
        }

        public override void AddSubview(UIView view)
        {
            if (view.GetLayoutParameters() == null)
            {
                view.SetLayoutParameters(new GridLayout.LayoutParameters());
            }

            if (!(view.GetLayoutParameters() is LayoutParameters))
            {
                throw new TypeMismatchException(typeof(LayoutParameters), view.GetLayoutParameters().GetType());
            }

            base.AddSubview(view);
        }

        public virtual void AddSubview(UIView view, GridLayout.LayoutParameters layoutParameters)
        {
            if (layoutParameters is null)
            {
                throw new ArgumentNullException(nameof(layoutParameters));
            }

            view.SetLayoutParameters(layoutParameters);
            AddSubview(view);
        }

        protected virtual List<(UIView View, CellGroupIndex CellGroupIndex)> AssignIndices()
        {
            List<(UIView View, CellGroupIndex CellGroupIndex)> result = new List<(UIView View, CellGroupIndex CellGroupIndex)>();

            foreach (UIView uiView in Subviews)
            {
                int rowStart, rowEnd, columnStart, columnEnd;
                bool carryColumnStart = false;
                LayoutParameters layoutParameters = GetLayoutParameters(uiView);

                if (layoutParameters.RowSpecification.Start != null)
                {
                    rowStart = layoutParameters.RowSpecification.Start.Value;
                }
                else
                {
                    if (!result.Any())
                    {
                        rowStart = 0;
                    }
                    else
                    {
                        if (Orientation == Orientation.Horizontal)
                        {
                            rowStart = result.Last().CellGroupIndex.RowEnd;
                        }
                        else
                        {
                            rowStart = result.Last().CellGroupIndex.RowEnd + 1;
                            if (rowStart >= RowCount)
                            {
                                rowStart = 0;
                                carryColumnStart = true;
                            }
                        }
                    }
                }

                if (layoutParameters.RowSpecification.End != null)
                {
                    rowEnd = layoutParameters.RowSpecification.End.Value;
                }
                else
                {
                    rowEnd = rowStart;
                }

                if (layoutParameters.ColumnSpecification.Start != null)
                {
                    columnStart = layoutParameters.ColumnSpecification.Start.Value;
                }
                else
                {
                    if (!result.Any())
                    {
                        columnStart = 0;
                        if (carryColumnStart)
                        {
                            columnStart++;
                        }
                    }
                    else
                    {
                        if (Orientation == Orientation.Horizontal)
                        {
                            columnStart = result.Last().CellGroupIndex.ColumnEnd + 1;
                            if (columnStart >= ColumnCount)
                            {
                                columnStart = 0;
                                rowStart++;
                            }
                        }
                        else
                        {
                            columnStart = result.Last().CellGroupIndex.ColumnEnd;
                            if (carryColumnStart)
                            {
                                columnStart++;
                            }
                        }
                    }
                }

                if (layoutParameters.ColumnSpecification.End != null)
                {
                    columnEnd = layoutParameters.ColumnSpecification.End.Value;
                }
                else
                {
                    columnEnd = columnStart;
                }

                result.Add((uiView, new CellGroupIndex(rowStart, rowEnd, columnStart, columnEnd)));
            }

            return result;
        }

        protected virtual LayoutParameters GetLayoutParameters(UIView uiView)
        {
            ViewGroup.LayoutParameters layoutParameters = uiView.GetLayoutParameters();

            if (layoutParameters is LayoutParameters typedLayoutParameters)
            {
                return typedLayoutParameters;
            }
            else if (layoutParameters == null)
            {
                throw new LayoutParametersNotFoundException("No layout parameters found for the given child view.");
            }
            else
            {
                throw new InvalidLayoutParametersException($"Layout parameters must be of type {typeof(LayoutParameters)}, found one of type {layoutParameters.GetType()}.");
            }
        }

        public override void LayoutSubviews()
        {
            base.LayoutSubviews();
            List<(UIView UIView, CellGroupIndex CellGroupIndex)> indexMap = AssignIndices();

        }

        public new class LayoutParameters : ViewGroup.LayoutParameters
        {
            public Specification RowSpecification { get; set; }
            public Specification ColumnSpecification { get; set; }

            public LayoutParameters() : this(new Specification() { Start = null, End = null, Alignment = Alignment.Left | Alignment.Top }, new Specification() { Start = null, End = null, Alignment = Alignment.Left | Alignment.Top }) { }
            public LayoutParameters(Specification rowSpecification, Specification columnSpecification) : base(ViewGroup.LayoutParameters.WrapContent, ViewGroup.LayoutParameters.WrapContent) 
            {
                RowSpecification = rowSpecification;
                ColumnSpecification = columnSpecification;
            }
        }

        public struct Specification
        {
            public int? Start { get; set; }
            public int? End { get; set; }
            public Alignment Alignment { get; set; }

            public Specification(int start)
            {
                Start = start;
                End = start;
                Alignment = Alignment.Left | Alignment.Top;
            }
            public Specification(int start, int span)
            {
                Start = start;
                End = start + span;
                Alignment = Alignment.Left | Alignment.Top;
            }
            public Specification(int start, int span, Alignment alignment)
            {
                Start = start;
                End = start + span;
                Alignment = alignment;
            }
        }

        public struct CellGroupIndex
        {
            public int RowStart { get; set; }
            public int RowEnd { get; set; }
            public int ColumnStart { get; set; }
            public int ColumnEnd { get; set; }

            public CellGroupIndex(int row, int column)
            {
                RowStart = row;
                RowEnd = row;
                ColumnStart = column;
                ColumnEnd = column;
            }

            public CellGroupIndex(int rowStart, int rowEnd, int columnStart, int columnEnd)
            {
                RowStart = rowStart;
                RowEnd = rowEnd;
                ColumnStart = columnStart;
                ColumnEnd = columnEnd;
            }
        }
    }
    */
}