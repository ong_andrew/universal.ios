﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using CoreGraphics;
using Foundation;
using MobileCoreServices;
using UIKit;
using Universal.Common.Extensions;
using Universal.iOS.Extensions;

namespace Universal.iOS.Widget
{
    public class FilePickerView : UIButton
    {
        public event EventHandler<FileChosenEventArgs> FileChosen;
        public NSUrl FileUri { get; protected set; }
        public string FileName { get; protected set; }
        public Universal.Common.MediaType FileMediaType { get; protected set; }
        public byte[] FileBytes { get; protected set; }
        public FilePickerView() { Initialize(); }
        public FilePickerView(NSCoder coder) : base(coder) { Initialize(); }
        public FilePickerView(IntPtr ptr) : base(ptr) { Initialize(); }
        public FilePickerView(CGRect frame) : base(frame) { Initialize(); }
        public FilePickerView(NSObjectFlag t) : base(t) { Initialize(); }

        private void Initialize()
        {
            SetTitle("Upload Document or Photo ", UIControlState.Normal);
            SetTitleColor(UIColor.LabelColor, UIControlState.Normal);
            HorizontalAlignment = UIControlContentHorizontalAlignment.Left;
            SemanticContentAttribute = UISemanticContentAttribute.ForceRightToLeft;
            Assembly thisAssembly = Assembly.GetExecutingAssembly();
            string assemblyName = thisAssembly.GetName().Name;
            SetImage(UIImage.FromResource(thisAssembly, assemblyName + ".Resources." + "baseline_cloud_upload_black_24dp.png").ImageWithTint(UIColor.DarkGray), UIControlState.Normal);
            TouchUpInside += (s, e) =>
            {
                PresentFileChooser();
            };

            FileChosen += (sender, eventArgs) =>
            {
                SetTitle($"{eventArgs.Name} ", UIControlState.Normal);
            };
        }

        private void PresentFileChooser()
        {
            UIWindow uiWindow = UIApplication.SharedApplication.KeyWindow;
            var actionOptionsAlertController = UIAlertController.Create("Would you like to choose from the photo gallery or the file app?", "", UIAlertControllerStyle.ActionSheet);

            actionOptionsAlertController.AddAction(UIAlertAction.Create("Photo Gallery", UIAlertActionStyle.Default, (UIAlertAction obj) =>
            {
                var imagePickerController = new UIImagePickerController();
                imagePickerController.SourceType = UIImagePickerControllerSourceType.PhotoLibrary;
                imagePickerController.Delegate = new CustomImagePickerController()
                {
                    FilePickerView = this
                };
                uiWindow = UIApplication.SharedApplication.KeyWindow;
                uiWindow.RootViewController.PresentViewController(imagePickerController, true, null);
            }));

            actionOptionsAlertController.AddAction(UIAlertAction.Create("File", UIAlertActionStyle.Default, (UIAlertAction obj) =>
            {
                var allowedUTIs = new string[] {
                        UTType.UTF8PlainText,
                        UTType.PlainText,
                        UTType.RTF,
                        UTType.PNG,
                        UTType.Text,
                        UTType.PDF,
                        UTType.Image
                    };
                UIDocumentBrowserViewController uIDocumentBrowserViewController = new UIDocumentBrowserViewController(allowedUTIs);
                uIDocumentBrowserViewController.Delegate = new CustomDocumentBrowserDelegate()
                {
                    FilePickerView = this
                };
                uiWindow = UIApplication.SharedApplication.KeyWindow;
                uiWindow.RootViewController.PresentViewController(uIDocumentBrowserViewController, true, null);
            }));

            uiWindow = UIApplication.SharedApplication.KeyWindow;
            uiWindow.RootViewController.PresentViewController(actionOptionsAlertController, true, null);
        }

        public class CustomDocumentBrowserDelegate : UIDocumentBrowserViewControllerDelegate
        {
            public FilePickerView FilePickerView { get; set; }
            public override void DidPickDocumentsAtUrls(UIDocumentBrowserViewController controller, NSUrl[] documentUrls)
            {
                FilePickerView.FileUri = documentUrls[0];
                FilePickerView.FileName = documentUrls[0].LastPathComponent;
                if (documentUrls[0].PathExtension.IsNullOrEmpty())
                {
                    FilePickerView.FileMediaType = Common.MediaTypes.Application.OctetStream;
                }
                else
                {
                    FilePickerView.FileMediaType = Universal.Common.MediaType.FromExtension(documentUrls[0].PathExtension);
                }

                documentUrls[0].StartAccessingSecurityScopedResource();
                try
                {
                    NSData data = NSData.FromUrl(documentUrls[0]);

                    if (FilePickerView.FileMediaType != null && (FilePickerView.FileMediaType.Equals(new Common.MediaType("image/heic")) ||
                        FilePickerView.FileMediaType.Equals(new Common.MediaType("image/heif")) ||
                        FilePickerView.FileMediaType.Equals(new Common.MediaType("image/heic-sequence")) ||
                        FilePickerView.FileMediaType.Equals(new Common.MediaType("image/heif-sequence"))))
                    {
                        UIImage image = new UIImage(data);
                        byte[] jpgBytes = image.AsJPEG(0.9f).ToArray();
                        FilePickerView.FileBytes = jpgBytes;

                        if (FilePickerView.FileName != null)
                        {
                            FilePickerView.FileName += ".jpg";
                        }
                        else
                        {
                            FilePickerView.FileName = "image.jpg";
                        }

                        FilePickerView.FileMediaType = new Common.MediaType("image/jpeg");
                    }
                    else
                    {
                        FilePickerView.FileBytes = data.ToArray();
                    }

                    FilePickerView.FileChosen?.Invoke(this, new FileChosenEventArgs(FilePickerView.FileUri, FilePickerView.FileName, FilePickerView.FileMediaType, FilePickerView.FileBytes));
                }
                catch (Exception e)
                {
                    Toast toast = new Toast("Cannot attach this from File", ToastLength.Long);
                    toast.Show();
                }
                documentUrls[0].StopAccessingSecurityScopedResource();
                
                controller.DismissViewController(true, null);
            }
        }

        public class FileChosenEventArgs : EventArgs
        {
            public NSUrl Uri { get; set; }
            public string Name { get; protected set; }
            public Universal.Common.MediaType MediaType { get; protected set; }
            public byte[] Bytes { get; protected set; }
            public FileChosenEventArgs(NSUrl uri, string name, Universal.Common.MediaType mediaType, byte[] bytes)
            {
                Uri = uri;
                Name = name;
                MediaType = mediaType;
                Bytes = bytes;
            }
        }

        public class CustomImagePickerController : UIImagePickerControllerDelegate
        {
            public FilePickerView FilePickerView { get; set; }
            public override void FinishedPickingMedia(UIImagePickerController controller, NSDictionary info)
            {
                try
                {
                    UIImage image = info[UIImagePickerController.OriginalImage] as UIImage;
                    if (info[UIImagePickerController.EditedImage] as UIImage != null)
                    {
                        image = info[UIImagePickerController.EditedImage] as UIImage;
                    }
                    if (image != null)
                    {
                        var imageURL = info[UIImagePickerController.ReferenceUrl] as NSUrl;
                        FilePickerView.FileUri = imageURL;
                        FilePickerView.FileName = imageURL.LastPathComponent;
                        if (imageURL.PathExtension.IsNullOrEmpty())
                        {
                            FilePickerView.FileMediaType = Common.MediaTypes.Application.OctetStream;
                        }
                        else
                        {
                            FilePickerView.FileMediaType = Universal.Common.MediaType.FromExtension(imageURL.PathExtension);
                        }

                        if (FilePickerView.FileMediaType != null && (FilePickerView.FileMediaType.Equals(new Common.MediaType("image/heic")) ||
                            FilePickerView.FileMediaType.Equals(new Common.MediaType("image/heif")) ||
                            FilePickerView.FileMediaType.Equals(new Common.MediaType("image/heic-sequence")) ||
                            FilePickerView.FileMediaType.Equals(new Common.MediaType("image/heif-sequence"))))
                        {
                            if (FilePickerView.FileName != null)
                            {
                                FilePickerView.FileName += ".jpg";
                            }
                            else
                            {
                                FilePickerView.FileName = "image.jpg";
                            }

                            FilePickerView.FileMediaType = new Common.MediaType("image/jpeg");
                        }

                        FilePickerView.FileBytes = image.AsJPEG(0.9f).ToArray();
                    }
                    FilePickerView.FileChosen?.Invoke(this, new FileChosenEventArgs(FilePickerView.FileUri, FilePickerView.FileName, FilePickerView.FileMediaType, FilePickerView.FileBytes));
                }
                catch (Exception e)
                {
                    Toast toast = new Toast("Cannot attach this from Photo Gallery.", ToastLength.Long);
                    toast.Show();
                }
                controller.DismissViewController(true, null);
            }
        }
    }
}
