﻿using System;
using UIKit;
using Universal.iOS.Views;

namespace Universal.iOS.Widget
{
    /// <summary>
    /// Default side-bar content to use for a <see cref="NavigationView" />.
    /// </summary>
    public class NavigationViewContent : LinearLayout
    {
        /// <summary>
        /// Raised when a menu item is selected.
        /// </summary>
        public event EventHandler<MenuItemSelectedEventArgs> MenuItemSelected;

        /// <summary>
        /// 
        /// </summary>
        public LinearLayout Header { get; private set; }
        private LinearLayout MenuItemContainer { get; set; }

        public NavigationViewContent()
        {
            Initialize();
        }

        private void Initialize()
        {
            Orientation = Orientation.Vertical;

            Header = new LinearLayout()
            {
                Orientation = Orientation.Vertical,
            };

            AddSubview(Header, new LinearLayout.LayoutParameters(ViewGroup.LayoutParameters.MatchParent, ViewGroup.LayoutParameters.WrapContent));

            MenuItemContainer = new LinearLayout()
            {
                Orientation = Orientation.Vertical,
                PaddingBottom = 20,
                PaddingTop = 20,
                PaddingLeft = 20,
                PaddingRight = 20
            };

            AddSubview(MenuItemContainer, new LinearLayout.LayoutParameters(ViewGroup.LayoutParameters.MatchParent, 0, 1));
        }

        public UIView AddMenuItem(int id, string text)
        {
            Button button = new Button()
            {
                Text = text,
                TextColor = UIColor.Black
            };

            button.TouchUpInside += (sender, eventArgs) =>
            {
                if (Superview is NavigationView navigationView)
                {
                    navigationView.Hide();
                }

                OnMenuItemSelected(id);
            };

            MenuItemContainer.AddSubview(button, new LayoutParameters(ViewGroup.LayoutParameters.WrapContent, ViewGroup.LayoutParameters.WrapContent));

            return button;
        }

        public UIView AddMenuItem(int id, string text, Action action)
        {
            UIView result = AddMenuItem(id, text);

            MenuItemSelected += (sender, eventArgs) =>
            {
                if (eventArgs.Id == id)
                {
                    action?.Invoke();
                }
            };

            return result;
        }

        private void OnMenuItemSelected(int id)
        {
            MenuItemSelected?.Invoke(this, new MenuItemSelectedEventArgs(id));
        }

        public class MenuItemSelectedEventArgs : EventArgs
        {
            public int Id { get; private set; }

            public MenuItemSelectedEventArgs(int id)
            {
                Id = id;
            }
        }
    }
}