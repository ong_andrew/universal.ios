﻿using System;
using System.Collections.Generic;
using System.Linq;
using UIKit;

namespace Universal.iOS.Widget
{
    /// <summary>
    /// Represents a group of <see cref="RadioButton" />s that can be checked exclusively.
    /// </summary>
    public class RadioGroup : LinearLayout
    {
        /// <summary>
        /// Gets the <see cref="RadioButton" />s managed by this <see cref="RadioGroup" />.
        /// </summary>
        public IEnumerable<RadioButton> RadioButtons
        {
            get
            {
                return Subviews.OfType<RadioButton>();
            }
        }

        public override void AddSubview(UIView view)
        {
            base.AddSubview(view);

            if (view is RadioButton radioButton)
            {
                AddCheckedByTouchListener(radioButton);
            }
        }

        private void AddCheckedByTouchListener(RadioButton radioButton)
        {
            radioButton.CheckedByTouch += OnChildCheckedByTouch;
        }

        protected virtual void OnChildCheckedByTouch(object sender, EventArgs e)
        {
            if (sender is RadioButton radioButton)
            {
                if (RadioButtons.Contains(radioButton))
                {
                    foreach (RadioButton otherRadioButton in RadioButtons.Except(new RadioButton[] { radioButton }))
                    {
                        if (otherRadioButton.Checked)
                        {
                            otherRadioButton.Checked = false;
                        }
                    }
                }
                else
                {
                    radioButton.CheckedByTouch -= OnChildCheckedByTouch;
                }
            }
        }
    }
}