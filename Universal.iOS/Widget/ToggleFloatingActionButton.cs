﻿using CoreGraphics;
using Foundation;
using MaterialComponents;
using System;
using System.ComponentModel;
using UIKit;
using Universal.Common.Forms;

namespace Universal.iOS.Widget
{
    [Register("ToggleFloatingActionButton")]
    [DesignTimeVisible(true)]
    public class ToggleFloatingActionButton : FloatingButton
    {
        protected bool mToggled;
        /// <summary>
        /// Raised when the toggle state changes.
        /// </summary>
        public event EventHandler<ToggleChangedEventArgs> ToggleChanged;
        public bool Toggled
        {
            get => mToggled;
            set
            {
                if (mToggled != value)
                {
                    mToggled = value;
                    InvokeOnMainThread(() =>
                    {
                        if (Toggled)
                        {
                            SetImage(ImageToggled, UIControlState.Normal);
                        }
                        else
                        {
                            SetImage(ImageNormal, UIControlState.Normal);
                        }
                        SetNeedsDisplay();
                    });
                    ToggleChanged?.Invoke(this, new ToggleChangedEventArgs(Toggled));
                }
            }
        }

        public ToggleFloatingActionButton() : base() { Initialize(); }
        public ToggleFloatingActionButton(CGRect frame) : base(frame) { Initialize(); }
        public ToggleFloatingActionButton(IntPtr handle) : base(handle) { Initialize(); }
        public ToggleFloatingActionButton(NSCoder coder) : base(coder) { Initialize(); }
        public ToggleFloatingActionButton(NSObjectFlag t) : base(t) { Initialize(); }

        /// <summary>
        /// Toggles this button.
        /// </summary>
        public void Toggle()
        {
            Toggled = !Toggled;
        }

        protected UIImage mImageNormal;
        public UIImage ImageNormal
        {
            get => mImageNormal;
            set
            {
                mImageNormal = value;
                if (!Toggled)
                {
                    SetImage(mImageNormal, UIControlState.Normal);
                    SetNeedsDisplay();
                }
            }
        }

        protected UIImage mImageToggled;
        public virtual UIImage ImageToggled
        {
            get => mImageToggled;
            set
            {
                mImageToggled = value;
                if (Toggled)
                {
                    SetImage(mImageToggled, UIControlState.Normal);
                    SetNeedsDisplay();
                }
            }
        }

        private void Initialize()
        {
            mToggled = false;

            TouchUpInside += (sender, eventArgs) =>
            {
                Toggle();
            };
        }
    }
}