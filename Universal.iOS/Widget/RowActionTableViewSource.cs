﻿using Foundation;
using System;
using System.Collections.Generic;
using UIKit;

namespace Universal.iOS.Widget
{
    /// <summary>
    /// <see cref="TableViewSource{TTableViewCell, TItem}"/> that exposes row actions as events.
    /// </summary>
    /// <typeparam name="TTableViewCell"></typeparam>
    /// <typeparam name="TItem"></typeparam>
    public class RowActionTableViewSource<TTableViewCell, TItem> : TableViewSource<TTableViewCell, TItem>
        where TTableViewCell: UITableViewCell
    {
        /// <summary>
        /// Raised when a row is deselected.
        /// </summary>
        public event Action<UITableView, NSIndexPath> OnRowDeselected;
        /// <summary>
        /// Raised when a row is highlighted.
        /// </summary>
        public event Action<UITableView, NSIndexPath> OnRowHighlighted;
        /// <summary>
        /// Raised when a row is selected.
        /// </summary>
        public event Action<UITableView, NSIndexPath> OnRowSelected;
        /// <summary>
        /// Raised when a row is unhighlighted.
        /// </summary>
        public event Action<UITableView, NSIndexPath> OnRowUnhighlighted;

        /// <summary>
        /// Instantiates a new <see cref="RowActionTableViewSource{TTableViewCell, TItem}"/> with a single-section element set for the given items.
        /// </summary>
        /// <param name="items"></param>
        /// <param name="createCellDelegate"></param>
        /// <param name="bindCellDelegate"></param>
        public RowActionTableViewSource(IEnumerable<TItem> items, CreateCellDelegate createCellDelegate, BindCellDelegate bindCellDelegate) : base(items, createCellDelegate, bindCellDelegate) { }
        /// <summary>
        /// Instantiates a new <see cref="RowActionTableViewSource{TTableViewCell, TItem}"/> with a single-section element set for the given items.
        /// </summary>
        /// <param name="items"></param>
        /// <param name="createCellDelegate"></param>
        /// <param name="bindCellDelegate"></param>
        public RowActionTableViewSource(IEnumerable<IEnumerable<TItem>> items, CreateCellDelegate createCellDelegate, BindCellDelegate bindCellDelegate) : base(items, createCellDelegate, bindCellDelegate) { }

        public override void RowDeselected(UITableView tableView, NSIndexPath indexPath)
        {
            OnRowDeselected?.Invoke(tableView, indexPath);
        }

        public override void RowHighlighted(UITableView tableView, NSIndexPath rowIndexPath)
        {
            OnRowHighlighted?.Invoke(tableView, rowIndexPath);
        }

        public override void RowSelected(UITableView tableView, NSIndexPath indexPath)
        {
            OnRowSelected?.Invoke(tableView, indexPath);
        }

        public override void RowUnhighlighted(UITableView tableView, NSIndexPath rowIndexPath)
        {
            OnRowUnhighlighted?.Invoke(tableView, rowIndexPath);
        }
    }
}