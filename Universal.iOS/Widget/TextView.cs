﻿using CoreGraphics;
using Foundation;
using System;
using System.ComponentModel;
using UIKit;

namespace Universal.iOS.Widget
{
    /// <summary>
    /// A <see cref="UILabel" /> that is multiline by default.
    /// </summary>
    [Register("TextView")]
    [DesignTimeVisible(true)]
    public class TextView : UILabel
    {
        /// <summary>
        /// Initializes a new <see cref="TextView" />.
        /// </summary>
        public TextView() { Initialize(); }
        /// <summary>
        /// Initializes a new <see cref="TextView" />.
        /// </summary>
        /// <param name="coder"></param>
        public TextView(NSCoder coder) : base(coder) { Initialize(); }
        /// <summary>
        /// Initializes a new <see cref="TextView" />.
        /// </summary>
        /// <param name="handle"></param>
        public TextView(IntPtr handle) : base(handle) { Initialize(); }
        /// <summary>
        /// Initializes a new <see cref="TextView" />.
        /// </summary>
        /// <param name="t"></param>
        public TextView(NSObjectFlag t) : base(t) { Initialize(); }
        /// <summary>
        /// Initializes a new <see cref="TextView" />.
        /// </summary>
        /// <param name="frame"></param>
        public TextView(CGRect frame) : base(frame) { Initialize(); }

        private void Initialize()
        {
            Lines = 0;
        }
    }
}