﻿using CoreGraphics;
using UIKit;
using Universal.iOS.Extensions;

namespace Universal.iOS.Widget
{
    internal class CheckBoxSquare : UIView
    {
        protected UIColor mControlNormalColor;
        /// <summary>
        /// Gets or sets the 
        /// </summary>
        public virtual UIColor ControlNormalColor
        {
            get => mControlNormalColor;
            set
            {
                if (mControlNormalColor != value)
                {
                    mControlNormalColor = value;
                    SetNeedsDisplay();
                }
            }
        }

        protected bool mChecked;
        public bool Checked
        {
            get => mChecked;
            set
            {
                if (mChecked != value)
                {
                    mChecked = value;
                    SetNeedsDisplay();
                }
            }
        }
        protected double Size { get; set; }

        public CheckBoxSquare() : this(24) { }
        public CheckBoxSquare(int size) : base() 
        { 
            Size = size; 
            Opaque = false;
            mControlNormalColor = UIColor.LabelColor;
        }

        public override void Draw(CGRect rect)
        {
            CGContext context = UIGraphics.GetCurrentContext();
            context.ClearRect(rect);

            double scaleFactor = Size / 24;

            UIBezierPath path = new UIBezierPath();

            if (Checked)
            {
                path.MoveTo(new CGPoint(19, 3).Scale(scaleFactor));
                path.AddHorizontalLineTo(5 * scaleFactor);
                path.AddCurveToPointRelative(new CGPoint(-2, 2).Scale(scaleFactor), new CGPoint(-1.11, 0).Scale(scaleFactor), new CGPoint(-2, 0.9).Scale(scaleFactor));
                path.AddVerticalLineToRelative(14 * scaleFactor);
                path.AddCurveToPointRelative(new CGPoint(2, 2).Scale(scaleFactor), new CGPoint(0, 1.1).Scale(scaleFactor), new CGPoint(0.89, 2).Scale(scaleFactor));
                path.AddHorizontalLineToRelative(14 * scaleFactor);
                path.AddCurveToPointRelative(new CGPoint(2, -2).Scale(scaleFactor), new CGPoint(1.1, 0).Scale(scaleFactor), new CGPoint(2, -0.9).Scale(scaleFactor));
                path.AddVerticalLineTo(5 * scaleFactor);
                path.AddCurveToPointRelative(new CGPoint(-2, -2).Scale(scaleFactor), new CGPoint(0, -1.1).Scale(scaleFactor), new CGPoint(-0.89, -2).Scale(scaleFactor));
                path.ClosePath();

                path.MoveToRelative(new CGPoint(-9, 14).Scale(scaleFactor));
                path.AddLineToRelative(new CGPoint(-5, -5).Scale(scaleFactor));
                path.AddLineToRelative(new CGPoint(1.41, -1.41).Scale(scaleFactor));
                path.AddLineTo(new CGPoint(10, 14.17).Scale(scaleFactor));
                path.AddLineToRelative(new CGPoint(7.59, -7.59).Scale(scaleFactor));
                path.AddLineTo(new CGPoint(19, 8).Scale(scaleFactor));
                path.AddLineToRelative(new CGPoint(-9, 9).Scale(scaleFactor));
                path.ClosePath();

            }
            else
            {
                path.MoveTo(new CGPoint(19, 5).Scale(scaleFactor));
                path.AddVerticalLineToRelative(14 * scaleFactor);
                path.AddHorizontalLineTo(5 * scaleFactor);
                path.AddVerticalLineTo(5 * scaleFactor);
                path.AddHorizontalLineToRelative(14 * scaleFactor);
                path.MoveToRelative(new CGPoint(0, -2).Scale(scaleFactor));
                path.AddHorizontalLineTo(5 * scaleFactor);
                path.AddCurveToPointRelative(new CGPoint(-2, 2).Scale(scaleFactor), new CGPoint(-1.1, 0).Scale(scaleFactor), new CGPoint(-2, 0.9).Scale(scaleFactor));
                path.AddVerticalLineToRelative(14 * scaleFactor);
                path.AddCurveToPointRelative(new CGPoint(2, 2).Scale(scaleFactor), new CGPoint(0, 1.1).Scale(scaleFactor), new CGPoint(0.9, 2).Scale(scaleFactor));
                path.AddHorizontalLineToRelative(14 * scaleFactor);
                path.AddCurveToPointRelative(new CGPoint(2, -2).Scale(scaleFactor), new CGPoint(1.1, 0).Scale(scaleFactor), new CGPoint(2, -0.9).Scale(scaleFactor));
                path.AddVerticalLineTo(5 * scaleFactor);
                path.AddCurveToPointRelative(new CGPoint(-2, -2).Scale(scaleFactor), new CGPoint(0, -1.1).Scale(scaleFactor), new CGPoint(-0.9, -2).Scale(scaleFactor));
                path.ClosePath();
            }

            ControlNormalColor.SetFill();
            path.Fill();
        }

        public override CGSize SizeThatFits(CGSize size)
        {
            return IntrinsicContentSize;
        }

        public override CGSize IntrinsicContentSize
        {
            get
            {
                return new CGSize(Size, Size);
            }
        }
    }
}