﻿using CoreAnimation;
using CoreGraphics;
using Foundation;
using System;
using UIKit;
using Universal.Common.Extensions;
using Universal.iOS.Extensions;
using Universal.iOS.Views;

namespace Universal.iOS.Widget
{
    public class MultilineEditText : UIView, IEditText, IUITextViewDelegate
    {
        public event EventHandler EditingDidBegin;
        public event EventHandler EditingDidEnd;
        /// <summary>
        /// Raised when the text of the embedded <see cref="MultilineEditText" /> changes by user interaction.
        /// </summary>
        public event EventHandler TextDidChange;

        private nfloat mUnderlineThickness;
        /// <summary>
        /// Gets or sets the thickness of the line to draw when in the normal state.
        /// </summary>
        public nfloat UnderlineThickness
        {
            get => mUnderlineThickness;
            set
            {
                if (mUnderlineThickness != value)
                {
                    mUnderlineThickness = value;
                    SetNeedsLayout();
                }
            }
        }
        private nfloat mUnderlineThicknessSelected;
        /// <summary>
        /// Gets or sets the thickness of the line to draw when in the selected state.
        /// </summary>
        public nfloat UnderlineThicknessSelected
        {
            get => mUnderlineThicknessSelected;
            set
            {
                if (mUnderlineThicknessSelected != value)
                {
                    mUnderlineThicknessSelected = value;
                    SetNeedsLayout();
                }
            }
        }
        private nfloat mFloatingLabelHeight;
        /// <summary>
        /// Gets or sets the height of the floating hint label.
        /// </summary>
        public virtual nfloat FloatingLabelHeight
        {
            get => mFloatingLabelHeight;
            set
            {
                if (mFloatingLabelHeight != value)
                {
                    mFloatingLabelHeight = value;
                    SetNeedsLayout();
                }
            }
        }
        private UIColor mControlNormalColor;
        /// <summary>
        /// Gets or sets the color to use when the control is in the normal state.
        /// </summary>
        public virtual UIColor ControlNormalColor
        {
            get => mControlNormalColor;
            set
            {
                if (mControlNormalColor != value)
                {
                    mControlNormalColor = value;
                    SetNeedsLayout();
                }
            }
        }
        private UIColor mAccentColor;
        /// <summary>
        /// Gets or sets the accent color to use.
        /// </summary>
        public virtual UIColor AccentColor
        {
            get => mAccentColor;
            set
            {
                if (mAccentColor != value)
                {
                    mAccentColor = value;
                    SetNeedsLayout();
                }
            }
        }
        private UIColor mPlaceholderColor;
        /// <summary>
        /// Gets or sets the placeholder color.
        /// </summary>
        public virtual UIColor PlaceholderColor
        {
            get => mPlaceholderColor;
            set
            {
                if (mPlaceholderColor != value)
                {
                    mPlaceholderColor = value;
                    SetNeedsLayout();
                }
            }
        }

        protected CALayer UnderlineLayer { get; private set; }
        /// <summary>
        /// Gets the <see cref="UITextView" /> embedded in this <see cref="MultilineEditText" />.
        /// </summary>
        public virtual UITextView TextView { get; private set; }
        protected CATextLayer HintLayer { get; private set; }

        private string mHint;
        /// <summary>
        /// Gets or sets the hint text.
        /// </summary>
        public virtual string Hint
        {
            get => mHint;
            set
            {
                if (mHint != value)
                {
                    mHint = value;
                    HintLayer.String = value;
                }
            }
        }
        /// <summary>
        /// Gets or sets the placeholder text.
        /// </summary>
        public virtual string Placeholder
        {
            get => Hint;
            set => Hint = value;
        }

        private bool mShowFloatingLabel;
        /// <summary>
        /// Gets or sets a value that determines if the floating label should be visible.
        /// </summary>
        public bool ShowFloatingLabel
        {
            get => mShowFloatingLabel;
            set
            {
                if (mShowFloatingLabel != value)
                {
                    mShowFloatingLabel = value;
                    SetNeedsLayout();
                }
            }
        }

        /// <summary>
        /// This property will set the text to display, or retrieve the text that is being displayed.
        /// </summary>
        public virtual string Text
        {
            get => TextView.Text;
            set
            {
                if (TextView.Text != value)
                {
                    TextView.Text = value;
                    OnTextChanged();
                }
            }
        }

        /// <summary>
        /// This property holds the font that will be used to display the text.
        /// </summary>
        public virtual UIFont Font
        {
            get => TextView.Font;
            set => TextView.Font = value;
        }

        /// <summary>
        /// The maximum number of lines that can be stored in the receiver.
        /// </summary>
        public virtual nuint MaximumNumberOfLines
        {
            get => TextView.TextContainer.MaximumNumberOfLines;
            set => TextView.TextContainer.MaximumNumberOfLines = value;
        }

        /// <summary>
        /// The behavior of the last line inside this NSTextContainer.
        /// </summary>
        public virtual UILineBreakMode LineBreakMode
        {
            get => TextView.TextContainer.LineBreakMode;
            set => TextView.TextContainer.LineBreakMode = value;
        }

        /// <summary>
        /// Returns whether this <see cref="MultilineEditText" /> is the First Responder.
        /// </summary>
        public override bool IsFirstResponder => TextView.IsFirstResponder;
        /// <summary>
        /// Called when this <see cref="MultilineEditText" /> has been asked to resign its first responder status.
        /// </summary>
        /// <returns></returns>
        public override bool ResignFirstResponder()
        {
            return TextView.ResignFirstResponder();
        }
        /// <summary>
        /// Determines whether this <see cref="MultilineEditText" /> is willing to become first responder.
        /// </summary>
        public override bool CanBecomeFirstResponder => TextView.CanBecomeFirstResponder;
        /// <summary>
        /// Determines whether this <see cref="MultilineEditText" /> is willing to give up its first responder status.
        /// </summary>
        public override bool CanResignFirstResponder => TextView.CanResignFirstResponder;
        /// <summary>
        /// Request the object to become the first responder.
        /// </summary>
        /// <returns></returns>
        public override bool BecomeFirstResponder()
        {
            return TextView.BecomeFirstResponder();
        }

        public MultilineEditText() : base() { Initialize(); }
        public MultilineEditText(CGRect frame) : base(frame) { Initialize(); }
        public MultilineEditText(IntPtr handle) : base(handle) { Initialize(); }
        public MultilineEditText(NSObjectFlag t) : base(t) { Initialize(); }
        public MultilineEditText(NSCoder coder) : base(coder) { Initialize(); }

        private void Initialize()
        {
            mUnderlineThickness = 1.4f;
            mUnderlineThicknessSelected = 2.0f;
            mFloatingLabelHeight = UIFont.SystemFontOfSize(UIFont.SmallSystemFontSize).LineHeight;
            mControlNormalColor = UIColor.LabelColor;
            mPlaceholderColor = UIColor.LabelColor.ColorWithAlpha(0.25f);
            mAccentColor = UIColor.SystemBlueColor;
            mShowFloatingLabel = true;

            TextView = new UITextView();
            TextView.BackgroundColor = UIColor.FromRGBA(0, 0, 0, 0);
            TextView.Font = UIFont.PreferredBody;
            TextView.ContentInset = new UIEdgeInsets(TextView.ContentInset.Top, 0, TextView.ContentInset.Bottom, 0);
            TextView.TextContainer.LineFragmentPadding = 0;
            TextView.TextContainerInset = new UIEdgeInsets(TextView.TextContainerInset.Top, 0, TextView.TextContainerInset.Bottom, 0);
            AddSubview(TextView);

            UnderlineLayer = new CALayer();
            UnderlineLayer.BackgroundColor = ControlNormalColor.CGColor;
            Layer.AddSublayer(UnderlineLayer);

            HintLayer = new CATextLayer();
            HintLayer.ContentsScale = UIScreen.MainScreen.Scale;
            HintLayer.String = Hint;
            Layer.AddSublayer(HintLayer);

            EditingDidBegin += (sender, eventArgs) =>
            {
                if (ShowFloatingLabel)
                {
                    HintLayer.Hidden = false;
                }
                else
                {
                    HintLayer.Hidden = true;
                }

                HintLayer.FontSize = UIFont.SmallSystemFontSize;
                HintLayer.Frame = new CGRect(0, 0, Frame.Width, FloatingLabelHeight);

                HintLayer.ForegroundColor = AccentColor.CGColor;
                UnderlineLayer.BackgroundColor = AccentColor.CGColor;
                UnderlineLayer.Frame = new CGRect(0, FloatingLabelHeight + TextView.Frame.Height, TextView.Frame.Width, UnderlineThicknessSelected);
            };

            EditingDidEnd += (sender, eventArgs) =>
            {
                if (!Text.IsNullOrEmpty())
                {
                    if (ShowFloatingLabel)
                    {
                        HintLayer.Hidden = false;
                    }
                    else
                    {
                        HintLayer.Hidden = true;
                    }

                    HintLayer.FontSize = UIFont.SmallSystemFontSize;
                    HintLayer.Frame = new CGRect(0, 0, TextView.Frame.Width, FloatingLabelHeight);

                }
                else
                {
                    HintLayer.Hidden = false;
                    HintLayer.FontSize = Font.PointSize;
                    HintLayer.Frame = new CGRect(0, FloatingLabelHeight, TextView.Frame.Width, TextView.Frame.Height);
                }

                HintLayer.ForegroundColor = ControlNormalColor.CGColor;
                UnderlineLayer.BackgroundColor = ControlNormalColor.CGColor;
                UnderlineLayer.Frame = new CGRect(0, FloatingLabelHeight + TextView.Frame.Height, TextView.Frame.Width, UnderlineThickness);
            };

            TextDidChange += (sender, eventArgs) =>
            {
                OnTextChanged();
            };

            TextView.Delegate = this;
        }

        public override void LayoutSubviews()
        {
            TextView.Frame = new CGRect(0, FloatingLabelHeight, Frame.Width, Frame.Height - FloatingLabelHeight - Math.Max(UnderlineThickness, UnderlineThicknessSelected));
            
            if (!Text.IsNullOrEmpty() || IsFirstResponder)
            {
                if (ShowFloatingLabel)
                {
                    HintLayer.Hidden = false;
                }
                else
                {
                    HintLayer.Hidden = true;
                }

                if (IsFirstResponder)
                {
                    HintLayer.ForegroundColor = AccentColor.CGColor;
                }
                else
                {
                    HintLayer.ForegroundColor = ControlNormalColor.CGColor;
                }

                HintLayer.FontSize = UIFont.SmallSystemFontSize;
                HintLayer.Frame = new CGRect(0, 0, TextView.Frame.Width, FloatingLabelHeight);
            }
            else
            {
                HintLayer.Hidden = false;
                HintLayer.ForegroundColor = PlaceholderColor.CGColor;
                HintLayer.FontSize = Font.PointSize;
                HintLayer.Frame = new CGRect(0, FloatingLabelHeight, TextView.Frame.Width, TextView.Frame.Height);
            }

            if (IsFirstResponder)
            {
                UnderlineLayer.Frame = new CGRect(0, FloatingLabelHeight + TextView.Frame.Height, TextView.Frame.Width, UnderlineThicknessSelected);
                UnderlineLayer.BackgroundColor = AccentColor.CGColor;
            }
            else
            {
                UnderlineLayer.Frame = new CGRect(0, FloatingLabelHeight + TextView.Frame.Height, TextView.Frame.Width, UnderlineThickness);
                UnderlineLayer.BackgroundColor = ControlNormalColor.CGColor;
            }
        }

        public override CGSize SizeThatFits(CGSize size)
        {
            CGSize textViewSizeThatFits = TextView.SizeThatFits(size);
            return new CGSize(textViewSizeThatFits.Width, textViewSizeThatFits.Height + FloatingLabelHeight + Math.Max(UnderlineThickness, UnderlineThicknessSelected));
        }

        [Export("textViewDidBeginEditing:")]
        public virtual void TextViewDidBeginEditing(UITextView textView)
        {
            EditingDidBegin?.Invoke(this, new EventArgs());
        }

        [Export("textViewDidEndEditing:")]
        public virtual void TextViewDidEndEditing(UITextView textView)
        {
            EditingDidEnd?.Invoke(this, new EventArgs());
        }

        [Export("textViewDidChange:")]
        public virtual void TextViewDidChange(UITextView textView)
        {
            TextDidChange?.Invoke(this, new EventArgs());
        }

        public virtual void OnTextChanged()
        {
            if (Superview is IViewGroup && this.GetLayoutParameters() != null && this.GetLayoutParameters().Width == ViewGroup.LayoutParameters.WrapContent || this.GetLayoutParameters().Height == ViewGroup.LayoutParameters.WrapContent)
            {
                Superview.SetNeedsLayout();
            }
        }
    }
}