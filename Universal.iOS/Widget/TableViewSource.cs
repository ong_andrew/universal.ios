﻿using Foundation;
using System;
using System.Collections.Generic;
using System.Linq;
using UIKit;

namespace Universal.iOS.Widget
{
    /// <summary>
    /// A generic <see cref="UITableViewSource" /> that manages a nested enumerable.
    /// </summary>
    /// <typeparam name="TTableViewCell"></typeparam>
    /// <typeparam name="TItem"></typeparam>
    public class TableViewSource<TTableViewCell, TItem> : UITableViewSource
        where TTableViewCell : UITableViewCell
    {
        /// <summary>
        /// Called by the <see cref="TableViewSource{TTableViewCell, TItem}"/> when a new cell should be created.
        /// </summary>
        /// <param name="reuseIdentifier">The reuse identifier that should be set for the cell so that it is reused.</param>
        /// <returns></returns>
        public delegate TTableViewCell CreateCellDelegate(string reuseIdentifier);
        /// <summary>
        /// Called by the <see cref="TableViewSource{TTableViewCell, TItem}"/> when a cell should be bound with data.
        /// </summary>
        /// <param name="tableViewCell"></param>
        /// <param name="item"></param>
        public delegate void BindCellDelegate(TTableViewCell tableViewCell, TItem item);
        protected CreateCellDelegate mCreateCellDelegate;
        protected BindCellDelegate mBindCellDelegate;
        /// <summary>
        /// Gets the items managed by this <see cref="TableViewSource{TTableViewCell, TItem}" />.
        /// </summary>
        public IEnumerable<IEnumerable<TItem>> Items { get; protected set; }

        /// <summary>
        /// Instantiates a new <see cref="TableViewSource{TTableViewCell, TItem}"/> with a single-section element set for the given items.
        /// </summary>
        /// <param name="items"></param>
        /// <param name="createCellDelegate"></param>
        /// <param name="bindCellDelegate"></param>
        public TableViewSource(IEnumerable<TItem> items, CreateCellDelegate createCellDelegate, BindCellDelegate bindCellDelegate) : this(new List<IEnumerable<TItem>>() { items }, createCellDelegate, bindCellDelegate) { }
        /// <summary>
        /// Instantiates a new <see cref="TableViewSource{TTableViewCell, TItem}"/> with a single-section element set for the given items.
        /// </summary>
        /// <param name="items"></param>
        /// <param name="createCellDelegate"></param>
        /// <param name="bindCellDelegate"></param>
        public TableViewSource(IEnumerable<IEnumerable<TItem>> items, CreateCellDelegate createCellDelegate, BindCellDelegate bindCellDelegate)
        {
            Items = items;
            mCreateCellDelegate = createCellDelegate;
            mBindCellDelegate = bindCellDelegate;
        }

        public override UITableViewCell GetCell(UITableView tableView, NSIndexPath indexPath)
        {
            TTableViewCell tableViewCell = tableView.DequeueReusableCell(typeof(TTableViewCell).ToString()) as TTableViewCell;
            if (tableViewCell == null)
            {
                tableViewCell = mCreateCellDelegate(typeof(TTableViewCell).ToString());
            }

            TItem item = Items.ElementAt(indexPath.Section).ElementAt(indexPath.Row);
            mBindCellDelegate(tableViewCell, item);

            return tableViewCell;
        }

        public override nint RowsInSection(UITableView tableview, nint section)
        {
            return Items.ElementAt((int)section).Count();
        }
    }
}