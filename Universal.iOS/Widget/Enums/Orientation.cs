﻿namespace Universal.iOS.Widget
{
    /// <summary>
    /// Enumeration for the possible orientations of a widget.
    /// </summary>
    public enum Orientation
    {
        Horizontal,
        Vertical
    }
}