﻿using CoreGraphics;
using Foundation;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using UIKit;
using Universal.Common;
//using Universal.Common.Extensions;
using Universal.iOS.Extensions;
using Universal.iOS.Views;

namespace Universal.iOS.Widget
{
    /// <summary>
    /// A layout that arranges its children in a single column or a single row.
    /// </summary>
    [Register("LinearLayout")]
    [DesignTimeVisible(true)]
    public class LinearLayout : ViewGroup
    {
        protected GravityFlags mGravity;
        /// <summary>
        /// Gets or sets a value for the gravity for the <see cref="LinearLayout"/>.
        /// </summary>
        public GravityFlags Gravity
        {
            get => mGravity;
            set
            {
                if (mGravity != value)
                {
                    mGravity = value;
                    SetNeedsLayout();
                }
            }
        }

        protected Orientation mOrientation;
        /// <summary>
        /// Gets or sets the orientation for the <see cref="LinearLayout"/>.
        /// </summary>
        public Orientation Orientation
        {
            get => mOrientation;
            set
            {
                if (mOrientation != value)
                {
                    mOrientation = value;
                    SetNeedsLayout();
                }
            }
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="LinearLayout"/> class.
        /// </summary>
        public LinearLayout() { Initialize(); }
        /// <summary>
        /// Initializes a new instance of the <see cref="LinearLayout"/> class.
        /// </summary>
        /// <param name="coder"></param>
        public LinearLayout(NSCoder coder) : base(coder) { Initialize(); }
        /// <summary>
        /// Initializes a new instance of the <see cref="LinearLayout"/> class.
        /// </summary>
        /// <param name="ptr"></param>
        public LinearLayout(IntPtr ptr) : base(ptr) { Initialize(); }
        /// <summary>
        /// Initializes a new instance of the <see cref="LinearLayout"/> class.
        /// </summary>
        /// <param name="frame"></param>
        public LinearLayout(CGRect frame) : base(frame) { Initialize(); }
        /// <summary>
        /// Initializes a new instance of the <see cref="LinearLayout"/> class.
        /// </summary>
        /// <param name="t"></param>
        public LinearLayout(NSObjectFlag t) : base(t) { Initialize(); }

        private void Initialize()
        {
        }

        /// <summary>
        /// Adds the <paramref name="view"/> to the <see cref="LinearLayout"/>, applying default <see cref="LayoutParameters"/> in the process if one is not found.
        /// </summary>
        /// <param name="view"></param>
        public override void AddSubview(UIView view)
        {
            if (view.GetLayoutParameters() == null)
            {
                if (view.Frame.Height != 0 || view.Frame.Width != 0)
                {
                    view.SetLayoutParameters(new LinearLayout.LayoutParameters((int)view.Frame.Width, (int)view.Frame.Height));
                }
                else
                {
                    view.SetLayoutParameters(new LayoutParameters(ViewGroup.LayoutParameters.WrapContent, ViewGroup.LayoutParameters.WrapContent));
                }
            }

            if (!(view.GetLayoutParameters() is LayoutParameters))
            {
                view.SetLayoutParameters(new LayoutParameters(view.GetLayoutParameters()));
            }

            base.AddSubview(view);
        }

        public virtual void AddSubview(UIView view, LinearLayout.LayoutParameters layoutParameters)
        {
            if (layoutParameters is null)
            {
                throw new ArgumentNullException(nameof(layoutParameters));
            }

            view.SetLayoutParameters(layoutParameters);
            AddSubview(view);
        }

        public override void MovedToSuperview()
        {
            base.MovedToSuperview();
            SetNeedsLayout();
        }

        protected virtual IEnumerable<CGRect> LayoutSubviewsWithFrame(CGRect frame)
        {
            List<CGRect> computedSubviewFrames = new List<CGRect>();
            for (int i = 0; i < Subviews.Count(); i++)
            {
                UIView subview = Subviews.ElementAt(i);
                LayoutParameters subviewLayoutParameters = GetLayoutParameters(subview);

                CGSize size = MeasureViewSize(frame.Size, subview);

                UIView precedingSubview = i - 1 > 0 ? Subviews.ElementAt(i - 1) : null;
                IEnumerable<CGRect> precedingSubviewsFrames = computedSubviewFrames.Take(i);
                CGRect precedingSubviewFrame = precedingSubviewsFrames.LastOrDefault();
                LayoutParameters precedingSubviewLayoutParameters = precedingSubview != null && GetLayoutParameters(precedingSubview).Visibility != ViewStates.Gone ? GetLayoutParameters(precedingSubview) : null;

                if (Orientation == Orientation.Horizontal)
                {
                    if (subviewLayoutParameters.Visibility != ViewStates.Gone)
                    {
                        computedSubviewFrames.Add(new CGRect(precedingSubviewFrame != null ? precedingSubviewFrame.Right + (precedingSubviewLayoutParameters?.MarginRight ?? 0) : PaddingLeft + subviewLayoutParameters.MarginLeft, PaddingTop + subviewLayoutParameters.MarginTop, size.Width, size.Height));
                    }
                    else
                    {
                        computedSubviewFrames.Add(new CGRect(precedingSubviewFrame != null ? precedingSubviewFrame.Right + (precedingSubviewLayoutParameters?.MarginRight ?? 0) : 0, 0, size.Width, size.Height));
                    }
                }
                else
                {
                    if (subviewLayoutParameters.Visibility != ViewStates.Gone)
                    {
                        computedSubviewFrames.Add(new CGRect(PaddingLeft + subviewLayoutParameters.MarginLeft, (precedingSubviewFrame != null ? precedingSubviewFrame.Bottom + (precedingSubviewLayoutParameters?.MarginBottom ?? 0) : PaddingTop), size.Width, size.Height));
                    }
                    else
                    {
                        computedSubviewFrames.Add(new CGRect(0, (precedingSubviewFrame != null ? precedingSubviewFrame.Bottom + (precedingSubviewLayoutParameters?.MarginBottom ?? 0) : 0), size.Width, size.Height));
                    }
                }
            }

            IEnumerable<(CGRect Frame, LayoutParameters LayoutParameters)> computedSubviewData = computedSubviewFrames.Zip(SubviewLayoutParameters, (x, y) => (x, y));

            int totalSubviewWeight = Subviews.Select(x => GetLayoutParameters(x)).Where(x => x.Visibility != ViewStates.Gone).Sum(x => x.Weight);
            double remainingWidth = frame.Width - PaddingLeft - PaddingRight - (computedSubviewData.Any(x => x.LayoutParameters.Visibility != ViewStates.Gone) ? computedSubviewData.Where(x => x.LayoutParameters.Visibility != ViewStates.Gone).Sum(x => x.Frame.Width + x.LayoutParameters.MarginLeft + x.LayoutParameters.MarginRight) : 0);
            double remainingHeight = frame.Height - PaddingTop - PaddingBottom - (computedSubviewData.Any(x => x.LayoutParameters.Visibility != ViewStates.Gone) ? computedSubviewData.Where(x => x.LayoutParameters.Visibility != ViewStates.Gone).Sum(x => x.Frame.Height + x.LayoutParameters.MarginTop + x.LayoutParameters.MarginBottom) : 0);

            // Layout pass to assign weights.
            for (int i = 0; i < Subviews.Count(); i++)
            {
                UIView subview = Subviews.ElementAt(i);
                CGRect computedSubviewFrame = computedSubviewFrames[i];
                LayoutParameters layoutParameters = GetLayoutParameters(subview);

                if (layoutParameters.Visibility == ViewStates.Gone)
                {
                    continue;
                }

                // Assign remaining dimension if weights are present.
                if (layoutParameters.Weight > 0)
                {
                    if (Orientation == Orientation.Horizontal && remainingWidth > 0)
                    {
                        double width = computedSubviewFrame.Width + (layoutParameters.Weight * (remainingWidth / totalSubviewWeight));
                        computedSubviewFrame = new CGRect(computedSubviewFrame.X, computedSubviewFrame.Y, width, computedSubviewFrame.Height);
                        computedSubviewFrames = computedSubviewFrames.Replace(i, computedSubviewFrame).ToList();
                    }
                    else if (Orientation == Orientation.Vertical && remainingHeight > 0)
                    {
                        double height = computedSubviewFrame.Height + (layoutParameters.Weight * (remainingHeight / totalSubviewWeight));
                        computedSubviewFrame = new CGRect(computedSubviewFrame.X, computedSubviewFrame.Y, computedSubviewFrame.Width, height);
                        computedSubviewFrames = computedSubviewFrames.Replace(i, computedSubviewFrame).ToList();
                    }
                }
            }

            // Give views a chance to resize their dimension.
            for (int i = 0; i < Subviews.Count(); i++)
            {
                UIView subview = Subviews.ElementAt(i);
                CGRect computedSubviewFrame = computedSubviewFrames[i];
                LayoutParameters layoutParameters = GetLayoutParameters(subview);

                if (layoutParameters.Visibility == ViewStates.Gone)
                {
                    continue;
                }

                CGSize idealSize = subview.SizeThatFits(computedSubviewFrame.Size);

                if (layoutParameters.Weight != 0)
                {
                    if (Orientation == Orientation.Horizontal)
                    {
                        //computedSubviewFrame = new CGRect(computedSubviewFrame.Location, new CGSize(idealSize.Width, idealSize.Height));
                        computedSubviewFrame = new CGRect(computedSubviewFrame.Location, new CGSize(computedSubviewFrame.Size.Width, idealSize.Height));
                        computedSubviewFrames = computedSubviewFrames.Replace(i, computedSubviewFrame).ToList();
                    }
                    else if (Orientation == Orientation.Vertical)
                    {
                        //computedSubviewFrame = new CGRect(computedSubviewFrame.Location, new CGSize(idealSize.Width, idealSize.Height));
                        computedSubviewFrame = new CGRect(computedSubviewFrame.Location, new CGSize(idealSize.Width, computedSubviewFrame.Size.Height));
                        computedSubviewFrames = computedSubviewFrames.Replace(i, computedSubviewFrame).ToList();
                    }
                }
            }

            computedSubviewData = computedSubviewFrames.Zip(SubviewLayoutParameters, (x, y) => (x, y));

            // A layout pass setting more fine-tuned gravity and parameters.
            for (int i = 0; i < Subviews.Count(); i++)
            {
                UIView subview = Subviews.ElementAt(i);
                CGRect computedSubviewFrame = computedSubviewFrames[i];
                LayoutParameters layoutParameters = GetLayoutParameters(subview);

                if (layoutParameters.Width == ViewGroup.LayoutParameters.MatchParent)
                {
                    if (layoutParameters.Visibility != ViewStates.Gone)
                    {
                        computedSubviewFrame = new CGRect(computedSubviewFrame.X, computedSubviewFrame.Y, frame.Width - PaddingLeft - PaddingRight - layoutParameters.MarginLeft - layoutParameters.MarginRight, computedSubviewFrame.Height);
                    }
                    else
                    {
                        computedSubviewFrame = new CGRect(computedSubviewFrame.X, computedSubviewFrame.Y, 0, 0);
                    }
                }

                if (layoutParameters.Height == ViewGroup.LayoutParameters.MatchParent)
                {
                    if (layoutParameters.Visibility != ViewStates.Gone)
                    {
                        computedSubviewFrame = new CGRect(computedSubviewFrame.X, computedSubviewFrame.Y, computedSubviewFrame.Width, frame.Height - PaddingTop - PaddingBottom - layoutParameters.MarginTop - layoutParameters.MarginBottom);
                    }
                    else
                    {
                        computedSubviewFrame = new CGRect(computedSubviewFrame.X, computedSubviewFrame.Y, 0, 0);
                    }
                }

                GravityFlags compoundGravity = layoutParameters.Gravity | Gravity;

                // Reset X and Y due to gravity.
                if (Orientation == Orientation.Horizontal)
                {
                    if ((compoundGravity & GravityFlags.Top) == GravityFlags.Top)
                    {
                        // Do nothing as we computed it for this scenario to get the sizing.
                    }
                    else if ((compoundGravity & GravityFlags.CenterVertical) == GravityFlags.CenterVertical)
                    {
                        nfloat y = (nfloat)((frame.Height / 2.0) - (computedSubviewFrame.Height / 2.0)) + PaddingTop - PaddingBottom;
                        if (layoutParameters.Visibility != ViewStates.Gone)
                        {
                            y += layoutParameters.MarginTop - layoutParameters.MarginBottom;
                        }
                        computedSubviewFrame = new CGRect(computedSubviewFrame.X, y, computedSubviewFrame.Width, computedSubviewFrame.Height);
                    }
                    else if ((compoundGravity & GravityFlags.Bottom) == GravityFlags.Bottom)
                    {
                        nfloat y = frame.Height - PaddingBottom - computedSubviewFrame.Height;
                        if (layoutParameters.Visibility != ViewStates.Gone)
                        {
                            y += -layoutParameters.MarginBottom;
                        }
                        computedSubviewFrame = new CGRect(computedSubviewFrame.X, y, computedSubviewFrame.Width, computedSubviewFrame.Height);
                    }
                }
                else
                {
                    if ((compoundGravity & GravityFlags.Left) == GravityFlags.Left)
                    {
                        // Do nothing as we computed it for this scenario to get the sizing.
                    }
                    else if ((compoundGravity & GravityFlags.CenterHorizontal) == GravityFlags.CenterHorizontal)
                    {
                        nfloat x = (nfloat)((frame.Width / 2.0) - (computedSubviewFrame.Width / 2.0)) + PaddingLeft - PaddingRight;
                        if (layoutParameters.Visibility != ViewStates.Gone)
                        {
                            x += layoutParameters.MarginLeft - layoutParameters.MarginRight;
                        }
                        computedSubviewFrame = new CGRect(x, computedSubviewFrame.Y, computedSubviewFrame.Width, computedSubviewFrame.Height);
                    }
                    else if ((compoundGravity & GravityFlags.Right) == GravityFlags.Right)
                    {
                        nfloat x = frame.Width - PaddingRight - computedSubviewFrame.Width;
                        if (layoutParameters.Visibility != ViewStates.Gone)
                        {
                            x += -layoutParameters.MarginRight;
                        }
                        computedSubviewFrame = new CGRect(x, computedSubviewFrame.Y, computedSubviewFrame.Width, computedSubviewFrame.Height);
                    }
                }

                // Update main coordinate, in case previous operations altered widths or heights.
                if (Orientation == Orientation.Horizontal)
                {
                    double x = PaddingLeft;
                    for (int j = 0; j < i; j++)
                    {
                        LayoutParameters jLayoutParameters = SubviewLayoutParameters.ElementAt(j);
                        if (jLayoutParameters.Visibility != ViewStates.Gone)
                        {
                            x += computedSubviewFrames[j].Width + jLayoutParameters.MarginLeft + jLayoutParameters.MarginRight;
                        }
                    }
                    if (layoutParameters.Visibility != ViewStates.Gone)
                    {
                        x += layoutParameters.MarginLeft;
                    }

                    computedSubviewFrame = new CGRect(x, computedSubviewFrame.Y, computedSubviewFrame.Width, computedSubviewFrame.Height);
                }
                else
                {
                    double y = PaddingTop;
                    for (int j = 0; j < i; j++)
                    {
                        LayoutParameters jLayoutParameters = SubviewLayoutParameters.ElementAt(j);
                        if (jLayoutParameters.Visibility != ViewStates.Gone)
                        {
                            y += computedSubviewFrames[j].Height + jLayoutParameters.MarginTop + jLayoutParameters.MarginBottom;
                        }
                    }
                    if (layoutParameters.Visibility != ViewStates.Gone)
                    {
                        y += layoutParameters.MarginTop;
                    }

                    computedSubviewFrame = new CGRect(computedSubviewFrame.X, y, computedSubviewFrame.Width, computedSubviewFrame.Height);
                }

                computedSubviewFrames = computedSubviewFrames.Replace(i, computedSubviewFrame).ToList();
            }

            computedSubviewData = computedSubviewFrames.Zip(SubviewLayoutParameters, (x, y) => (x, y));

            remainingWidth = frame.Width - PaddingLeft - PaddingRight - (computedSubviewData.Any(x => x.LayoutParameters.Visibility != ViewStates.Gone) ? computedSubviewData.Where(x => x.LayoutParameters.Visibility != ViewStates.Gone).Sum(x => x.Frame.Width + x.LayoutParameters.MarginLeft + x.LayoutParameters.MarginRight) : 0);
            remainingHeight = frame.Height - PaddingTop - PaddingBottom - (computedSubviewData.Any(x => x.LayoutParameters.Visibility != ViewStates.Gone) ? computedSubviewData.Where(x => x.LayoutParameters.Visibility != ViewStates.Gone).Sum(x => x.Frame.Height + x.LayoutParameters.MarginTop + x.LayoutParameters.MarginBottom) : 0);

            double xGravityOffset = 0;
            double yGravityOffset = 0;

            if (Orientation == Orientation.Horizontal && remainingWidth > 0)
            {
                if ((Gravity & GravityFlags.Left) == GravityFlags.Left)
                {
                    // Do nothing.
                }
                else if ((Gravity & GravityFlags.CenterHorizontal) == GravityFlags.CenterHorizontal)
                {
                    xGravityOffset = remainingWidth / 2;
                }
                else if ((Gravity & GravityFlags.Right) == GravityFlags.Right)
                {
                    xGravityOffset = remainingWidth;
                }
            }
            else if (Orientation == Orientation.Vertical && remainingHeight > 0)
            {
                if ((Gravity & GravityFlags.Top) == GravityFlags.Top)
                {
                    // Do nothing.
                }
                else if ((Gravity & GravityFlags.CenterVertical) == GravityFlags.CenterVertical)
                {
                    yGravityOffset = remainingHeight / 2;
                }
                else if ((Gravity & GravityFlags.Bottom) == GravityFlags.Bottom)
                {
                    yGravityOffset = remainingHeight;
                }
            }

            // Pass to compute our own gravity, if any dimension remains.
            for (int i = 0; i < Subviews.Count(); i++)
            {
                UIView subview = Subviews.ElementAt(i);
                CGRect computedSubviewFrame = computedSubviewFrames[i];

                if (xGravityOffset != 0 || yGravityOffset != 0)
                {
                    computedSubviewFrame = new CGRect(new CGPoint(computedSubviewFrame.X + xGravityOffset, computedSubviewFrame.Y + yGravityOffset), computedSubviewFrame.Size);
                    computedSubviewFrames = computedSubviewFrames.Replace(i, computedSubviewFrame).ToList();
                }
            }

            return computedSubviewFrames;
        }

        public override void LayoutSubviews()
        {
            IEnumerable<CGRect> subviewFrames = LayoutSubviewsWithFrame(Frame);

            for (int i = 0; i < Subviews.Count(); i++)
            {
                Subviews.ElementAt(i).SetFrameIfNeeded(subviewFrames.ElementAt(i));
            }
        }

        /// <summary>
        /// Gets the <see cref="LayoutParameters"/> for the given <see cref="UIView"/>, throwing an exception if 
        /// </summary>
        /// <param name="uiView"></param>
        /// <returns></returns>
        protected virtual LayoutParameters GetLayoutParameters(UIView uiView)
        {
            ViewGroup.LayoutParameters layoutParameters = uiView.GetLayoutParameters();

            if (layoutParameters is LayoutParameters)
            {
                return (LayoutParameters)layoutParameters;
            }
            else if (layoutParameters == null)
            {
                throw new LayoutParametersNotFoundException("No layout parameters found for the given child view.");
            }
            else
            {
                throw new InvalidLayoutParametersException($"Layout parameters must be of type {typeof(LayoutParameters)}, found one of type {layoutParameters.GetType()}.");
            }
        }

        /// <summary>
        /// Gets the layout parameters for the subview of this <see cref="LinearLayout"/>.
        /// </summary>
        public IEnumerable<LayoutParameters> SubviewLayoutParameters
        {
            get
            {
                return Subviews.Select(x => GetLayoutParameters(x));
            }
        }

        /// <summary>
        /// Estimate the size that fits for this view disregarding our own layout parameters.
        /// </summary>
        /// <param name="size"></param>
        /// <returns></returns>
        public override CGSize SizeThatFitsSubviews(CGSize size)
        {
            nfloat availableWidth = size.Width;
            nfloat availableHeight = size.Height;
            nfloat width = 0;
            nfloat height = 0;
            double totalWeight = 0;
            List<CGSize> subviewSizes = new List<CGSize>();

            foreach (UIView subview in Subviews)
            {
                LayoutParameters subviewLayoutParameters = GetLayoutParameters(subview);
                CGSize availableSize = new CGSize(availableWidth - subviewLayoutParameters.MarginLeft - subviewLayoutParameters.MarginRight, availableHeight - subviewLayoutParameters.MarginTop - subviewLayoutParameters.MarginBottom);
                if (availableSize.Width < 0)
                {
                    availableSize = new CGSize(0, availableSize.Height);
                }
                if (availableSize.Height < 0)
                {
                    availableSize = new CGSize(availableSize.Width, 0);
                }
                CGSize subviewSizeThatFits = subview.SizeThatFits(availableSize);

                nfloat subviewRequiredWidth = subviewLayoutParameters.MarginLeft + subviewLayoutParameters.MarginRight;
                nfloat subviewRequiredHeight = subviewLayoutParameters.MarginTop + subviewLayoutParameters.MarginBottom;

                if (subviewLayoutParameters.Width >= 0)
                {
                    subviewRequiredWidth += subviewLayoutParameters.Width;
                }
                else if (subviewLayoutParameters.Width == ViewGroup.LayoutParameters.WrapContent || subviewLayoutParameters.Width == ViewGroup.LayoutParameters.MatchParent)
                {
                    subviewRequiredWidth += subviewSizeThatFits.Width;
                }

                if (subviewLayoutParameters.Height >= 0)
                {
                    subviewRequiredHeight += subviewLayoutParameters.Height;
                }
                else if (subviewLayoutParameters.Height == ViewGroup.LayoutParameters.WrapContent || subviewLayoutParameters.Height == ViewGroup.LayoutParameters.MatchParent)
                {
                    subviewRequiredHeight += subviewSizeThatFits.Height;
                }

                if (subviewLayoutParameters.Visibility == ViewStates.Gone)
                {
                    subviewRequiredWidth = 0;
                    subviewRequiredHeight = 0;
                }
                else
                {
                    totalWeight += subviewLayoutParameters.Weight;
                }

                if (Orientation == Orientation.Horizontal)
                {
                    if (subviewRequiredHeight > height)
                    {
                        height = subviewRequiredHeight;
                    }

                    width += subviewRequiredWidth;
                    availableWidth -= subviewRequiredWidth;
                    if (availableWidth < 0)
                    {
                        availableWidth = 0;
                    }
                }
                else if (Orientation == Orientation.Vertical)
                {
                    if (subviewRequiredWidth > width)
                    {
                        width = subviewRequiredWidth;
                    }

                    height += subviewRequiredHeight;
                    availableHeight -= subviewRequiredHeight;
                    if (availableHeight < 0)
                    {
                        availableHeight = 0;
                    }
                }

                subviewSizes.Add(new CGSize(subviewRequiredWidth, subviewRequiredHeight));
            }

            if (totalWeight > 0)
            {
                double excessDimension = 0;

                if (Orientation == Orientation.Horizontal && width < size.Width)
                {
                    excessDimension = size.Width - width;
                }
                else if (Orientation == Orientation.Vertical && height < size.Height)
                {
                    excessDimension = size.Height - height;
                }

                if (excessDimension > 0)
                {
                    width = 0;
                    height = 0;

                    for (int i = 0; i < Subviews.Length; i++)
                    {
                        UIView subview = Subviews[i];
                        LayoutParameters subviewLayoutParameters = GetLayoutParameters(subview);
                        CGSize availableSize = subviewSizes[i];
                        nfloat fixedWidth = new nfloat(availableSize.Width + ((subviewLayoutParameters.Weight / totalWeight) * excessDimension));
                        nfloat fixedHeight = new nfloat(availableSize.Height + ((subviewLayoutParameters.Weight / totalWeight) * excessDimension));

                        if (Orientation == Orientation.Horizontal)
                        {
                            availableSize = new CGSize(fixedWidth - subviewLayoutParameters.MarginLeft - subviewLayoutParameters.MarginRight, size.Height - subviewLayoutParameters.MarginTop - subviewLayoutParameters.MarginBottom);
                        }
                        else if (Orientation == Orientation.Vertical)
                        {
                            availableSize = new CGSize(size.Width - subviewLayoutParameters.MarginLeft - subviewLayoutParameters.MarginRight, fixedHeight - subviewLayoutParameters.MarginTop - subviewLayoutParameters.MarginBottom);
                        }

                        if (availableSize.Width < 0)
                        {
                            availableSize = new CGSize(0, availableSize.Height);
                        }
                        if (availableSize.Height < 0)
                        {
                            availableSize = new CGSize(availableSize.Width, 0);
                        }

                        CGSize subviewSizeThatFits = subview.SizeThatFits(availableSize);

                        nfloat subviewRequiredWidth = subviewLayoutParameters.MarginLeft + subviewLayoutParameters.MarginRight + subviewSizeThatFits.Width;
                        nfloat subviewRequiredHeight = subviewLayoutParameters.MarginTop + subviewLayoutParameters.MarginBottom + subviewSizeThatFits.Height;

                        if (subviewLayoutParameters.Visibility == ViewStates.Gone)
                        {
                            subviewRequiredWidth = 0;
                            subviewRequiredHeight = 0;
                        }

                        if (Orientation == Orientation.Horizontal)
                        {
                            if (subviewRequiredHeight > height)
                            {
                                height = subviewRequiredHeight;
                            }

                            width += subviewRequiredWidth;
                        }
                        else if (Orientation == Orientation.Vertical)
                        {
                            if (subviewRequiredWidth > width)
                            {
                                width = subviewRequiredWidth;
                            }

                            height += subviewRequiredHeight;
                        }

                        subviewSizes = subviewSizes.Replace(i, new CGSize(subviewRequiredWidth, subviewRequiredHeight)).ToList();
                    }
                }
            }

            return new CGSize(width, height);
        }

        public override CGSize IntrinsicContentSize
        {
            get
            {
                nfloat width = 0;
                nfloat height = 0;

                foreach (UIView subview in Subviews)
                {
                    LayoutParameters subviewLayoutParameters = GetLayoutParameters(subview);
                    CGSize subviewIntrinsicContentSize = subview.IntrinsicContentSize;

                    nfloat subviewRequiredWidth = subviewLayoutParameters.MarginLeft + subviewLayoutParameters.MarginRight;
                    nfloat subviewRequiredHeight = subviewLayoutParameters.MarginTop + subviewLayoutParameters.MarginBottom;

                    if (subviewLayoutParameters.Width >= 0)
                    {
                        subviewRequiredWidth += subviewLayoutParameters.Width;
                    }
                    else if (subviewLayoutParameters.Width == ViewGroup.LayoutParameters.WrapContent || subviewLayoutParameters.Width == ViewGroup.LayoutParameters.MatchParent)
                    {
                        subviewRequiredWidth += subviewIntrinsicContentSize.Width;
                    }

                    if (subviewLayoutParameters.Height >= 0)
                    {
                        subviewRequiredHeight += subviewLayoutParameters.Height;
                    }
                    else if (subviewLayoutParameters.Height == ViewGroup.LayoutParameters.WrapContent || subviewLayoutParameters.Height == ViewGroup.LayoutParameters.MatchParent)
                    {
                        subviewRequiredHeight += subviewIntrinsicContentSize.Height;
                    }

                    if (subviewLayoutParameters.Visibility == ViewStates.Gone)
                    {
                        subviewRequiredWidth = 0;
                        subviewRequiredHeight = 0;
                    }

                    if (Orientation == Orientation.Horizontal)
                    {
                        if (subviewRequiredHeight > height)
                        {
                            height = subviewRequiredHeight;
                        }

                        width += subviewRequiredWidth;
                    }
                    else if (Orientation == Orientation.Vertical)
                    {
                        if (subviewRequiredWidth > width)
                        {
                            width = subviewRequiredWidth;
                        }

                        height += subviewRequiredHeight;
                    }
                }

                width += PaddingLeft + PaddingRight;
                height += PaddingTop + PaddingBottom;
                return new CGSize(width, height);
            }
        }

        /// <summary>
        /// Layout parameters to used by <see cref="LinearLayout"/>.
        /// </summary>
        public new class LayoutParameters : ViewGroup.LayoutParameters
        {
            protected int mWeight;
            /// <summary>
            /// Gets or sets a value indicating the weight for the view.
            /// </summary>
            public int Weight
            {
                get => mWeight;
                set
                {
                    if (value < 0)
                    {
                        throw new InvalidOperationException("Weight must be non-negative.");
                    }

                    if (mWeight != value)
                    {
                        mWeight = value;
                        NotifyParametersChanged();
                    }
                }
            }
            protected GravityFlags mGravity;
            /// <summary>
            /// Gets or sets a value indicating the gravity for the view.
            /// </summary>
            public GravityFlags Gravity
            {
                get => mGravity;
                set
                {
                    if (mGravity != value)
                    {
                        mGravity = value;
                        NotifyParametersChanged();
                    }
                }
            }

            /// <summary>
            /// Initializes a new instance of the <see cref="LayoutParameters"/> class with the given width and height.
            /// </summary>
            /// <param name="width"></param>
            /// <param name="height"></param>
            public LayoutParameters(int width, int height) : base(width, height) { }
            /// <summary>
            /// Initializes a new instance of the <see cref="LayoutParameters"/> class with the given width, height, and weight.
            /// </summary>
            /// <param name="width"></param>
            /// <param name="height"></param>
            /// <param name="weight"></param>
            public LayoutParameters(int width, int height, int weight) : this(width, height) { mWeight = weight; }
            /// <summary>
            /// Initializes a new instance of the <see cref="LayoutParameters"/> class with the given other instance of layout parameters.
            /// </summary>
            /// <param name="other"></param>
            public LayoutParameters(ViewGroup.LayoutParameters other) : base(other) { }
        }
    }
}