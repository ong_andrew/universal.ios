﻿using CoreGraphics;
using Foundation;
using System;
using System.ComponentModel;
using UIKit;
using Universal.Common;
using Universal.iOS.Extensions;
using Universal.iOS.Views;

namespace Universal.iOS.Widget
{
    /// <summary>
    /// A layout that arranges its children relative to itself.
    /// </summary>
    [Register("RelativeLayout")]
    [DesignTimeVisible(true)]
    public class RelativeLayout : ViewGroup
    {
        public override void AddSubview(UIView view)
        {
            if (view.GetLayoutParameters() == null)
            {
                view.SetLayoutParameters(new RelativeLayout.LayoutParameters(ViewGroup.LayoutParameters.WrapContent, ViewGroup.LayoutParameters.WrapContent));
            }

            if (!(view.GetLayoutParameters() is LayoutParameters))
            {
                throw new TypeMismatchException(typeof(LayoutParameters), view.GetLayoutParameters().GetType());
            }

            base.AddSubview(view);
        }

        public virtual void AddSubview(UIView view, RelativeLayout.LayoutParameters layoutParameters)
        {
            if (layoutParameters is null)
            {
                throw new ArgumentNullException(nameof(layoutParameters));
            }

            view.SetLayoutParameters(layoutParameters);
            AddSubview(view);
        }

        public override CGSize IntrinsicContentSize
        {
            get
            {
                nfloat widthRequired = 0;
                nfloat heightRequired = 0;

                for (int i = 0; i < Subviews.Length; i++)
                {
                    UIView subview = Subviews[i];
                    LayoutParameters subviewLayoutParameters = GetLayoutParameters(subview);

                    CGSize subviewSize = subview.IntrinsicContentSize;
                    subviewSize = new CGSize(subviewSize.Width + subviewLayoutParameters.MarginLeft + subviewLayoutParameters.MarginRight, subviewSize.Height + subviewLayoutParameters.MarginTop + subviewLayoutParameters.MarginBottom);

                    if (subviewSize.Width > widthRequired)
                    {
                        widthRequired = subviewSize.Width;
                    }

                    if (subviewSize.Height > heightRequired)
                    {
                        heightRequired = subviewSize.Height;
                    }
                }

                return new CGSize(widthRequired + PaddingLeft + PaddingRight, heightRequired + PaddingTop + PaddingBottom);
            }
        }

        public override CGSize SizeThatFitsSubviews(CGSize size)
        {
            nfloat widthRequired = 0;
            nfloat heightRequired = 0;

            for (int i = 0; i < Subviews.Length; i++)
            {
                UIView subview = Subviews[i];
                LayoutParameters subviewLayoutParameters = GetLayoutParameters(subview);

                if (subviewLayoutParameters.Visibility == ViewStates.Gone)
                {
                    continue;
                }

                CGSize subviewSize = MeasureViewSize(size, subview);
                subviewSize = new CGSize(subviewSize.Width + subviewLayoutParameters.MarginLeft + subviewLayoutParameters.MarginRight, subviewSize.Height + subviewLayoutParameters.MarginTop + subviewLayoutParameters.MarginBottom);

                if (subviewSize.Width > widthRequired)
                {
                    widthRequired = subviewSize.Width;
                }

                if (subviewSize.Height > heightRequired)
                {
                    heightRequired = subviewSize.Height;
                }
            }

            return new CGSize(widthRequired, heightRequired);
        }

        public override void LayoutSubviews()
        {
            for (int i = 0; i < Subviews.Length; i++)
            {
                UIView subview = Subviews[i];
                CGSize size = MeasureViewSize(Frame.Size, subview);
                LayoutParameters layoutParameters = GetLayoutParameters(subview);

                nfloat x = 0;
                nfloat y = 0;

                if (layoutParameters.CenterHorizontal)
                {
                    x = (nfloat)((Frame.Width / 2.0) - (size.Width / 2.0)) + PaddingLeft - PaddingRight + layoutParameters.MarginLeft - layoutParameters.MarginRight;
                }
                else if (layoutParameters.AlignParentRight)
                {
                    x = Frame.Width - PaddingRight - layoutParameters.MarginRight - size.Width;
                }
                else
                {
                    x = layoutParameters.MarginLeft + PaddingLeft;
                }

                if (layoutParameters.CenterVertical)
                {
                    y = (nfloat)((Frame.Height / 2.0) - (size.Height / 2.0)) + PaddingTop - PaddingBottom + layoutParameters.MarginTop - layoutParameters.MarginBottom;
                }
                else if (layoutParameters.AlignParentBottom)
                {
                    y = Frame.Height - PaddingBottom - layoutParameters.MarginBottom - size.Height;
                }
                else
                {
                    y = layoutParameters.MarginTop + PaddingTop;
                }

                CGRect subviewFrame = new CGRect(new CGPoint(x, y), size);
                subview.SetFrameIfNeeded(subviewFrame);
            }
        }

        protected virtual LayoutParameters GetLayoutParameters(UIView uiView)
        {
            ViewGroup.LayoutParameters layoutParameters = uiView.GetLayoutParameters();

            if (layoutParameters is LayoutParameters typedLayoutParameters)
            {
                return typedLayoutParameters;
            }
            else if (layoutParameters == null)
            {
                throw new LayoutParametersNotFoundException("No layout parameters found for the given child view.");
            }
            else
            {
                throw new InvalidLayoutParametersException($"Layout parameters must be of type {typeof(LayoutParameters)}, found one of type {layoutParameters.GetType()}.");
            }
        }

        public new class LayoutParameters : ViewGroup.LayoutParameters
        {
            protected bool mAlignParentTop;
            /// <summary>
            /// Gets or sets a value indicating if the view should be aligned to its parents top.
            /// </summary>
            public bool AlignParentTop
            {
                get => mAlignParentTop;
                set
                {
                    if (mAlignParentTop != value)
                    {
                        mAlignParentTop = value;
                        NotifyParametersChanged();
                    }
                }
            }

            protected bool mAlignParentBottom;
            /// <summary>
            /// Gets or sets a value indicating if the view should be aligned to its parents bottom.
            /// </summary>
            public bool AlignParentBottom
            {
                get => mAlignParentBottom;
                set
                {
                    if (mAlignParentBottom != value)
                    {
                        mAlignParentBottom = value;
                        NotifyParametersChanged();
                    }
                }
            }

            protected bool mAlignParentLeft;
            /// <summary>
            /// Gets or sets a value indicating if the view should be aligned to its parents left.
            /// </summary>
            public bool AlignParentLeft
            {
                get => mAlignParentLeft;
                set
                {
                    if (mAlignParentLeft != value)
                    {
                        mAlignParentLeft = value;
                        NotifyParametersChanged();
                    }
                }
            }

            protected bool mAlignParentRight;
            /// <summary>
            /// Gets or sets a value indicating if the view should be aligned to its parents right.
            /// </summary>
            public bool AlignParentRight
            {
                get => mAlignParentRight;
                set
                {
                    if (mAlignParentRight != value)
                    {
                        mAlignParentRight = value;
                        NotifyParametersChanged();
                    }
                }
            }

            protected bool mCenterHorizontal;
            /// <summary>
            /// Gets or sets a value indicating if the view should be positioned center horizontally.
            /// </summary>
            public bool CenterHorizontal
            {
                get => mCenterHorizontal;
                set
                {
                    if (mCenterHorizontal != value)
                    {
                        mCenterHorizontal = value;
                        NotifyParametersChanged();
                    }
                }
            }

            protected bool mCenterVertical;
            /// <summary>
            /// Gets or sets a value indicating if the view should be positioned in the vertical center position.
            /// </summary>
            public bool CenterVertical
            {
                get => mCenterVertical;
                set
                {
                    if (mCenterVertical != value)
                    {
                        mCenterVertical = value;
                        NotifyParametersChanged();
                    }
                }
            }

            /// <summary>
            /// Gets or sets a value indicating if the view should be positioned in the center of its parent.
            /// </summary>
            public bool CenterInParent
            {
                get => mCenterHorizontal && mCenterVertical;
                set
                {
                    if (!mCenterHorizontal || !mCenterVertical)
                    {
                        mCenterHorizontal = true;
                        mCenterVertical = true;
                        NotifyParametersChanged();
                    }
                }
            }

            public LayoutParameters(int width, int height) : base(width, height) 
            {
                mAlignParentBottom = false;
                mAlignParentLeft = false;
                mAlignParentRight = false;
                mAlignParentTop = false;
                mCenterHorizontal = false;
                mCenterVertical = false;
            }
        }
    }
}