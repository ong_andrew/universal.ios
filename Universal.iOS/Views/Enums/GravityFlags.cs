﻿using System;

namespace Universal.iOS.Views
{
    /// <summary>
    /// Enumeration for the possible gravity values.
    /// </summary>
    [Flags]
    public enum GravityFlags
    {
        NoGravity = 0,
        CenterHorizontal = 1,
        Left = 2,
        Right = 4,
        CenterVertical = 16,
        Center = 17,
        Top = 32,
        Bottom = 64,
    }
}