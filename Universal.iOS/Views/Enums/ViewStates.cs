﻿namespace Universal.iOS.Views
{
    /// <summary>
    /// Enumerates the visibility states of a view.
    /// </summary>
    public enum ViewStates
    {
        /// <summary>
        /// The view should be visibly presented on the screen.
        /// </summary>
        Visible = 0,
        /// <summary>
        /// The view should not be visible, but should still take up space in layouts.
        /// </summary>
        Invisible = 4,
        /// <summary>
        /// The view should be treated as though it is not part of the layout hierarchy.
        /// </summary>
        Gone = 8
    };
}