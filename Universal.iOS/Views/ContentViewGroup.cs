﻿using CoreGraphics;
using Foundation;
using System;
using System.Linq;
using UIKit;
using Universal.iOS.Extensions;
using Universal.iOS.Views;

namespace Universal.iOS.Views
{
    /// <summary>
    /// Abstract <see cref="ViewGroup" /> that can only contain a single content view.
    /// </summary>
    public abstract class ContentViewGroup : ViewGroup
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ContentViewGroup" /> class.
        /// </summary>
        public ContentViewGroup() : base() { Initialize(); }
        /// <summary>
        /// Initializes a new instance of the <see cref="ContentViewGroup" /> class.
        /// </summary>
        /// <param name="handle"></param>
        public ContentViewGroup(IntPtr handle) : base(handle) { Initialize(); }
        /// <summary>
        /// Initializes a new instance of the <see cref="ContentViewGroup" /> class.
        /// </summary>
        /// <param name="coder"></param>
        public ContentViewGroup(NSCoder coder) : base(coder) { Initialize(); }
        /// <summary>
        /// Initializes a new instance of the <see cref="ContentViewGroup" /> class.
        /// </summary>
        /// <param name="t"></param>
        public ContentViewGroup(NSObjectFlag t) : base(t) { Initialize(); }
        /// <summary>
        /// Initializes a new instance of the <see cref="ContentViewGroup" /> class.
        /// </summary>
        /// <param name="frame"></param>
        public ContentViewGroup(CGRect frame) : base(frame) { Initialize(); }

        private void Initialize()
        {
        }

        /// <summary>
        /// Gets or sets the content view of this <see cref="ContentViewGroup"/>.
        /// </summary>
        public virtual UIView ContentView
        {
            get => Subviews.SingleOrDefault();
            set
            {
                if (ContentView != null)
                {
                    ContentView.RemoveFromSuperview();
                }

                AddSubview(value);
            }
        }

        public override void AddSubview(UIView view)
        {
            if (ContentView != null)
            {
                throw new InvalidOperationException($"A {GetType()} can only have one child.");
            }
            else
            {
                base.AddSubview(view);
            }
        }

        public override CGSize SizeThatFitsSubviews(CGSize size)
        {
            if (ContentView != null)
            {
                LayoutParameters contentViewLayoutParameters = ContentView.GetLayoutParameters();

                nfloat availableWidth = size.Width - contentViewLayoutParameters.MarginLeft - contentViewLayoutParameters.MarginRight;
                nfloat availableHeight = size.Height - contentViewLayoutParameters.MarginTop - contentViewLayoutParameters.MarginBottom;

                if (availableWidth < 0)
                {
                    availableWidth = 0;
                }

                if (availableHeight < 0)
                {
                    availableHeight = 0;
                }

                CGSize availableSize = new CGSize(availableWidth, availableHeight);

                CGSize subviewSizeThatFits = CGSize.Empty;
                if (contentViewLayoutParameters.Visibility != ViewStates.Gone)
                {
                    subviewSizeThatFits = ContentView.SizeThatFits(availableSize);
                    subviewSizeThatFits = new CGSize(subviewSizeThatFits.Width + contentViewLayoutParameters.MarginLeft + contentViewLayoutParameters.MarginRight, subviewSizeThatFits.Height + contentViewLayoutParameters.MarginTop + contentViewLayoutParameters.MarginBottom);
                }

                return subviewSizeThatFits;
            }
            else
            {
                return new CGSize(0, 0);
            }
        }

        public override CGSize IntrinsicContentSize
        {
            get
            {
                if (ContentView != null)
                {
                    LayoutParameters contentViewLayoutParameters = ContentView.GetLayoutParameters();

                    CGSize subviewSizeThatFits = CGSize.Empty;
                    if (contentViewLayoutParameters.Visibility != ViewStates.Gone)
                    {
                        subviewSizeThatFits = ContentView.IntrinsicContentSize;
                        subviewSizeThatFits = new CGSize(subviewSizeThatFits.Width + contentViewLayoutParameters.MarginLeft + contentViewLayoutParameters.MarginRight, subviewSizeThatFits.Height + contentViewLayoutParameters.MarginTop + contentViewLayoutParameters.MarginBottom);
                    }

                    return new CGSize(subviewSizeThatFits.Width + PaddingLeft + PaddingRight, subviewSizeThatFits.Height + PaddingTop + PaddingBottom);
                }
                else
                {
                    return new CGSize(PaddingLeft + PaddingRight, PaddingTop + PaddingBottom);
                }
            }
        }
    }
}