﻿using System;
using System.Runtime.Serialization;

namespace Universal.iOS.Views
{
    /// <summary>
    /// Represents an error due to invalid <see cref="ViewGroup.LayoutParameters"/>.
    /// </summary>
    public class InvalidLayoutParametersException : Exception
    {
        public InvalidLayoutParametersException() : base() { }
        public InvalidLayoutParametersException(string message) : base(message) { }
        public InvalidLayoutParametersException(string message, Exception innerException) : base(message, innerException) { }
        public InvalidLayoutParametersException(SerializationInfo info, StreamingContext context) : base(info, context) { }
    }
}