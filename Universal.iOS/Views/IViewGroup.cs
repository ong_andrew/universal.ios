﻿namespace Universal.iOS.Views
{
    /// <summary>
    /// Represents a view that manages its subview layouts.
    /// </summary>
    public interface IViewGroup
    {
    }
}