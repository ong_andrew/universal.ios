﻿using CoreGraphics;
using Foundation;
using System;
using UIKit;
using Universal.iOS.Extensions;

namespace Universal.iOS.Views
{
    /// <summary>
    /// Abstract base class for views primarily designed to hold other views.
    /// </summary>
    public abstract class ViewGroup : UIView, IViewGroup
    {
        protected nfloat mPaddingBottom;
        /// <summary>
        /// Gets or sets a value for the bottom padding.
        /// </summary>
        public virtual nfloat PaddingBottom 
        { 
            get => mPaddingBottom;
            set 
            {
                if (mPaddingBottom != value)
                {
                    mPaddingBottom = value;
                    SetNeedsLayout();
                }
            } 
        }
        protected nfloat mPaddingLeft;
        /// <summary>
        /// Gets or sets a value for the left padding.
        /// </summary>
        public virtual nfloat PaddingLeft 
        { 
            get => mPaddingLeft;
            set
            {
                if (mPaddingLeft != value)
                {
                    mPaddingLeft = value;
                    SetNeedsLayout();
                }
            } 
        }
        protected nfloat mPaddingRight;
        /// <summary>
        /// Gets or sets a value for the right padding.
        /// </summary>
        public virtual nfloat PaddingRight 
        { 
            get => mPaddingRight;
            set
            {
                if (mPaddingRight != value)
                {
                    mPaddingRight = value;
                    SetNeedsLayout();
                }
            }
        }
        protected nfloat mPaddingTop;
        /// <summary>
        /// Gets or sets a value for the top padding.
        /// </summary>
        public virtual nfloat PaddingTop 
        { 
            get => mPaddingTop;
            set
            {
                if (mPaddingTop != value)
                {
                    mPaddingTop = value;
                    SetNeedsLayout();
                }
            }
        }

        public ViewGroup() { Initialize(); }
        public ViewGroup(NSCoder coder) : base(coder) { Initialize(); }
        public ViewGroup(IntPtr ptr) : base(ptr) { Initialize(); }
        public ViewGroup(CGRect frame) : base(frame) { Initialize(); }
        public ViewGroup(NSObjectFlag t) : base(t) { Initialize(); }

        private void Initialize() 
        {
            ClipsToBounds = true;
        }

        public override void AddSubview(UIView view)
        {
            base.AddSubview(view);
            if (view.GetLayoutParameters() != null)
            {
                view.GetLayoutParameters().LayoutParametersChanged += OnLayoutParametersChanged;
            }
        }

        public override void WillRemoveSubview(UIView uiview)
        {
            base.WillRemoveSubview(uiview);
            if (uiview.GetLayoutParameters() != null)
            {
                uiview.GetLayoutParameters().LayoutParametersChanged -= OnLayoutParametersChanged;
            }
            SetNeedsLayout();
        }

        /// <summary>
        /// Notifies this <see cref="ViewGroup"/> that a subview's layout parameters has changed.
        /// </summary>
        /// <param name="uiView"></param>
        /// <param name="layoutParametersOld"></param>
        /// <param name="layoutParametersNew"></param>
        public void NotifySubviewLayoutParametersChanged(UIView uiView, LayoutParameters layoutParametersOld, LayoutParameters layoutParametersNew)
        {
            if (layoutParametersOld != null)
            {
                layoutParametersOld.LayoutParametersChanged -= OnLayoutParametersChanged;
            }

            if (layoutParametersNew != null)
            {
                layoutParametersNew.LayoutParametersChanged += OnLayoutParametersChanged;
            }
        }

        protected virtual void OnLayoutParametersChanged(object sender, EventArgs eventArgs)
        {
            SetNeedsLayout();
        }

        /// <summary>
        /// Sets the padding of this <see cref="ViewGroup"/> from iOS' <see cref="UIView.LayoutMargins"/> property.
        /// </summary>
        public virtual void SetPaddingFromLayoutMargins()
        {
            PaddingLeft = LayoutMargins.Left;
            PaddingRight = LayoutMargins.Right;
            PaddingTop = LayoutMargins.Top;
            PaddingBottom = LayoutMargins.Bottom;
        }

        /// <summary>
        /// Sets the padding accordingly.
        /// </summary>
        /// <param name="left"></param>
        /// <param name="top"></param>
        /// <param name="right"></param>
        /// <param name="bottom"></param>
        public virtual void SetPadding(nfloat left, nfloat top, nfloat right, nfloat bottom)
        {
            PaddingLeft = left;
            PaddingTop = top;
            PaddingRight = right;
            PaddingBottom = bottom;
        }

        /// <summary>
        /// Measures the size of the given <see cref="UIView" /> according to its assigned <see cref="LayoutParameters" />.
        /// </summary>
        /// <param name="parentSize"></param>
        /// <param name="view"></param>
        /// <returns></returns>
        public virtual CGSize MeasureViewSize(CGSize parentSize, UIView view)
        {
            return MeasureViewSize(parentSize, view, view.GetLayoutParameters());
        }

        /// <summary>
        /// Measures the size of the given <see cref="UIView" /> according to the provided <see cref="LayoutParameters" />.
        /// </summary>
        /// <param name="parentSize"></param>
        /// <param name="view"></param>
        /// <param name="layoutParameters"></param>
        /// <returns></returns>
        public virtual CGSize MeasureViewSize(CGSize parentSize, UIView view, LayoutParameters layoutParameters)
        {
            if (layoutParameters == null)
            {
                throw new InvalidOperationException("The view must have layout parameters assigned.");
            }

            if (layoutParameters.Visibility == ViewStates.Gone)
            {
                return new CGSize(0, 0);
            }

            nfloat width = 0;
            nfloat height = 0;

            if (layoutParameters.Width >= 0)
            {
                width = layoutParameters.Width;
            }
            else if (layoutParameters.Width == ViewGroup.LayoutParameters.WrapContent)
            {
                CGSize maxSize = parentSize;

                nfloat adjustedWidth = maxSize.Width - layoutParameters.MarginLeft - layoutParameters.MarginRight - PaddingLeft - PaddingRight;
                nfloat adjustedHeight = maxSize.Height - layoutParameters.MarginTop - layoutParameters.MarginBottom - PaddingTop - PaddingBottom;
                maxSize = new CGSize(adjustedWidth >= 0 ? adjustedWidth : 0, adjustedHeight >= 0 ? adjustedHeight : 0);

                width = view.SizeThatFits(maxSize).Width;
            }
            else if (layoutParameters.Width == ViewGroup.LayoutParameters.MatchParent)
            {
                CGSize maxSize = parentSize;

                nfloat adjustedWidth = maxSize.Width - layoutParameters.MarginLeft - layoutParameters.MarginRight - PaddingLeft - PaddingRight;
                nfloat adjustedHeight = maxSize.Height - layoutParameters.MarginTop - layoutParameters.MarginBottom - PaddingTop - PaddingBottom;
                maxSize = new CGSize(adjustedWidth >= 0 ? adjustedWidth : 0, adjustedHeight >= 0 ? adjustedHeight : 0);

                width = maxSize.Width;
            }

            if (layoutParameters.Height >= 0)
            {
                height = layoutParameters.Height;
            }
            else if (layoutParameters.Height == ViewGroup.LayoutParameters.WrapContent)
            {
                CGSize maxSize = parentSize;

                nfloat adjustedWidth = maxSize.Width - layoutParameters.MarginLeft - layoutParameters.MarginRight - PaddingLeft - PaddingRight;
                nfloat adjustedHeight = maxSize.Height - layoutParameters.MarginTop - layoutParameters.MarginBottom - PaddingTop - PaddingBottom;
                maxSize = new CGSize(adjustedWidth >= 0 ? adjustedWidth : 0, adjustedHeight >= 0 ? adjustedHeight : 0);

                height = view.SizeThatFits(maxSize).Height;
            }
            else if (layoutParameters.Height == ViewGroup.LayoutParameters.MatchParent)
            {
                CGSize maxSize = parentSize;

                nfloat adjustedWidth = maxSize.Width - layoutParameters.MarginLeft - layoutParameters.MarginRight - PaddingLeft - PaddingRight;
                nfloat adjustedHeight = maxSize.Height - layoutParameters.MarginTop - layoutParameters.MarginBottom - PaddingTop - PaddingBottom;
                maxSize = new CGSize(adjustedWidth >= 0 ? adjustedWidth : 0, adjustedHeight >= 0 ? adjustedHeight : 0);

                height = maxSize.Height;
            }

            return new CGSize(width, height);
        }

        /// <summary>
        /// Returns the base size that fits this <see cref="ViewGroup" />'s subviews, ignoring padding and the <see cref="ViewGroup"/>'s own layout parameters.
        /// </summary>
        /// <param name="size">The size of the parent available for content after padding has been taken into account.</param>
        /// <returns></returns>
        public abstract CGSize SizeThatFitsSubviews(CGSize size);

        public override CGSize SizeThatFits(CGSize size)
        {
            CGSize availableSize = new CGSize(size.Width - PaddingLeft - PaddingRight, size.Height - PaddingLeft - PaddingRight);
            if (availableSize.Width < 0)
            {
                availableSize = new CGSize(0, availableSize.Height);
            }
            if (availableSize.Height < 0)
            {
                availableSize = new CGSize(availableSize.Width, 0);
            }
            CGSize sizeThatFitsSubviews = SizeThatFitsSubviews(availableSize);
            CGSize sizeThatFitsSubviewsWithPadding = new CGSize(sizeThatFitsSubviews.Width + PaddingLeft + PaddingRight, sizeThatFitsSubviews.Height + PaddingTop + PaddingBottom);

            nfloat width = sizeThatFitsSubviewsWithPadding.Width;
            nfloat height = sizeThatFitsSubviewsWithPadding.Height;

            ViewGroup.LayoutParameters layoutParameters = this.GetLayoutParameters();
            if (layoutParameters != null)
            {
                if (layoutParameters.Width == LayoutParameters.MatchParent)
                {
                    if (size.Width > width)
                    {
                        width = size.Width;
                    }
                }
                else if (layoutParameters.Width >= 0)
                {
                    width = layoutParameters.Width;
                }

                if (layoutParameters.Height == LayoutParameters.MatchParent)
                {
                    if (size.Height > height)
                    {
                        height = size.Height;
                    }
                }
                else if (layoutParameters.Height >= 0)
                {
                    height = layoutParameters.Height;
                }
            }

            return new CGSize(width, height);
        }

        public override void SetNeedsLayout()
        {
            base.SetNeedsLayout();
            if (Superview is IViewGroup)
            {
                Superview.SetNeedsLayout();
            }
        }

        /// <summary>
        /// General layout parameters for <see cref="ViewGroup"/>s.
        /// </summary>
        public class LayoutParameters
        {
            /// <summary>
            /// Constant indicating that the <see cref="UIView"/> should match the parent's dimension.
            /// </summary>
            public const int MatchParent = -1;
            /// <summary>
            /// Constant indicating that the <see cref="UIView"/> should take up as much space to fit its contents.
            /// </summary>
            public const int WrapContent = -2;

            protected int mWidth;
            /// <summary>
            /// Gets or sets a value indicating the width a <see cref="UIView"/> should take up.
            /// </summary>
            public virtual int Width
            {
                get => mWidth;
                set
                {
                    if (mWidth != value)
                    {
                        mWidth = value;
                        NotifyParametersChanged();
                    }
                }
            }
            protected int mHeight;
            /// <summary>
            /// Gets or sets a value indicating the height a <see cref="UIView"/> should take up.
            /// </summary>
            public virtual int Height
            {
                get => mHeight;
                set
                {
                    if (mHeight != value)
                    {
                        mHeight = value;
                        NotifyParametersChanged();
                    }
                }
            }

            protected nfloat mMarginBottom;
            /// <summary>
            /// Gets or sets a value for the margin of the view.
            /// </summary>
            public virtual nfloat MarginBottom
            {
                get => mMarginBottom;
                set
                {
                    if (mMarginBottom != value)
                    {
                        mMarginBottom = value;
                        NotifyParametersChanged();
                    }
                }
            }
            protected nfloat mMarginTop;
            /// <summary>
            /// Gets or sets a value for the margin of the view.
            /// </summary>
            public virtual nfloat MarginTop
            {
                get => mMarginTop;
                set
                {
                    if (mMarginTop != value)
                    {
                        mMarginTop = value;
                        NotifyParametersChanged();
                    }
                }
            }
            protected nfloat mMarginLeft;
            /// <summary>
            /// Gets or sets a value for the margin of the view.
            /// </summary>
            public virtual nfloat MarginLeft
            {
                get => mMarginLeft;
                set
                {
                    if (mMarginLeft != value)
                    {
                        mMarginLeft = value;
                        NotifyParametersChanged();
                    }
                }
            }
            protected nfloat mMarginRight;
            /// <summary>
            /// Gets or sets a value for the margin of the view.
            /// </summary>
            public virtual nfloat MarginRight
            {
                get => mMarginRight;
                set
                {
                    if (mMarginRight != value)
                    {
                        mMarginRight = value;
                        NotifyParametersChanged();
                    }
                }
            }
            protected ViewStates mVisibility;
            /// <summary>
            /// Gets or sets a value for the visibility of the view.
            /// </summary>
            public virtual ViewStates Visibility
            {
                get => mVisibility;
                set
                {
                    if (mVisibility != value)
                    {
                        mVisibility = value;
                        NotifyParametersChanged();
                    }
                }
            }

            public override string ToString()
            {
                string width = Width == WrapContent ? "WrapContent" : Width == MatchParent ? "MatchParent" : Width.ToString();
                string height = Height == WrapContent ? "WrapContent" : Height == MatchParent ? "MatchParent" : Height.ToString();

                return $"[{GetType().Name}] {{Width={width}, Height={height}}}";
            }

            /// <summary>
            /// Event raised when layout parameter properties are changed.
            /// </summary>
            public event EventHandler<EventArgs> LayoutParametersChanged;

            /// <summary>
            /// Initializes a new instance of the <see cref="LayoutParameters"/> class with the given width and height.
            /// </summary>
            /// <param name="width"></param>
            /// <param name="height"></param>
            public LayoutParameters(int width, int height)
            {
                mWidth = width;
                mHeight = height;
            }
            /// <summary>
            /// Initializes a new instance of the <see cref="LayoutParameters"/> class using another <see cref="LayoutParameters"/> instance.
            /// </summary>
            /// <param name="other"></param>
            public LayoutParameters(LayoutParameters other) : this(other.Width, other.Height)
            {
                mMarginBottom = other.MarginBottom;
                mMarginLeft = other.MarginLeft;
                mMarginRight = other.MarginRight;
                mMarginTop = other.MarginTop;
                mVisibility = other.Visibility;
            }

            /// <summary>
            /// Notifies this <see cref="LayoutParameters"/> that its parameters have changed.
            /// </summary>
            public virtual void NotifyParametersChanged()
            {
                LayoutParametersChanged?.Invoke(this, new EventArgs());
            }

            /// <summary>
            /// Sets the margins.
            /// </summary>
            /// <param name="left"></param>
            /// <param name="top"></param>
            /// <param name="right"></param>
            /// <param name="bottom"></param>
            public virtual void SetMargins(nfloat left, nfloat top, nfloat right, nfloat bottom)
            {
                MarginLeft = left;
                MarginTop = top;
                MarginRight = right;
                MarginBottom = bottom;
            }
        }
    }
}