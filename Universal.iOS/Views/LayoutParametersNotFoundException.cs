﻿using System;
using System.Runtime.Serialization;

namespace Universal.iOS.Views
{
    /// <summary>
    /// Error thrown when a <see cref="ViewGroup.LayoutParameters"/> is not set for a <see cref="UIKit.UIView"/>.
    /// </summary>
    public class LayoutParametersNotFoundException : Exception
    {
        public LayoutParametersNotFoundException() : base() { }
        public LayoutParametersNotFoundException(string message) : base(message) { }
        public LayoutParametersNotFoundException(string message, Exception innerException) : base(message, innerException) { }
        public LayoutParametersNotFoundException(SerializationInfo info, StreamingContext context) : base(info, context) { }
    }
}