﻿using UIKit;

namespace Universal.iOS
{
    public static class UIColors
    {
        public static UIColor DesignerBlue
        {
            get
            {
                return UIColor.FromRGB(10, 96, 255);
            }
        }

        public static UIColor DefaultButtonText
        {
            get
            {
                return UIColor.FromRGB(0, 122, 255);
            }
        }
    }
}