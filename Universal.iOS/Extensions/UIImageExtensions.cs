﻿using System;
using CoreGraphics;
using UIKit;

namespace Universal.iOS.Extensions
{
    public static class UIImageExtensions
    {
        /// <summary>
        /// Creates a new <see cref="UIImage"/> based on the current one, with the specified <paramref name="tintColor"/>.
        /// </summary>
        /// <param name="UIImage"></param>
        /// <param name="tintColor"></param>
        /// <returns></returns>
        public static UIImage ImageWithTint(this UIImage UIImage, UIColor tintColor)
        {
            if (UIImage == null)
            {
                throw new ArgumentNullException(nameof(UIImage));
            }

            UIImage result = new UIImage(UIImage.CGImage).ImageWithRenderingMode(UIImageRenderingMode.AlwaysTemplate);
            UIGraphics.BeginImageContextWithOptions(UIImage.Size, false, UIImage.CurrentScale);

            tintColor.SetColor();
            UIImage.Draw(new CGRect(0, 0, UIImage.Size.Width, UIImage.Size.Height));

            result = UIGraphics.GetImageFromCurrentImageContext();
            UIGraphics.EndImageContext();

            return result;
        }
    }
}