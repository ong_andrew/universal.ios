﻿using ObjCRuntime;
using System.Linq;
using UIKit;

namespace Universal.iOS.Extensions
{
    /// <summary>
    /// Extensions for the <see cref="UIApplication"/> class.
    /// </summary>
    public static class UIApplicationExtensions
    {
        /// <summary>
        /// Gets the window for the keyboard, if any. Otherwise returns null.
        /// </summary>
        /// <param name="application"></param>
        /// <returns></returns>
        public static UIWindow GetRemoteKeyboardWindow(this UIApplication application)
        {
            return application.Windows.FirstOrDefault(x => x.IsKindOfClass(new Class(Class.GetHandle("UIRemoteKeyboardWindow"))));
        }
    }
}