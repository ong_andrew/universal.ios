﻿using CoreGraphics;
using Foundation;
using System;
using System.Collections.Generic;
using System.Linq;
using UIKit;
using Universal.iOS.ViewControllers;
using Universal.iOS.Widget;

namespace Universal.iOS.Extensions
{
    /// <summary>
    /// Extensions for the <see cref="UIViewController"/> class.
    /// </summary>
    public static class UIViewControllerExtensions
    {
        private static nfloat? mFrameHeight = null;
        private static Dictionary<UIViewController, List<NSObject>> mAddedObservers = new Dictionary<UIViewController, List<NSObject>>();
        private static Dictionary<UIViewController, List<object>> mActivityLoadingOverlayRequests = new Dictionary<UIViewController, List<object>>();

        /// <summary>
        /// Dismisses the current presented view controller if it is an <see cref="ActivityIndicatorViewController" />.
        /// </summary>
        /// <param name="UIViewController"></param>
        public static void DismissLoadingOverlay(this UIViewController UIViewController)
        {
            if (UIViewController.IsPresentingLoadingOverlay())
            {
                UIViewController.DismissViewController(false, null);
            }
        }

        /// <summary>
        /// Returns 
        /// </summary>
        /// <param name="UIViewController"></param>
        /// <returns></returns>
        public static bool IsPresentingLoadingOverlay(this UIViewController UIViewController)
        {
            return UIViewController.PresentedViewController != null && UIViewController.PresentedViewController is ActivityIndicatorViewController;
        }

        /// <summary>
        /// Presents an alert with a title and message to the user.
        /// </summary>
        /// <param name="UIViewController"></param>
        /// <param name="title"></param>
        /// <param name="message"></param>
        public static void PresentAlert(this UIViewController UIViewController, string title, string message)
        {
            UIAlertController alertController = UIAlertController.Create(title, message, UIAlertControllerStyle.Alert);
            alertController.AddAction(UIAlertAction.Create("OK", UIAlertActionStyle.Default, (alertAction) => { }));
            UIViewController.PresentViewController(alertController, true, () => { });
        }

        /// <summary>
        /// Modally presents an <see cref="ActivityIndicatorViewController" />.
        /// </summary>
        /// <param name="UIViewController"></param>
        public static void PresentLoadingOverlay(this UIViewController UIViewController)
        {
            UIViewController.PresentViewController(new ActivityIndicatorViewController() { ModalPresentationStyle = UIModalPresentationStyle.OverFullScreen, ModalTransitionStyle = UIModalTransitionStyle.CrossDissolve }, true, null);
        }

        /// <summary>
        /// Withdraws a request previouwly made with <see cref="RequestPresentLoadingOverlay(UIViewController, object)" />, hiding the loading overlay if no requestors remain.
        /// </summary>
        /// <param name="UIViewController"></param>
        /// <param name="key"></param>
        public static void RequestDismissLoadingOverlay(this UIViewController UIViewController, object key)
        {
            if (!mActivityLoadingOverlayRequests.ContainsKey(UIViewController))
            {
                mActivityLoadingOverlayRequests.Add(UIViewController, new List<object>());
            }

            mActivityLoadingOverlayRequests[UIViewController].Remove(key);

            if (!mActivityLoadingOverlayRequests[UIViewController].Any())
            {
                if (UIViewController.IsPresentingLoadingOverlay())
                {
                    UIViewController.DismissLoadingOverlay();
                }
            }
        }

        /// <summary>
        ///  Requests the loading overlay be shown until the <see cref="RequestDismissLoadingOverlay(UIViewController, object)" /> is called.
        /// </summary>
        /// <param name="UIViewController"></param>
        /// <param name="key"></param>
        public static void RequestPresentLoadingOverlay(this UIViewController UIViewController, object key)
        {
            if (!mActivityLoadingOverlayRequests.ContainsKey(UIViewController))
            {
                mActivityLoadingOverlayRequests.Add(UIViewController, new List<object>());
            }

            mActivityLoadingOverlayRequests[UIViewController].Add(key);

            if (!UIViewController.IsPresentingLoadingOverlay())
            {
                UIViewController.PresentLoadingOverlay();
            }
        }

        /// <summary>
        /// Sets the <see cref="KeyboardMode"/> for the given <see cref="UIViewController"/>.
        /// </summary>
        /// <param name="UIViewController"></param>
        /// <param name="keyboardMode"></param>
        public static void SetWindowKeyboardMode(this UIViewController UIViewController, KeyboardMode keyboardMode)
        {
            UIViewController.View.EndEditing(true);
            RemoveManagedObservers(UIViewController);

            if (keyboardMode == KeyboardMode.AdjustNone)
            {
            }
            else if (keyboardMode == KeyboardMode.AdjustResize)
            {
                AddResizeOnKeyboardObservers(UIViewController);
            }
            else if (keyboardMode == KeyboardMode.AdjustPan)
            {
                AddPanOnKeyboardObservers(UIViewController);
            }
            else if (keyboardMode == KeyboardMode.AdjustFirstResponder)
            {
                AddFirstResponderKeyboardObservers(UIViewController);
            }
        }

        private static void AddPanOnKeyboardObservers(UIViewController uiViewController)
        {
            AddManagedObserver(uiViewController, NSNotificationCenter.DefaultCenter.AddObserver(UIKeyboard.DidShowNotification, (notification) =>
            {
                UIView firstResponder = uiViewController.View.Window?.FindFirstResponder();

                if (firstResponder != null)
                {
                    nfloat heightToBottom = firstResponder.Superview.ConvertRectToView(firstResponder.Frame, null).Bottom;
                    nfloat frameHeight = uiViewController.View.Window.Frame.Height;
                    //Console.WriteLine($"height to bottom: {heightToBottom}");
                    if (firstResponder.Ancestors().Any(x => typeof(UIScrollView).IsAssignableFrom(x.GetType())))
                    {
                        var parentScrollView = ((UIScrollView)firstResponder.Ancestors().First(x => typeof(UIScrollView).IsAssignableFrom(x.GetType())));

                        //Console.WriteLine(parentScrollView.ContentSize);
                        if (heightToBottom > frameHeight)
                        {
                            var scrollAdjustment = heightToBottom - frameHeight;
                            
                            heightToBottom -= scrollAdjustment;
                            
                            parentScrollView.ContentOffset = new CGPoint(parentScrollView.ContentOffset.X, parentScrollView.ContentOffset.Y - scrollAdjustment);
                        }
                        //old code in here for some reason:
                        //heightToBottom -= UIApplication.SharedApplication.StatusBarFrame.Height;
                        //Console.WriteLine(UIApplication.SharedApplication.StatusBarFrame.Height);
                    }
                    
                    //Console.WriteLine($"frame height: {frameHeight}");
                    if (uiViewController.NavigationController != null && uiViewController.NavigationController.NavigationBar != null && !uiViewController.NavigationController.NavigationBarHidden)
                    {
                        frameHeight -= uiViewController.NavigationController.NavigationBar.Frame.Height;
                        //Console.WriteLine($"navbar height: {uiViewController.NavigationController.NavigationBar.Frame.Height}");
                    }
                    nfloat keyboardHeight = UIKeyboard.BoundsFromNotification(notification).Height;
                    //Console.WriteLine($"keyboard height: {keyboardHeight}");
                    nfloat remainingHeight = frameHeight - keyboardHeight;
                    nfloat excessHeight = remainingHeight - heightToBottom;

                    if (excessHeight < 0)
                    {
                        CGRect newFrame = uiViewController.View.Frame;
                        newFrame.Y = excessHeight;
                        uiViewController.View.Frame = newFrame;
                    }
                }

            }));

            AddManagedObserver(uiViewController, NSNotificationCenter.DefaultCenter.AddObserver(UIKeyboard.DidHideNotification, (notification) =>
            {
                CGRect frame = uiViewController.View.Frame;
                frame.Y = 0;
                uiViewController.View.Frame = frame;
            }));
        }

        private static void AddResizeOnKeyboardObservers(UIViewController uiViewController)
        {
            /*
            void AdjustViewControllerSize(NSNotification notification)
            {
                NSValue frameEndUserInfo = (NSValue)notification.UserInfo.ValueForKey(UIKeyboard.FrameEndUserInfoKey);

                nfloat availableHeight = frameEndUserInfo.CGRectValue.Y;

                uiViewController.View.Frame = new CGRect(uiViewController.View.Frame.X,
                    uiViewController.View.Frame.Y,
                    uiViewController.View.Frame.Width,
                    availableHeight);
            }

            AddManagedObserver(uiViewController, NSNotificationCenter.DefaultCenter.AddObserver(UIKeyboard.DidShowNotification, (notification) =>
            {
                AdjustViewControllerSize(notification);
                
            }));

            AddManagedObserver(uiViewController, NSNotificationCenter.DefaultCenter.AddObserver(UIKeyboard.DidHideNotification, (notification) =>
            {
                AdjustViewControllerSize(notification);
            }));
            */
            AddManagedObserver(uiViewController, NSNotificationCenter.DefaultCenter.AddObserver(UIKeyboard.DidShowNotification, (notification) =>
            {
                NSValue keyboardFrameBeginUserInfo = (NSValue)notification.UserInfo.ValueForKey(UIKeyboard.FrameBeginUserInfoKey);
                NSValue keyboardFrameEndUserInfo = (NSValue)notification.UserInfo.ValueForKey(UIKeyboard.FrameEndUserInfoKey);
                float keyboardHeightChange = keyboardFrameEndUserInfo.RectangleFValue.Bottom - keyboardFrameBeginUserInfo.RectangleFValue.Bottom;
                if (keyboardHeightChange != 0)
                {
                    uiViewController.View.Frame = new CGRect(uiViewController.View.Frame.X,
                    uiViewController.View.Frame.Y,
                    uiViewController.View.Frame.Width,
                    uiViewController.View.Frame.Height - (keyboardFrameEndUserInfo.RectangleFValue.Bottom - keyboardFrameEndUserInfo.RectangleFValue.Top));
                }
            }));
            AddManagedObserver(uiViewController, NSNotificationCenter.DefaultCenter.AddObserver(UIKeyboard.DidHideNotification, (notification) =>
            {
                NSValue keyboardFrameBeginUserInfo = (NSValue)notification.UserInfo.ValueForKey(UIKeyboard.FrameBeginUserInfoKey);
                NSValue keyboardFrameEndUserInfo = (NSValue)notification.UserInfo.ValueForKey(UIKeyboard.FrameEndUserInfoKey);
                float keyboardHeightChange = keyboardFrameEndUserInfo.RectangleFValue.Bottom - keyboardFrameBeginUserInfo.RectangleFValue.Bottom;
                if (keyboardHeightChange != 0)
                {
                    uiViewController.View.Frame = new CGRect(uiViewController.View.Frame.X,
                    uiViewController.View.Frame.Y,
                    uiViewController.View.Frame.Width,
                    uiViewController.View.Frame.Height + (keyboardFrameEndUserInfo.RectangleFValue.Bottom - keyboardFrameEndUserInfo.RectangleFValue.Top));
                }
            }));
        }

        private static void AddFirstResponderKeyboardObservers(UIViewController uiViewController)
        {
            AddManagedObserver(uiViewController, NSNotificationCenter.DefaultCenter.AddObserver(UIKeyboard.DidShowNotification, (notification) =>
            {
                UIView firstResponder = uiViewController.View.Window?.FindFirstResponder();

                if (firstResponder != null)
                {
                    if (firstResponder.Superview.Constraints.Any(x => (x.FirstItem == firstResponder && x.FirstAttribute == NSLayoutAttribute.Bottom) || (x.SecondItem == firstResponder && x.SecondAttribute == NSLayoutAttribute.Bottom)))
                    {
                        if (mFrameHeight == null)
                        {
                            mFrameHeight = uiViewController.View.Frame.Height;
                        }

                        uiViewController.View.Frame = new CGRect(uiViewController.View.Frame.X, uiViewController.View.Frame.Y, uiViewController.View.Frame.Width, uiViewController.View.Frame.Height - UIKeyboard.BoundsFromNotification(notification).Height);
                    }
                    else
                    {
                        nfloat heightToBottom = firstResponder.Superview.ConvertRectToView(firstResponder.Frame, null).Bottom;
                        if (firstResponder.Ancestors().Any(x => typeof(UIScrollView).IsAssignableFrom(x.GetType())))
                        {
                            heightToBottom -= UIApplication.SharedApplication.StatusBarFrame.Height;
                        }
                        nfloat frameHeight = uiViewController.View.Window.Frame.Height;
                        if (uiViewController.NavigationController != null && uiViewController.NavigationController.NavigationBar != null && !uiViewController.NavigationController.NavigationBarHidden)
                        {
                            frameHeight -= uiViewController.NavigationController.NavigationBar.Frame.Height;
                        }
                        nfloat keyboardHeight = UIKeyboard.BoundsFromNotification(notification).Height;

                        nfloat remainingHeight = frameHeight - keyboardHeight;
                        nfloat excessHeight = remainingHeight - heightToBottom;

                        if (excessHeight < 0)
                        {
                            CGRect newFrame = uiViewController.View.Frame;
                            newFrame.Y = excessHeight;
                            uiViewController.View.Frame = newFrame;
                        }
                    }
                }
            }));

            AddManagedObserver(uiViewController, NSNotificationCenter.DefaultCenter.AddObserver(UIKeyboard.DidHideNotification, (notification) =>
            {
                CGRect frame = uiViewController.View.Frame;
                frame.Y = 0;
                uiViewController.View.Frame = frame;

                if (mFrameHeight != null)
                {
                    uiViewController.View.Frame = new CGRect(uiViewController.View.Frame.X, uiViewController.View.Frame.Y, uiViewController.View.Frame.Width, mFrameHeight.Value);
                    mFrameHeight = null;
                }
            }));
        }

        private static void RemoveManagedObservers(UIViewController uiViewController)
        {
            if (mAddedObservers.ContainsKey(uiViewController))
            {
                NSNotificationCenter.DefaultCenter.RemoveObservers(mAddedObservers[uiViewController]);
                mAddedObservers[uiViewController].Clear();
            }
        }

        private static void AddManagedObserver(UIViewController uiViewController, NSObject observer)
        {
            if (!mAddedObservers.ContainsKey(uiViewController))
            {
                mAddedObservers.Add(uiViewController, new List<NSObject>());
            }

            mAddedObservers[uiViewController].Add(observer);
        }
    }
}