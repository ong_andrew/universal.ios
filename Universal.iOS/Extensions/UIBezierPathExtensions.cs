﻿using CoreGraphics;
using System.Linq;
using UIKit;

namespace Universal.iOS.Extensions
{
    /// <summary>
    /// Extensions for the <see cref="UIBezierPath" /> class.
    /// </summary>
    public static class UIBezierPathExtensions
    {
        /// <summary>
        /// Appends a cubic Bezier curve to this UIBezierPath.
        /// </summary>
        /// <param name="UIBezierPath"></param>
        /// <param name="endPoint"></param>
        /// <param name="controlPoint1"></param>
        /// <param name="controlPoint2"></param>
        public static void AddCurveToPointRelative(this UIBezierPath UIBezierPath, CGPoint endPoint, CGPoint controlPoint1, CGPoint controlPoint2)
        {
            UIBezierPath.AddCurveToPoint(UIBezierPath.CurrentPoint.Add(endPoint), UIBezierPath.CurrentPoint.Add(controlPoint1), UIBezierPath.CurrentPoint.Add(controlPoint2));
        }

        /// <summary>
        /// Appends a horizontal straight line to this path to the specified x-coordinate.
        /// </summary>
        /// <param name="UIBezierPath"></param>
        /// <param name="x"></param>
        public static void AddHorizontalLineTo(this UIBezierPath UIBezierPath, double x)
        {
            UIBezierPath.AddLineTo(new CGPoint(x, UIBezierPath.CurrentPoint.Y));
        }

        /// <summary>
        /// Appends a horizontal straight line to this path to the specified relative x-coordinate.
        /// </summary>
        /// <param name="UIBezierPath"></param>
        /// <param name="x"></param>
        public static void AddHorizontalLineToRelative(this UIBezierPath UIBezierPath, double x)
        {
            UIBezierPath.AddLineTo(new CGPoint(UIBezierPath.CurrentPoint.X + x, UIBezierPath.CurrentPoint.Y));
        }

        /// <summary>
        /// Appends a straight line to this path to the specified location.
        /// </summary>
        /// <param name="UIBezierPath"></param>
        /// <param name="point"></param>
        public static void AddLineToRelative(this UIBezierPath UIBezierPath, CGPoint point)
        {
            UIBezierPath.AddLineTo(UIBezierPath.CurrentPoint.Add(point));
        }

        /// <summary>
        /// Appends a quadratic Bezier curve to this UIBezierPath.
        /// </summary>
        /// <param name="UIBezierPath"></param>
        /// <param name="endPoint"></param>
        /// <param name="controlPoint"></param>
        public static void AddQuadCurveToPointRelative(this UIBezierPath UIBezierPath, CGPoint endPoint, CGPoint controlPoint)
        {
            UIBezierPath.AddQuadCurveToPoint(UIBezierPath.CurrentPoint.Add(endPoint), UIBezierPath.CurrentPoint.Add(controlPoint));
        }

        /// <summary>
        /// Adds a smooth cubic Bezier curve to the path, assuming the first control point is the reflection of the previous cubic Bezier curve's second control point reflected about the current point, or the current point if the previous operation was not a cubic Bezier curve addition.
        /// </summary>
        /// <param name="UIBezierPath"></param>
        /// <param name="endPoint"></param>
        /// <param name="controlPoint2"></param>
        public static void AddSmoothCurveToPoint(this UIBezierPath UIBezierPath, CGPoint endPoint, CGPoint controlPoint2)
        {
            CGPath cgPath = UIBezierPath.CGPath;
            CGPoint controlPoint1 = UIBezierPath.CurrentPoint;

            if (cgPath.EnumerateElements().Any())
            {
                CGPathElement lastPathElement = cgPath.EnumerateElements().Last();
                if (lastPathElement.Type == CGPathElementType.AddCurveToPoint)
                {
                    controlPoint1 = lastPathElement.Point2.Reflect(UIBezierPath.CurrentPoint);
                }
            }

            UIBezierPath.AddCurveToPoint(endPoint, controlPoint1, controlPoint2);
        }

        /// <summary>
        /// Adds a smooth cubic Bezier curve to the path, assuming the first control point is the reflection of the previous cubic Bezier curve's second control point reflected about the current point, or the current point if the previous operation was not a cubic Bezier curve addition.
        /// </summary>
        /// <param name="UIBezierPath"></param>
        /// <param name="endPoint"></param>
        /// <param name="controlPoint2"></param>
        public static void AddSmoothCurveToPointRelative(this UIBezierPath UIBezierPath, CGPoint endPoint, CGPoint controlPoint2)
        {
            UIBezierPath.AddSmoothCurveToPoint(UIBezierPath.CurrentPoint.Add(endPoint), UIBezierPath.CurrentPoint.Add(controlPoint2));
        }

        /// <summary>
        /// Appends a vertical straight line to this path to the specified y-coordinate.
        /// </summary>
        /// <param name="UIBezierPath"></param>
        /// <param name="y"></param>
        public static void AddVerticalLineTo(this UIBezierPath UIBezierPath, double y)
        {
            UIBezierPath.AddLineTo(new CGPoint(UIBezierPath.CurrentPoint.X, y));
        }

        /// <summary>
        /// Appends a vertical straight line to this path to the specified relative y-coordinate.
        /// </summary>
        /// <param name="UIBezierPath"></param>
        /// <param name="y"></param>
        public static void AddVerticalLineToRelative(this UIBezierPath UIBezierPath, double y)
        {
            UIBezierPath.AddLineTo(new CGPoint(UIBezierPath.CurrentPoint.X, UIBezierPath.CurrentPoint.Y + y));
        }

        /// <summary>
        /// Moves the CurrentPoint to the relative location specified.
        /// </summary>
        /// <param name="UIBezierPath"></param>
        /// <param name="point"></param>
        public static void MoveToRelative(this UIBezierPath UIBezierPath, CGPoint point)
        {
            UIBezierPath.MoveTo(UIBezierPath.CurrentPoint.Add(point));
        }
    }
}