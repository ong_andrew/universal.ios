﻿using CoreGraphics;
using Foundation;
using System;
using System.Collections.Generic;
using UIKit;

namespace Universal.iOS.Extensions
{
    /// <summary>
    /// Extensions for the <see cref="UIWindow"/> class.
    /// </summary>
    public static class UIWindowExtensions
    {
        private static Dictionary<UIWindow, List<NSObject>> mAddedObservers = new Dictionary<UIWindow, List<NSObject>>();

        /// <summary>
        /// Sets the <see cref="KeyboardMode"/> for the given <see cref="UIWindow"/>.
        /// </summary>
        /// <param name="uiWindow"></param>
        /// <param name="keyboardMode"></param>
        public static void SetWindowKeyboardMode(this UIWindow uiWindow, KeyboardMode keyboardMode)
        {
            uiWindow.EndEditing(true);
            RemoveManagedObservers(uiWindow);

            if (keyboardMode == KeyboardMode.AdjustNone)
            {
            }
            else if (keyboardMode == KeyboardMode.AdjustResize)
            {
                AddResizeOnKeyboardObservers(uiWindow);
            }
            else if (keyboardMode == KeyboardMode.AdjustPan)
            {
                throw new NotImplementedException();
            }
            else if (keyboardMode == KeyboardMode.AdjustFirstResponder)
            {
                throw new NotImplementedException();
            }
        }

        /// <summary>
        /// Finds the first responder for the given <see cref="UIWindow"/>.
        /// </summary>
        /// <param name="UIWindow"></param>
        /// <returns></returns>
        public static UIView FindFirstResponder(this UIWindow UIWindow)
        {
            if (UIWindow.IsFirstResponder)
            {
                return UIWindow;
            }
            else
            {
                foreach (UIView uiView in UIWindow.Subviews)
                {
                    UIView firstResponderSubview = uiView.FindFirstResponder();

                    if (firstResponderSubview != null)
                    {
                        return firstResponderSubview;
                    }
                }

                return null;
            }
        }

        private static void AddResizeOnKeyboardObservers(UIWindow uiWindow)
        {
            AddManagedObserver(uiWindow, NSNotificationCenter.DefaultCenter.AddObserver(UIKeyboard.DidShowNotification, (notification) =>
            {
                NSValue keyboardFrameBeginUserInfo = (NSValue)notification.UserInfo.ValueForKey(UIKeyboard.FrameBeginUserInfoKey);
                NSValue keyboardFrameEndUserInfo = (NSValue)notification.UserInfo.ValueForKey(UIKeyboard.FrameEndUserInfoKey);
                float keyboardHeightChange = keyboardFrameEndUserInfo.RectangleFValue.Bottom - keyboardFrameBeginUserInfo.RectangleFValue.Bottom;
                if (keyboardHeightChange != 0)
                {
                    uiWindow.Frame = new CGRect(
                        uiWindow.Frame.X,
                        uiWindow.Frame.Y,
                        uiWindow.Frame.Width,
                        uiWindow.Frame.Height - (keyboardFrameEndUserInfo.RectangleFValue.Bottom - keyboardFrameEndUserInfo.RectangleFValue.Top));
                }
            }));

            AddManagedObserver(uiWindow, NSNotificationCenter.DefaultCenter.AddObserver(UIKeyboard.DidHideNotification, (notification) =>
            {
                NSValue keyboardFrameBeginUserInfo = (NSValue)notification.UserInfo.ValueForKey(UIKeyboard.FrameBeginUserInfoKey);
                NSValue keyboardFrameEndUserInfo = (NSValue)notification.UserInfo.ValueForKey(UIKeyboard.FrameEndUserInfoKey);
                float keyboardHeightChange = keyboardFrameEndUserInfo.RectangleFValue.Bottom - keyboardFrameBeginUserInfo.RectangleFValue.Bottom;
                if (keyboardHeightChange != 0)
                {
                    uiWindow.Frame = new CGRect(
                        uiWindow.Frame.X,
                        uiWindow.Frame.Y,
                        uiWindow.Frame.Width,
                        uiWindow.Frame.Height + (keyboardFrameEndUserInfo.RectangleFValue.Bottom - keyboardFrameEndUserInfo.RectangleFValue.Top));
                }
            }));
        }

        private static void RemoveManagedObservers(UIWindow uiWindow)
        {
            if (mAddedObservers.ContainsKey(uiWindow))
            {
                NSNotificationCenter.DefaultCenter.RemoveObservers(mAddedObservers[uiWindow]);
                mAddedObservers[uiWindow].Clear();
            }
        }

        private static void AddManagedObserver(UIWindow uiWindow, NSObject observer)
        {
            if (!mAddedObservers.ContainsKey(uiWindow))
            {
                mAddedObservers.Add(uiWindow, new List<NSObject>());
            }

            mAddedObservers[uiWindow].Add(observer);
        }
    }
}