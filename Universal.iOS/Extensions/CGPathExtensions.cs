﻿using CoreGraphics;
using System.Collections.Generic;

namespace Universal.iOS.Extensions
{
    /// <summary>
    /// Extensions for the <see cref="CGPath" /> class.
    /// </summary>
    public static class CGPathExtensions
    {
        /// <summary>
        /// Gets the <see cref="CGPathElement" />s contained in this path.
        /// </summary>
        /// <param name="CGPath"></param>
        /// <returns></returns>
        public static IEnumerable<CGPathElement> EnumerateElements(this CGPath CGPath)
        {
            List<CGPathElement> elements = new List<CGPathElement>();

            CGPath.Apply((element) =>
            {
                elements.Add(element);
            });

            return elements;
        }
    }
}