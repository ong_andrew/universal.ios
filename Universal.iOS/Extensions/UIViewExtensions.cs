﻿using CoreGraphics;
using System;
using System.Collections.Generic;
using System.Linq;
using UIKit;
using Universal.iOS.Views;

namespace Universal.iOS.Extensions
{
    /// <summary>
    /// Extensions for the <see cref="UIView"/> class.
    /// </summary>
    public static class UIViewExtensions
    {
        private static Dictionary<UIView, ViewGroup.LayoutParameters> LayoutParameterDictionary;

        static UIViewExtensions()
        {
            LayoutParameterDictionary = new Dictionary<UIView, ViewGroup.LayoutParameters>();
        }

        /// <summary>
        /// Gets a virtually-attached <see cref="ViewGroup.LayoutParameters"/> 
        /// </summary>
        /// <param name="UIView"></param>
        /// <returns></returns>
        public static ViewGroup.LayoutParameters GetLayoutParameters(this UIView UIView) 
        {
            if (LayoutParameterDictionary.ContainsKey(UIView))
            {
                return LayoutParameterDictionary[UIView];
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        /// Sets a virtually-attached <see cref="ViewGroup.LayoutParameters"/>, notifying the parent if it derived from <see cref="ViewGroup"/>, or patching some functionality if it does not.
        /// </summary>
        /// <param name="UIView"></param>
        /// <param name="layoutParameters"></param>
        public static void SetLayoutParameters(this UIView UIView, ViewGroup.LayoutParameters layoutParameters)
        {
            ViewGroup.LayoutParameters layoutParametersOld = GetLayoutParameters(UIView);

            if (layoutParameters == null)
            {
                if (LayoutParameterDictionary.ContainsKey(UIView))
                {
                    LayoutParameterDictionary.Remove(UIView);
                }
            }
            else
            {
                if (LayoutParameterDictionary.ContainsKey(UIView))
                {
                    LayoutParameterDictionary[UIView] = layoutParameters;
                }
                else
                {
                    LayoutParameterDictionary.Add(UIView, layoutParameters);
                }
            }

            if (UIView.Superview != null && UIView.Superview is ViewGroup)
            {
                ((ViewGroup)UIView.Superview).NotifySubviewLayoutParametersChanged(UIView, layoutParametersOld, layoutParameters);
            }
            else if (UIView.Superview != null && !(UIView.Superview is ViewGroup))
            {
                if (layoutParameters != null)
                {
                    double newWidth = UIView.Frame.Width;
                    double newHeight = UIView.Frame.Height;
                    if (layoutParameters.Width == ViewGroup.LayoutParameters.MatchParent)
                    {
                        newWidth = UIView.Superview.Frame.Width;
                    }
                    else if (layoutParameters.Width >= 0)
                    {
                        newWidth = layoutParameters.Width;
                    }

                    if (layoutParameters.Height == ViewGroup.LayoutParameters.MatchParent)
                    {
                        newHeight = UIView.Superview.Frame.Height;
                    }
                    else if (layoutParameters.Height >= 0)
                    {
                        newHeight = layoutParameters.Height;
                    }

                    CGSize newSize = new CGSize(newWidth, newHeight);
                    UIView.SetFrameSizeIfNeeded(newSize);
                }
            }

            UIView.SetNeedsLayout();
        }

        /// <summary>
        /// Enumerates the superviews of this <see cref="UIView"/>.
        /// </summary>
        /// <param name="UIView"></param>
        /// <returns></returns>
        public static IEnumerable<UIView> Ancestors(this UIView UIView)
        {
            UIView superview = UIView.Superview;
            while (superview != null)
            {
                yield return superview;
                superview = superview.Superview;
            }
        }

        /// <summary>
        /// Enumerates the superviews of type <typeparamref name="T"/> of this <see cref="UIView"/>.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="UIView"></param>
        /// <returns></returns>
        public static IEnumerable<T> Ancestors<T>(this UIView UIView)
            where T : UIView
        {
            return UIView.Ancestors().OfType<T>();
        }

        /// <summary>
        /// Enumerates the superviews of this <see cref="UIView"/>.
        /// </summary>
        /// <param name="UIView"></param>
        /// <returns></returns>
        [Obsolete("Use Ancestors instead.")]
        public static IEnumerable<UIView> EnumerateAncestors(this UIView UIView)
        {
            return UIView.Ancestors();
        }

        /// <summary>
        /// Enumerates the children of this <see cref="UIView"/> (ie its subviews).
        /// </summary>
        /// <param name="UIView"></param>
        /// <returns></returns>
        public static IEnumerable<UIView> Children(this UIView UIView)
        {
            return UIView.Subviews;
        }

        /// <summary>
        /// Enumerates the children of this <see cref="UIView"/> (ie its subviews) of the given type.
        /// </summary>
        /// <param name="UIView"></param>
        /// <returns></returns>
        public static IEnumerable<T> Children<T>(this UIView UIView)
            where T : UIView
        {
            return UIView.Subviews.OfType<T>();
        }

        /// <summary>
        /// Enumerates the descendant views of this <see cref="UIView"/>.
        /// </summary>
        /// <param name="UIView"></param>
        /// <returns></returns>
        public static IEnumerable<UIView> Descendants(this UIView UIView)
        {
            return UIView.Subviews.Concat(UIView.Subviews.SelectMany(x => x.Descendants()));
        }

        /// <summary>
        /// Enumerates the descendant views of type <typeparamref name="T"/> of this <see cref="UIView"/>.
        /// </summary>
        /// <param name="UIView"></param>
        /// <returns></returns>
        public static IEnumerable<T> Descendants<T>(this UIView UIView)
            where T : UIView
        {
            return UIView.Descendants().OfType<T>();
        }

        /// <summary>
        /// Enumerates the descendant views of this <see cref="UIView"/>.
        /// </summary>
        /// <param name="UIView"></param>
        /// <returns></returns>
        [Obsolete("Use Descendants instead.")]
        public static IEnumerable<UIView> EnumerateDescendants(this UIView UIView)
        {
            return UIView.Descendants();
        }

        /// <summary>
        /// Finds the first responder for the given <see cref="UIView"/>.
        /// </summary>
        /// <param name="UIView"></param>
        /// <returns></returns>
        public static UIView FindFirstResponder(this UIView UIView)
        {
            if (UIView.IsFirstResponder)
            {
                return UIView;
            }
            else
            {
                foreach (UIView uiView in UIView.Subviews)
                {
                    UIView firstResponderSubview = uiView.FindFirstResponder();

                    if (firstResponderSubview != null)
                    {
                        return firstResponderSubview;
                    }
                }

                return null;
            }
        }

        /// <summary>
        /// Returns null or the first view of the given type amongst the view and its descendants if found.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="UIView"></param>
        /// <returns></returns>
        public static T FindViewByType<T>(this UIView UIView)
            where T : UIView
        {
            if (UIView is T)
            {
                return UIView as T;
            }
            else
            {
                return UIView.Descendants<T>().FirstOrDefault();
            }
        }

        /// <summary>
        /// Sets the frame of the <see cref="UIView"/> if it is different from the <paramref name="newFrame"/>.
        /// </summary>
        /// <param name="UIView"></param>
        /// <param name="newFrame"></param>
        public static void SetFrameIfNeeded(this UIView UIView, CGRect newFrame)
        {
            if (UIView.Frame == null || UIView.Frame.Width != newFrame.Width || UIView.Frame.Height != newFrame.Height || UIView.Frame.X != newFrame.X || UIView.Frame.Y != newFrame.Y)
            {
                UIView.Frame = newFrame;
            }
        }

        /// <summary>
        /// Sets the size of the <see cref="UIView"/> if it is different from the <paramref name="newSize"/>.
        /// </summary>
        /// <param name="UIView"></param>
        /// <param name="newSize"></param>
        public static void SetFrameSizeIfNeeded(this UIView UIView, CGSize newSize)
        {
            if (UIView.Frame == null || UIView.Frame.Width != newSize.Width || UIView.Frame.Height != newSize.Height)
            {
                UIView.Frame = new CGRect(UIView.Frame.Location, newSize);
            }
        }

        /// <summary>
        /// Calls <see cref="UIView.RemoveFromSuperview" /> on the subviews of this <see cref="UIView" />.
        /// </summary>
        /// <param name="UIView"></param>
        public static void RemoveAllSubviews(this UIView UIView)
        {
            foreach (UIView subview in UIView.Subviews)
            {
                subview.RemoveFromSuperview();
            }
        }

        /// <summary>
        /// Removes all constraints involving the given <see cref="UIView"/>.
        /// </summary>
        /// <param name="UIView"></param>
        public static void RemoveAllConstraints(this UIView UIView)
        {
            UIView superview = UIView.Superview;
            while (superview != null)
            {
                foreach (NSLayoutConstraint layoutConstraint in superview.Constraints)
                {
                    if (layoutConstraint.FirstItem == UIView || layoutConstraint.SecondItem == UIView)
                    {
                        superview.RemoveConstraint(layoutConstraint);
                    }
                }

                superview = superview.Superview;
            }

            UIView.RemoveConstraints(UIView.Constraints);
        }
    }
}