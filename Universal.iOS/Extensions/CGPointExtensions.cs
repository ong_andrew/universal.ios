﻿using CoreGraphics;

namespace Universal.iOS.Extensions
{
    /// <summary>
    /// Extensions for the <see cref="CGPoint" /> class.
    /// </summary>
    public static class CGPointExtensions
    {
        /// <summary>
        /// Adds the X and Y coordinates of the points, returning a new <see cref="CGPoint" /> with the result of the operation.
        /// </summary>
        /// <param name="CGPoint"></param>
        /// <param name="other"></param>
        /// <returns></returns>
        public static CGPoint Add(this CGPoint CGPoint, CGPoint other)
        {
            return new CGPoint(CGPoint.X + other.X, CGPoint.Y + other.Y);
        }

        /// <summary>
        /// Reflects the given point using the given center point.
        /// </summary>
        /// <param name="CGPoint"></param>
        /// <param name="center"></param>
        /// <returns></returns>
        public static CGPoint Reflect(this CGPoint CGPoint, CGPoint center)
        {
            return new CGPoint((2 * center.X) - CGPoint.X, (2 * center.Y) - CGPoint.Y);
        }

        /// <summary>
        /// Scales the X-coordinate of the point, returning a new <see cref="CGPoint" /> with the result of the operation.
        /// </summary>
        /// <param name="CGPoint"></param>
        /// <param name="scale"></param>
        /// <returns></returns>
        public static CGPoint ScaleX(this CGPoint CGPoint, double scale)
        {
            return new CGPoint(CGPoint.X * scale, CGPoint.Y);
        }

        /// <summary>
        /// Scales the Y-coordinate of the point, returning a new <see cref="CGPoint" /> with the result of the operation.
        /// </summary>
        /// <param name="CGPoint"></param>
        /// <param name="scale"></param>
        /// <returns></returns>
        public static CGPoint ScaleY(this CGPoint CGPoint, double scale)
        {
            return new CGPoint(CGPoint.X, CGPoint.Y * scale);
        }

        /// <summary>
        /// Scales both coordinates of the point by the same scaling factor, returning a new <see cref="CGPoint" /> with the result of the operation.
        /// </summary>
        /// <param name="CGPoint"></param>
        /// <param name="scale"></param>
        /// <returns></returns>
        public static CGPoint Scale(this CGPoint CGPoint, double scale)
        {
            return new CGPoint(CGPoint.X * scale, CGPoint.Y * scale);
        }

        /// <summary>
        /// Scales the coordinates of the points by the scaling factors, returning a new <see cref="CGPoint" /> with the result of the operation.
        /// </summary>
        /// <param name="CGPoint"></param>
        /// <param name="scaleX"></param>
        /// <param name="scaleY"></param>
        /// <returns></returns>
        public static CGPoint Scale(this CGPoint CGPoint, double scaleX, double scaleY)
        {
            return new CGPoint(CGPoint.X * scaleX, CGPoint.Y * scaleY);
        }

        /// <summary>
        /// Subtracts the X and Y coordinates of the points, returning a new <see cref="CGPoint" /> with the result of the operation.
        /// </summary>
        /// <param name="CGPoint"></param>
        /// <param name="other"></param>
        /// <returns></returns>
        public static CGPoint Subtract(this CGPoint CGPoint, CGPoint other)
        {
            return new CGPoint(CGPoint.X - other.X, CGPoint.Y - other.Y);
        }
    }
}