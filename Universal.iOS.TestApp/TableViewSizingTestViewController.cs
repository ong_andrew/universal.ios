﻿using System.Collections.Generic;
using System.Linq;
using UIKit;
using Universal.iOS.Extensions;
using Universal.iOS.Views;
using Universal.iOS.Widget;

namespace Universal.iOS.TestApp
{
    public class TableViewSizingTestViewController : UIViewController
    {
        public class CustomTableViewCell : TableViewCell
        {
            private Model mModel;
            public Model Model
            {
                get => mModel;
                set
                {
                    if (mModel != value)
                    {
                        mModel = value;
                        LinearLayout linearLayout = ContentView.Descendants<LinearLayout>().First();
                        TextView textViewTitle = ContentView.Descendants<TextView>().First();
                        textViewTitle.Text = Model?.Title ?? string.Empty;
                        TextView textViewDescription = ContentView.Descendants<TextView>().Last();
                        textViewDescription.Text = Model?.Description ?? string.Empty;
                        linearLayout.SetNeedsLayout();
                        SetNeedsLayout();
                    }
                }
            }

            public CustomTableViewCell()
            {
                LinearLayout linearLayout = new LinearLayout()
                {
                    Orientation = Orientation.Vertical
                };

                ContentView.AddSubview(linearLayout);

                linearLayout.SetLayoutParameters(new ViewGroup.LayoutParameters(ViewGroup.LayoutParameters.MatchParent, ViewGroup.LayoutParameters.WrapContent));

                ContentView.Layer.CornerRadius = 6.0f;
                ContentView.Layer.BorderColor = UIColor.Gray.CGColor;
                ContentView.Layer.BorderWidth = 1.0f;

                TextView textViewTitle = new TextView()
                {
                    Font = UIFont.PreferredTitle1
                };

                linearLayout.AddSubview(textViewTitle, new LinearLayout.LayoutParameters(ViewGroup.LayoutParameters.MatchParent, ViewGroup.LayoutParameters.WrapContent));

                TextView textViewDescription = new TextView()
                {
                    Font = UIFont.PreferredBody
                };

                linearLayout.AddSubview(textViewDescription, new LinearLayout.LayoutParameters(ViewGroup.LayoutParameters.MatchParent, ViewGroup.LayoutParameters.WrapContent));
            }
        }

        public class Model
        {
            public string Title;
            public string Description;
        }

        public override void LoadView()
        {
            UITableView tableView = new UITableView()
            {
                BackgroundColor = UIColor.White
            };
            tableView.TranslatesAutoresizingMaskIntoConstraints = false;

            RowActionTableViewSource<CustomTableViewCell, Model> uiTableViewSource = new RowActionTableViewSource<CustomTableViewCell, Model>(new List<Model>()
            {
                new Model() { Title = "Item 1", Description = "Lorem ipsum" },
                new Model() { Title = "Item 2", Description = "Lorem ipsum superlong text whoa...." },
                new Model() { Title = "Item 3", Description = "Lorem ipsum superlong text whoa flow over multiple lines and do some expanding of the cell's height please" }
            }, (reuseIdentifier) =>
            {
                CustomTableViewCell customTableViewCell = new CustomTableViewCell();
                return customTableViewCell;
            },
            (tableViewCell, item) =>
            {
                tableViewCell.Model = item;
            });

            tableView.Source = uiTableViewSource;
            tableView.Delegate = new TableViewDelegate();
            View = tableView;
        }

    }
}