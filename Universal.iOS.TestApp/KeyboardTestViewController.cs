﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Foundation;
using UIKit;
using Universal.iOS.Extensions;
using Universal.iOS.Views;
using Universal.iOS.Widget;

namespace Universal.iOS.TestApp
{
    public class KeyboardTestViewController : UIViewController
    {
        public override void LoadView()
        {
            AutomaticallyAdjustsScrollViewInsets = false;

            ScrollView scrollView = new ScrollView();
            scrollView.ContentInset = UIEdgeInsets.Zero;
            scrollView.SetLayoutParameters(new ViewGroup.LayoutParameters(
                    ViewGroup.LayoutParameters.MatchParent,
                    ViewGroup.LayoutParameters.MatchParent));

            LinearLayout linearLayout = new LinearLayout()
            {
                BackgroundColor = UIColor.White,
                Orientation = Orientation.Vertical
            };
            scrollView.AddSubview(linearLayout, new ViewGroup.LayoutParameters(ViewGroup.LayoutParameters.MatchParent, ViewGroup.LayoutParameters.WrapContent));

            //EditText editText = new EditText();            
            //linearLayout.AddSubview(editText, new LinearLayout.LayoutParameters(ViewGroup.LayoutParameters.MatchParent, ViewGroup.LayoutParameters.WrapContent));

            FilePickerView filePickerView = new FilePickerView();
            linearLayout.AddSubview(filePickerView, new LinearLayout.LayoutParameters(ViewGroup.LayoutParameters.MatchParent, ViewGroup.LayoutParameters.WrapContent));

            PopupFilterableEditText popupFilterableEditText = new PopupFilterableEditText();
            popupFilterableEditText.Elements = new List<string>()
            {
                "Test A",
                "Test B",
                "Test C"
            };

            linearLayout.AddSubview(popupFilterableEditText, new LinearLayout.LayoutParameters(ViewGroup.LayoutParameters.MatchParent, ViewGroup.LayoutParameters.WrapContent));

            Button button = new Button();
            button.Text = "Remove keyboard";
            button.TouchUpInside += (sender, eventArgs) =>
            {
                View.EndEditing(true);
            };
            linearLayout.AddSubview(button, new LinearLayout.LayoutParameters(ViewGroup.LayoutParameters.WrapContent, ViewGroup.LayoutParameters.WrapContent));

            UIView rectangle = new UIView();
            linearLayout.AddSubview(rectangle, new LinearLayout.LayoutParameters(ViewGroup.LayoutParameters.MatchParent, 1000));

            UIView redSquare = new UIView()
            {
                BackgroundColor = UIColor.SystemRedColor
            };
            linearLayout.AddSubview(redSquare, new LinearLayout.LayoutParameters(100, 100));

            View = scrollView;
        }

        public override void ViewWillDisappear(bool animated)
        {
            UIApplication.SharedApplication.KeyWindow.SetWindowKeyboardMode(KeyboardMode.AdjustNone);
        }

        public override void ViewDidAppear(bool animated)
        {
            UIApplication.SharedApplication.KeyWindow.SetWindowKeyboardMode(KeyboardMode.AdjustResize);
        }
    }
}