﻿namespace Universal.iOS.Microsoft.Cards
{
    /// <summary>
    /// <see cref="ICardActionHandler{TCard, TAction}" /> that has Adaptive Card specific events.
    /// </summary>
    /// <typeparam name="TCard"></typeparam>
    /// <typeparam name="TAction"></typeparam>
    public interface IAdaptiveCardActionHandler<TCard, TAction> : ICardActionHandler<TCard, TAction>
    {
        void OnSubmit(RenderedCard<TCard> renderedCard, TAction action, object value);
        void OnOpenUrl(RenderedCard<TCard> renderedCard, TAction action, string url);
        void OnShowCard(RenderedCard<TCard> renderedCard, TAction action, RenderedCard<TCard> card);
    }
}