﻿namespace Universal.iOS.Microsoft.Cards
{
    /// <summary>
    /// Represents a handler of a card action.
    /// </summary>
    /// <typeparam name="TCard"></typeparam>
    /// <typeparam name="TAction"></typeparam>
    public interface ICardActionHandler<TCard, TAction>
    {
        void OnAction(RenderedCard<TCard> renderedCard, TAction action);
    }
}