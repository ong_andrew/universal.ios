﻿using System;

namespace Universal.iOS.Microsoft.Cards
{
    /// <summary>
    /// A generic handler for card actions.
    /// </summary>
    /// <typeparam name="TCard"></typeparam>
    /// <typeparam name="TAction"></typeparam>
    public class CardActionHandler<TCard, TAction> : ICardActionHandler<TCard, TAction>
    {
        /// <summary>
        /// Raised when <see cref="OnAction(RenderedCard{TCard}, TAction)" /> is called.
        /// </summary>
        public event EventHandler<EventArgs<TCard, TAction>> Action;

        public virtual void OnAction(RenderedCard<TCard> renderedCard, TAction action)
        {
            Action?.Invoke(this, new EventArgs<TCard, TAction>(renderedCard, action));
        }

        public class EventArgs<TCard, TAction> : System.EventArgs
        {
            /// <summary>
            /// The rendered instance of the card.
            /// </summary>
            public RenderedCard<TCard> RenderedCard { get; protected set; }
            /// <summary>
            /// The action to be executed.
            /// </summary>
            public TAction Action { get; protected set; }

            public EventArgs(RenderedCard<TCard> renderedCard, TAction action)
            {
                RenderedCard = renderedCard;
                Action = action;
            }
        }
    }
}