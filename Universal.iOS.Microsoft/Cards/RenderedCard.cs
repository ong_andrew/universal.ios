﻿using UIKit;

namespace Universal.iOS.Microsoft.Cards
{
    /// <summary>
    /// Represents a rendered card.
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class RenderedCard<T>
    {
        /// <summary>
        /// The card specification.
        /// </summary>
        public T Card { get; protected set; }
        /// <summary>
        /// The view instance that it was rendered as.
        /// </summary>
        public UIView View { get; protected set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="RenderedCard{T}" /> class with the given parameters.
        /// </summary>
        /// <param name="card"></param>
        /// <param name="view"></param>
        public RenderedCard(T card, UIView view)
        {
            Card = card;
            View = view;
        }
    }
}