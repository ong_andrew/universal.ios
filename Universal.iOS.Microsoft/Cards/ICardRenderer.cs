﻿namespace Universal.iOS.Microsoft.Cards
{
    /// <summary>
    /// Represents an object that can render a given card.
    /// </summary>
    /// <typeparam name="TCard"></typeparam>
    /// <typeparam name="TAction"></typeparam>
    public interface ICardRenderer<TCard, TAction>
    {
        RenderedCard<TCard> Render(TCard card, ICardActionHandler<TCard, TAction> actionHandler);
    }
}