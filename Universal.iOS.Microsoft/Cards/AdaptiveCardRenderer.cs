﻿using AdaptiveCards;
using Foundation;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Net.Http;
using UIKit;
using Universal.Common.Collections.Extensions;
using Universal.Common.Extensions;
using Universal.iOS.Extensions;
using Universal.iOS.Views;
using Universal.iOS.Widget;

namespace Universal.iOS.Microsoft.Cards
{
    /// <summary>
    /// Renderer for adaptive cards.
    /// </summary>
    public class AdaptiveCardRenderer : ICardRenderer<AdaptiveCard, AdaptiveAction>, IDisposable
    {
        protected HttpClient HttpClient { get; set; }

        public AdaptiveCardRenderer()
        {
            HttpClient = new HttpClient();
        }

        RenderedCard<AdaptiveCard> ICardRenderer<AdaptiveCard, AdaptiveAction>.Render(AdaptiveCard card, ICardActionHandler<AdaptiveCard, AdaptiveAction> actionHandler)
        {
            return Render(card, actionHandler);
        }

        /// <summary>
        /// Renders the given card.
        /// </summary>
        /// <param name="card"></param>
        /// <param name="actionHandler"></param>
        /// <returns></returns>
        public RenderedAdaptiveCard Render(AdaptiveCard card, ICardActionHandler<AdaptiveCard, AdaptiveAction> actionHandler)
        {
            LinearLayout linearLayout = new LinearLayout()
            {
                Orientation = Orientation.Vertical
            };
            RenderedAdaptiveCard result = new RenderedAdaptiveCard(card, linearLayout);

            Action<JObject> aggregateBinder = null;

            if (card.Body != null && card.Body.Any())
            {
                LinearLayout bodyContainer = new LinearLayout()
                {
                    Orientation = Orientation.Vertical
                };
                bodyContainer.SetLayoutParameters(new LinearLayout.LayoutParameters(ViewGroup.LayoutParameters.MatchParent, ViewGroup.LayoutParameters.WrapContent));

                foreach (AdaptiveElement element in card.Body)
                {
                    (UIView elementView, Action<JObject> binder) = RenderElement(new LinearLayout.LayoutParameters(ViewGroup.LayoutParameters.MatchParent, ViewGroup.LayoutParameters.WrapContent), element);

                    if (binder != null)
                    {
                        if (aggregateBinder == null)
                        {
                            aggregateBinder = binder;
                        }
                        else
                        {
                            aggregateBinder += binder;
                        }
                    }

                    bodyContainer.AddSubview(elementView);
                }

                linearLayout.AddSubview(bodyContainer);
            }

            result.InputBinders = aggregateBinder;

            if (card.Actions != null && card.Actions.Any())
            {
                LinearLayout actionContainer = new LinearLayout()
                {
                    Orientation = Orientation.Vertical
                };

                actionContainer.SetLayoutParameters(new LinearLayout.LayoutParameters(ViewGroup.LayoutParameters.MatchParent, ViewGroup.LayoutParameters.WrapContent));

                foreach (AdaptiveAction action in card.Actions)
                {
                    UIView actionView = RenderActionView(new LinearLayout.LayoutParameters(ViewGroup.LayoutParameters.MatchParent, ViewGroup.LayoutParameters.WrapContent), action);
                    actionView.AddGestureRecognizer(new UITapGestureRecognizer(() =>
                    {
                        if (action is AdaptiveShowCardAction showCardAction)
                        {
                            if (result.ShowCardActionToCardMapping.ContainsKey(showCardAction))
                            {
                                if (actionHandler is IAdaptiveCardActionHandler<AdaptiveCard, AdaptiveAction> typedActionHandler)
                                {
                                    typedActionHandler.OnShowCard(result, action, result.ShowCardActionToCardMapping.ContainsKey(showCardAction) ? result.ShowCardActionToCardMapping[showCardAction] : null);
                                }

                                if (result.ShowCardActionToCardMapping[showCardAction].View.GetLayoutParameters() != null)
                                {
                                    ViewGroup.LayoutParameters layoutParameters = result.ShowCardActionToCardMapping[showCardAction].View.GetLayoutParameters();

                                    if (layoutParameters.Visibility == ViewStates.Visible)
                                    {
                                        layoutParameters.Visibility = ViewStates.Gone;
                                    }
                                    else
                                    {
                                        layoutParameters.Visibility = ViewStates.Visible;
                                    }
                                }

                                if (result.ShowCardActionToCardMapping[showCardAction].View.Hidden)
                                {
                                    result.ShowCardActionToCardMapping[showCardAction].View.Hidden = false;
                                }
                                else
                                {
                                    result.ShowCardActionToCardMapping[showCardAction].View.Hidden = true;
                                }

                                if (result.View.Superview != null)
                                {

                                }
                            }
                            else
                            {
                                Console.WriteLine("ShowCard action initiated but no rendered card available for given action.");
                            }

                            actionHandler?.OnAction(result, action);
                        }
                        else if (action is AdaptiveSubmitAction submitAction)
                        {
                            object submitData = submitAction.Data;

                            if (submitData is JObject jsonObjectOriginal)
                            {
                                submitData = jsonObjectOriginal.DeepClone();
                            }

                            if (submitData == null)
                            {
                                submitData = new JObject();
                            }

                            if (submitData is JObject jsonObject)
                            {
                                foreach (RenderedAdaptiveCard cardChainCard in result.FindRootNode(x => x.ParentCard).BreadthFirst(x => x.ShowCardActionToCardMapping.Select(y => y.Value)))
                                {
                                    cardChainCard.InputBinders?.Invoke(jsonObject);
                                }
                            }

                            if (actionHandler is IAdaptiveCardActionHandler<AdaptiveCard, AdaptiveAction> typedActionHandler)
                            {
                                typedActionHandler.OnSubmit(result, action, submitData);
                            }
                        }
                        else if (action is AdaptiveOpenUrlAction openUrlAction)
                        {
                            if (actionHandler is IAdaptiveCardActionHandler<AdaptiveCard, AdaptiveAction> typedActionHandler)
                            {
                                typedActionHandler.OnOpenUrl(result, action, openUrlAction.Url.ToString());
                            }
                        }
                    }));

                    actionContainer.AddSubview(actionView);

                    if (action.Type == Universal.Microsoft.Cards.Adaptive.ActionTypes.ShowCard)
                    {
                        if (action is AdaptiveShowCardAction showCardAction)
                        {
                            AdaptiveCard actionCard = showCardAction.Card;
                            RenderedAdaptiveCard renderedActionCard = Render(actionCard, actionHandler);
                            renderedActionCard.ParentCard = result;
                            result.ShowCardActionToCardMapping.Add(showCardAction, renderedActionCard);
                        }
                        else
                        {
                            Console.WriteLine($"ShowCard action found, but invalid type provided for action. Received {action?.GetType()}, expected {typeof(AdaptiveShowCardAction)}.");
                        }
                    }
                }

                linearLayout.AddSubview(actionContainer);
            }

            foreach (RenderedCard<AdaptiveCard> renderedShowCardActionCard in result.ShowCardActionToCardMapping.Select(x => x.Value))
            {
                renderedShowCardActionCard.View.SetLayoutParameters(new LinearLayout.LayoutParameters(ViewGroup.LayoutParameters.MatchParent, ViewGroup.LayoutParameters.WrapContent)
                {
                    Visibility = ViewStates.Gone
                });
                renderedShowCardActionCard.View.Hidden = true;
                linearLayout.AddSubview(renderedShowCardActionCard.View);
            }

            return result;
        }

        protected virtual (UIView View, Action<JObject> Binder) RenderElement(ViewGroup.LayoutParameters layoutParameters, AdaptiveElement element)
        {
            UIView view = null;
            Action<JObject> binder = null;

            if (element is AdaptiveContainer container)
            {
                (view, binder) = RenderContainer(layoutParameters, container);
            }
            else if (element is AdaptiveTextBlock textBlock)
            {
                view = RenderTextBlock(layoutParameters, textBlock);
            }
            else if (element is AdaptiveImage image)
            {
                view = RenderImage(layoutParameters, image);
            }
            else if (element is AdaptiveImageSet imageSet)
            {
                view = RenderImageSet(layoutParameters, imageSet);
            }
            else if (element is AdaptiveColumnSet columnSet)
            {
                (view, binder) = RenderColumnSet(layoutParameters, columnSet);
            }
            else if (element is AdaptiveColumn column)
            {
                (view, binder) = RenderColumn(layoutParameters, column);
            }
            else if (element is AdaptiveFactSet factSet)
            {
                view = RenderFactSet(layoutParameters, factSet);
            }
            else if (element is AdaptiveTextInput textInput)
            {
                (view, binder) = RenderTextInput(layoutParameters, textInput);
            }
            else if (element is AdaptiveChoiceSetInput choiceSetInput)
            {
                (view, binder) = RenderChoiceSetInput(layoutParameters, choiceSetInput);
            }
            else if (element is AdaptiveDateInput dateInput)
            {
                (view, binder) = RenderDateInput(layoutParameters, dateInput);
            }
            else if (element is AdaptiveTimeInput timeInput)
            {
                (view, binder) = RenderTimeInput(layoutParameters, timeInput);
            }
            else if (element is AdaptiveNumberInput numberInput)
            {
                (view, binder) = RenderNumberInput(layoutParameters, numberInput);
            }
            else if (element is AdaptiveToggleInput toggleInput)
            {
                (view, binder) = RenderToggleInput(layoutParameters, toggleInput);
            }
            else
            {
                view = RenderUnhandledElement(layoutParameters, element);
            }

            if (view != null)
            {
                view.SetLayoutParameters(layoutParameters);

                if (!element.IsVisible)
                {
                    view.Hidden = true;
                }
            }

            return (view, binder);
        }

        public virtual (UIView View, Action<JObject> Binder) RenderContainer(ViewGroup.LayoutParameters layoutParameters, AdaptiveContainer container)
        {
            Action<JObject> aggregateBinder = null;

            LinearLayout containerView = new LinearLayout()
            {
                Orientation = Orientation.Vertical
            };

            foreach (AdaptiveElement element in container.Items)
            {
                (UIView elementView, Action<JObject> binder) = RenderElement(new LinearLayout.LayoutParameters(ViewGroup.LayoutParameters.WrapContent, ViewGroup.LayoutParameters.WrapContent), element);

                if (binder != null)
                {
                    if (aggregateBinder == null)
                    {
                        aggregateBinder = binder;
                    }
                    else
                    {
                        aggregateBinder += binder;
                    }
                }

                containerView.AddSubview(elementView);
            }

            return (containerView, aggregateBinder);
        }

        public virtual UIView RenderTextBlock(ViewGroup.LayoutParameters layoutParameters, AdaptiveTextBlock textBlock)
        {
            UILabel textBlockView = new TextView();

            textBlockView.Text = textBlock.Text;

            textBlockView.Font = UIFont.PreferredBody;

            if (textBlock.Size == AdaptiveTextSize.Large)
            {
                textBlockView.Font = UIFont.PreferredBody;
            }
            else if (textBlock.Size == AdaptiveTextSize.ExtraLarge)
            {
                textBlockView.Font = UIFont.PreferredBody;
            }
            else if (textBlock.Size == AdaptiveTextSize.Small)
            {
                textBlockView.Font = UIFont.PreferredBody;
            }

            if (textBlock.Weight == AdaptiveTextWeight.Bolder && textBlock.Italic)
            {
                textBlockView.Font = UIFont.PreferredBody;
            }
            else if (textBlock.Weight == AdaptiveTextWeight.Bolder)
            {
                textBlockView.Font = UIFont.PreferredBody;
            }
            else if (textBlock.Italic)
            {
                textBlockView.Font = UIFont.PreferredBody;
            }

            if (layoutParameters is LinearLayout.LayoutParameters linearLayoutParameters)
            {
                if (textBlock.HorizontalAlignment == AdaptiveHorizontalAlignment.Center)
                {
                    linearLayoutParameters.Gravity = GravityFlags.CenterHorizontal;
                }
                else if (textBlock.HorizontalAlignment == AdaptiveHorizontalAlignment.Right)
                {
                    linearLayoutParameters.Gravity = GravityFlags.Right;
                }
            }

            return textBlockView;
        }

        public virtual (UIView View, Action<JObject> Binder) RenderTextInput(ViewGroup.LayoutParameters layoutParameters, AdaptiveTextInput textInput)
        {
            EditText textInputView = new EditText()
            {
                Placeholder = textInput.Placeholder
            };

            if (!textInput.Value.IsNullOrEmpty())
            {
                textInputView.Text = textInput.Value;
            }

            return (textInputView, (JObject data) =>
            {
                data.Add(textInput.Id, textInputView.Text);
            });
        }

        public virtual (UIView View, Action<JObject> Binder) RenderChoiceSetInput(ViewGroup.LayoutParameters layoutParameters, AdaptiveChoiceSetInput choiceSetInput)
        {
            if (choiceSetInput.IsMultiSelect)
            {
                LinearLayout linearLayout = new LinearLayout()
                {
                    Orientation = Orientation.Vertical
                };

                List<CheckBox> checkBoxes = new List<CheckBox>();

                foreach (AdaptiveChoice choice in choiceSetInput.Choices)
                {
                    CheckBox checkBox = new CheckBox()
                    {
                        Text = choice.Title
                    };

                    checkBox.SetLayoutParameters(new LinearLayout.LayoutParameters(ViewGroup.LayoutParameters.WrapContent, ViewGroup.LayoutParameters.WrapContent));

                    linearLayout.AddSubview(checkBox);
                    checkBoxes.Add(checkBox);
                }

                if (!choiceSetInput.Value.IsNullOrEmpty())
                {
                    string[] values = choiceSetInput.Value.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries);

                    for (int i = 0; i < choiceSetInput.Choices.Count; i++)
                    {
                        if (values.Contains(choiceSetInput.Choices[i].Value))
                        {
                            checkBoxes[i].Checked = true;
                        }
                    }
                }

                return (linearLayout, (JObject data) =>
                {
                    if (choiceSetInput.Choices.Count != checkBoxes.Count)
                    {
                        Console.WriteLine("Could not bind given choice set because of mismatched schema and view.");
                        return;
                    }

                    List<string> selectedChoices = new List<string>();

                    for (int i = 0; i < choiceSetInput.Choices.Count; i++)
                    {
                        if (checkBoxes[i].Checked)
                        {
                            selectedChoices.Add(choiceSetInput.Choices[i].Value);
                        }
                    }

                    data.Add(choiceSetInput.Id, string.Join(",", selectedChoices));
                }
                );
            }
            else
            {
                if (choiceSetInput.Style == AdaptiveChoiceInputStyle.Expanded)
                {
                    RadioGroup radioGroup = new RadioGroup()
                    {
                        Orientation = Orientation.Vertical
                    };

                    List<RadioButton> radioButtons = new List<RadioButton>();

                    foreach (AdaptiveChoice choice in choiceSetInput.Choices)
                    {
                        RadioButton radioButton = new RadioButton()
                        {
                            Text = choice.Title
                        };

                        radioButton.SetLayoutParameters(new RadioGroup.LayoutParameters(ViewGroup.LayoutParameters.WrapContent, ViewGroup.LayoutParameters.WrapContent));

                        radioGroup.AddSubview(radioButton);
                        radioButtons.Add(radioButton);
                    }

                    if (!choiceSetInput.Value.IsNullOrEmpty())
                    {
                        if (choiceSetInput.Choices.Any(x => x.Value == choiceSetInput.Value))
                        {
                            for (int i = 0; i < choiceSetInput.Choices.Count; i++)
                            {
                                if (choiceSetInput.Choices[i].Value == choiceSetInput.Value)
                                {
                                    radioButtons[i].Checked = true;
                                    break;
                                }
                            }
                        }
                    }

                    return (radioGroup, (JObject data) =>
                    {
                        if (choiceSetInput.Choices.Count != radioButtons.Count)
                        {
                            Console.WriteLine("Could not bind given choice set because of mismatched schema and view.");
                            return;
                        }

                        List<string> selectedChoices = new List<string>();

                        for (int i = 0; i < choiceSetInput.Choices.Count; i++)
                        {
                            if (radioButtons[i].Checked)
                            {
                                selectedChoices.Add(choiceSetInput.Choices[i].Value);
                            }
                        }

                        data.Add(choiceSetInput.Id, string.Join(",", selectedChoices));
                    }
                    );
                }
                else
                {
                    PopupFilterableEditText<AdaptiveChoice> popupFilterableEditText = new PopupFilterableEditText<AdaptiveChoice>();

                    popupFilterableEditText.Elements = choiceSetInput.Choices;
                    popupFilterableEditText.Projection = (choice) => choice.Title;

                    if (!choiceSetInput.Value.IsNullOrEmpty() && popupFilterableEditText.Elements.Any(x => x.Value == choiceSetInput.Value))
                    {
                        popupFilterableEditText.Value = popupFilterableEditText.Elements.First(x => x.Value == choiceSetInput.Value);
                    }

                    return (popupFilterableEditText, (JObject data) =>
                    {
                        data.Add(choiceSetInput.Id, popupFilterableEditText.Value != null ? popupFilterableEditText.Value.Value : null);
                    }
                    );
                }
            }
        }

        public virtual (UIView View, Action<JObject> Binder) RenderDateInput(ViewGroup.LayoutParameters layoutParameters, AdaptiveDateInput dateInput)
        {
            DatePickerEditText datePickerEditText = new DatePickerEditText()
            {
                Placeholder = dateInput.Placeholder,
                Format = "yyyy-MM-dd"
            };

            if (dateInput.Value != null)
            {
                if (DateTime.TryParseExact(dateInput.Value, "yyyy-MM-dd", CultureInfo.InvariantCulture, DateTimeStyles.None, out DateTime date))
                {
                    datePickerEditText.Date = date;
                }
                else
                {
                    Console.WriteLine($"Could not parse date value {dateInput.Value}.");
                }
            }

            return (datePickerEditText, (JObject data) => { data.Add(dateInput.Id, datePickerEditText.Date?.ToString("yyyy-MM-dd")); });
        }

        public virtual (UIView View, Action<JObject> Binder) RenderTimeInput(ViewGroup.LayoutParameters layoutParameters, AdaptiveTimeInput timeInput)
        {
            TimePickerEditText timePickerEditText = new TimePickerEditText()
            {
                Placeholder = timeInput.Placeholder,
                Format = "hh\\:mm"
            };

            if (timeInput.Value != null)
            {
                if (TimeSpan.TryParseExact(timeInput.Value, "hh\\:mm", CultureInfo.InvariantCulture, out TimeSpan time))
                {
                    timePickerEditText.Time = time;
                }
                else
                {
                    Console.WriteLine($"Could not parse time value {timeInput.Value}.");
                }
            }

            return (timePickerEditText, (JObject data) => { data.Add(timeInput.Id, timePickerEditText.Time?.ToString("hh\\:mm")); });
        }

        public virtual (UIView View, Action<JObject> Binder) RenderNumberInput(ViewGroup.LayoutParameters layoutParameters, AdaptiveNumberInput numberInput)
        {
            EditText editText = new EditText()
            {
                Placeholder = numberInput.Placeholder
            };

            editText.Text = numberInput.Value.ToString();

            return (editText, (JObject data) => { data.Add(numberInput.Id, editText.Text); });
        }

        public virtual (UIView View, Action<JObject> Binder) RenderToggleInput(ViewGroup.LayoutParameters layoutParameters, AdaptiveToggleInput toggleInput)
        {
            CheckBox checkBox = new CheckBox()
            {
                Text = toggleInput.Title
            };

            if (toggleInput.Value == toggleInput.ValueOn)
            {
                checkBox.Checked = true;
            }

            return (checkBox, (JObject data) => { data.Add(toggleInput.Id, checkBox.Checked ? toggleInput.ValueOn : toggleInput.ValueOff); });
        }

        public virtual UIView RenderImage(ViewGroup.LayoutParameters layoutParameters, AdaptiveImage image)
        {
            UIImageView imageView = new UIImageView();

            try
            {
                using (HttpResponseMessage httpResponseMessage = HttpClient.GetAsync(image.Url).GetAwaiter().GetResult())
                {
                    if (httpResponseMessage.IsSuccessStatusCode)
                    {
                        imageView.Image = UIImage.LoadFromData(NSData.FromArray(httpResponseMessage.Content.ReadAsByteArrayAsync().GetAwaiter().GetResult()));
                    }
                }
            }
            catch (Exception e)
            {
                Console.WriteLine($"Could not render image:{Environment.NewLine}{e}.");
                return null;
            }

            return imageView;
        }

        public virtual UIView RenderImageSet(ViewGroup.LayoutParameters layoutParameters, AdaptiveImageSet imageSet)
        {
            LinearLayout container = new LinearLayout()
            {
                Orientation = Orientation.Vertical
            };

            foreach (AdaptiveImage image in imageSet.Images)
            {
                LinearLayout.LayoutParameters imageLayoutParameters = new LinearLayout.LayoutParameters(ViewGroup.LayoutParameters.WrapContent, ViewGroup.LayoutParameters.WrapContent);

                UIView imageView = RenderImage(imageLayoutParameters, image);

                imageView.SetLayoutParameters(imageLayoutParameters);

                container.AddSubview(imageView);
            }

            return container;
        }

        public virtual (UIView View, Action<JObject> Binder) RenderColumnSet(ViewGroup.LayoutParameters layoutParameters, AdaptiveColumnSet columnSet)
        {
            Action<JObject> aggregateBinder = null;

            LinearLayout linearLayout = new LinearLayout()
            {
                Orientation = Orientation.Horizontal
            };

            foreach (AdaptiveColumn column in columnSet.Columns)
            {
                LinearLayout.LayoutParameters columnLayoutParameters = new LinearLayout.LayoutParameters(0, ViewGroup.LayoutParameters.WrapContent, 1);
                (UIView columnView, Action<JObject> binder) = RenderColumn(columnLayoutParameters, column);

                if (binder != null)
                {
                    if (aggregateBinder == null)
                    {
                        aggregateBinder = binder;
                    }
                    else
                    {
                        aggregateBinder += binder;
                    }
                }

                columnView.SetLayoutParameters(columnLayoutParameters);

                linearLayout.AddSubview(columnView);
            }

            return (linearLayout, aggregateBinder);
        }

        public virtual (UIView View, Action<JObject> Binder) RenderColumn(ViewGroup.LayoutParameters layoutParameters, AdaptiveColumn column)
        {
            Action<JObject> aggregateBinder = null;

            LinearLayout containerView = new LinearLayout()
            {
                Orientation = Orientation.Vertical
            };

            foreach (AdaptiveElement element in column.Items)
            {
                (UIView elementView, Action<JObject> binder) = RenderElement(new LinearLayout.LayoutParameters(ViewGroup.LayoutParameters.WrapContent, ViewGroup.LayoutParameters.WrapContent), element);

                if (binder != null)
                {
                    if (aggregateBinder == null)
                    {
                        aggregateBinder = binder;
                    }
                    else
                    {
                        aggregateBinder += binder;
                    }
                }

                containerView.AddSubview(elementView);
            }

            return (containerView, aggregateBinder);
        }

        public virtual UIView RenderFactSet(ViewGroup.LayoutParameters layoutParameters, AdaptiveFactSet factSet)
        {
            return null;
            /*
            TableLayout tableLayout = new TableLayout(context);

            foreach (AdaptiveFact fact in factSet.Facts)
            {
                TableLayout.LayoutParams factLayoutParameters = new TableLayout.LayoutParams(ViewGroup.LayoutParams.WrapContent, ViewGroup.LayoutParams.WrapContent);

                UIView factView = RenderFact(factLayoutParameters, fact);

                factView.LayoutParameters = factLayoutParameters;

                tableLayout.AddView(factView);
            }

            return tableLayout;
            */
        }

        public virtual UIView RenderFact(ViewGroup.LayoutParameters layoutParameters, AdaptiveFact fact)
        {
            return null;
            /*
            TableRow tableRow = new TableRow(context);

            UILabel textViewFactKey = new UILabel(context)
            {
                Text = fact.Title,
                LayoutParameters = new TableRow.LayoutParams(ViewGroup.LayoutParams.WrapContent, ViewGroup.LayoutParams.WrapContent)
                {
                    Span = 1
                }
            };
            TextView textViewFactValue = new TextView(context)
            {
                Text = fact.Value,
                LayoutParameters = new TableRow.LayoutParams(ViewGroup.LayoutParams.WrapContent, ViewGroup.LayoutParams.WrapContent)
                {
                    Span = 1
                }
            };
            textViewFactValue.SetTextAppearance(Resource.Style.TextAppearance_AppCompat_Body2);
            textViewFactValue.SetTextAppearance(Resource.Style.TextAppearance_AppCompat_Body1);

            tableRow.AddView(textViewFactKey);
            tableRow.AddView(textViewFactValue);

            return tableRow;
            */
        }

        public virtual UIView RenderUnhandledElement(ViewGroup.LayoutParameters layoutParameters, AdaptiveElement element)
        {
            return null;
        }

        public virtual UIView RenderActionView(ViewGroup.LayoutParameters layoutParameters, AdaptiveAction action)
        {
            Button actionView = new Button()
            {
                Text = action.Title
            };
            actionView.TitleLabel.Lines = 0;
            actionView.TitleLabel.LineBreakMode = UILineBreakMode.WordWrap;
            actionView.TitleLabel.TextAlignment = UITextAlignment.Center;

            actionView.SetLayoutParameters(layoutParameters);

            return actionView;
        }

        #region IDisposable Support
        private bool disposedValue = false; // To detect redundant calls

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    HttpClient.Dispose();
                }

                // TODO: free unmanaged resources (unmanaged objects) and override a finalizer below.
                // TODO: set large fields to null.

                disposedValue = true;
            }
        }

        // TODO: override a finalizer only if Dispose(bool disposing) above has code to free unmanaged resources.
        // ~AdaptiveCardRenderer()
        // {
        //   // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
        //   Dispose(false);
        // }

        // This code added to correctly implement the disposable pattern.
        public void Dispose()
        {
            // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
            Dispose(true);
            // TODO: uncomment the following line if the finalizer is overridden above.
            // GC.SuppressFinalize(this);
        }
        #endregion
    }
}