﻿using System.Linq;
using UIKit;
using Universal.Common.Extensions;
using Universal.iOS.Views;
using Universal.iOS.Widget;
using Universal.Microsoft.Cards.Hero;
using Action = Universal.Microsoft.Cards.Hero.Action;

namespace Universal.iOS.Microsoft.Cards
{
    /// <summary>
    /// Class that renders Microsoft Hero cards.
    /// </summary>
    public class HeroCardRenderer : ICardRenderer<HeroCard, Action>
    {
        /// <summary>
        /// Renders the given Hero card.
        /// </summary>
        /// <param name="heroCard"></param>
        /// <param name="actionHandler"></param>
        /// <returns></returns>
        public virtual RenderedCard<HeroCard> Render(HeroCard heroCard, ICardActionHandler<HeroCard, Action> actionHandler)
        {
            LinearLayout linearLayout = new LinearLayout()
            {
                Orientation = Orientation.Vertical
            };
            RenderedCard<HeroCard> result = new RenderedCard<HeroCard>(heroCard, linearLayout);

            if (!heroCard.Title.IsNullOrEmpty())
            {
                UIView titleView = RenderTitleView(heroCard.Title);
                linearLayout.AddSubview(titleView, new LinearLayout.LayoutParameters(ViewGroup.LayoutParameters.MatchParent, ViewGroup.LayoutParameters.WrapContent));
            }

            if (!heroCard.Subtitle.IsNullOrEmpty())
            {
                UIView subtitleView = RenderSubtitleView(heroCard.Subtitle);

                linearLayout.AddSubview(subtitleView, new LinearLayout.LayoutParameters(ViewGroup.LayoutParameters.MatchParent, ViewGroup.LayoutParameters.WrapContent));
            }

            if (!heroCard.Text.IsNullOrEmpty())
            {
                UIView textView = RenderTextView(heroCard.Text);

                linearLayout.AddSubview(textView, new LinearLayout.LayoutParameters(ViewGroup.LayoutParameters.MatchParent, ViewGroup.LayoutParameters.WrapContent));
            }

            if (heroCard.Buttons != null && heroCard.Buttons.Any())
            {
                LinearLayout buttonContainer = new LinearLayout()
                {
                    Orientation = Orientation.Vertical,
                    Gravity = GravityFlags.CenterHorizontal
                };

                foreach (Action action in heroCard.Buttons)
                {
                    UIView actionView = RenderActionView(action);
                    actionView.AddGestureRecognizer(new UITapGestureRecognizer(() =>
                    {
                        actionHandler?.OnAction(result, action);
                    }));

                    buttonContainer.AddSubview(actionView, new LinearLayout.LayoutParameters(ViewGroup.LayoutParameters.MatchParent, ViewGroup.LayoutParameters.WrapContent));
                }

                linearLayout.AddSubview(buttonContainer, new LinearLayout.LayoutParameters(ViewGroup.LayoutParameters.MatchParent, ViewGroup.LayoutParameters.WrapContent));
            }

            return result;
        }

        protected virtual UIView RenderTitleView(string title)
        {
            UILabel titleTextView = new UILabel();
            titleTextView.Font = UIFont.PreferredHeadline;
            titleTextView.Text = title;

            return titleTextView;
        }

        protected virtual UIView RenderSubtitleView(string subtitle)
        {
            UILabel subtitleView = new UILabel();
            subtitleView.Font = UIFont.PreferredSubheadline;
            subtitleView.Text = subtitle;

            return subtitleView;
        }

        protected virtual UIView RenderTextView(string text)
        {
            UILabel textView = new TextView();
            textView.Font = UIFont.PreferredBody;
            textView.Text = text;

            return textView;
        }

        protected virtual UIView RenderActionView(Action action)
        {
            UIButton actionView = new Button()
            {
                Text = action.Title
            };
            actionView.TitleLabel.Lines = 0;
            actionView.TitleLabel.LineBreakMode = UILineBreakMode.WordWrap;
            actionView.TitleLabel.TextAlignment = UITextAlignment.Center;

            return actionView;
        }
    }
}