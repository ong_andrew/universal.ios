﻿using AdaptiveCards;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using UIKit;

namespace Universal.iOS.Microsoft.Cards
{
    public class RenderedAdaptiveCard : RenderedCard<AdaptiveCard>
    {
        internal RenderedAdaptiveCard ParentCard { get; set; }
        internal Dictionary<AdaptiveShowCardAction, RenderedAdaptiveCard> ShowCardActionToCardMapping { get; set; }
        internal Action<JObject> InputBinders;

        public RenderedAdaptiveCard(AdaptiveCard card, UIView view) : base(card, view)
        {
            ShowCardActionToCardMapping = new Dictionary<AdaptiveShowCardAction, RenderedAdaptiveCard>();
        }
    }
}