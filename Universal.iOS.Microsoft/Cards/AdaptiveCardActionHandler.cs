﻿using System;

namespace Universal.iOS.Microsoft.Cards
{
    public class AdaptiveCardActionHandler<TCard, TAction> : CardActionHandler<TCard, TAction>, IAdaptiveCardActionHandler<TCard, TAction>
    {
        public event EventHandler<SubmitEventArgs<TCard, TAction>> Submit;
        public event EventHandler<ShowCardEventArgs<TCard, TAction>> ShowCard;
        public event EventHandler<OpenUrlEventArgs<TCard, TAction>> OpenUrl;

        public void OnOpenUrl(RenderedCard<TCard> renderedCard, TAction action, string url)
        {
            OpenUrl?.Invoke(this, new OpenUrlEventArgs<TCard, TAction>(renderedCard, action, url));
        }

        public void OnShowCard(RenderedCard<TCard> renderedCard, TAction action, RenderedCard<TCard> childCard)
        {
            ShowCard?.Invoke(this, new ShowCardEventArgs<TCard, TAction>(renderedCard, action, childCard));
        }

        public void OnSubmit(RenderedCard<TCard> renderedCard, TAction action, object value)
        {
            Submit?.Invoke(this, new SubmitEventArgs<TCard, TAction>(renderedCard, action, value));
        }

        public class OpenUrlEventArgs<TCard, TAction> : CardActionHandler<TCard, TAction>.EventArgs<TCard, TAction>
        {
            public string Url { get; protected set; }

            public OpenUrlEventArgs(RenderedCard<TCard> renderedCard, TAction action, string url) : base(renderedCard, action) { Url = url; }
        }

        public class SubmitEventArgs<TCard, TAction> : CardActionHandler<TCard, TAction>.EventArgs<TCard, TAction>
        {
            public object Value { get; protected set; }

            public SubmitEventArgs(RenderedCard<TCard> renderedCard, TAction action, object value) : base(renderedCard, action) { Value = value; }
        }

        public class ShowCardEventArgs<TCard, TAction> : CardActionHandler<TCard, TAction>.EventArgs<TCard, TAction>
        {
            public RenderedCard<TCard> ChildCard { get; protected set; }

            public ShowCardEventArgs(RenderedCard<TCard> renderedCard, TAction action, RenderedCard<TCard> childCard) : base(renderedCard, action) { ChildCard = childCard; }
        }
    }
}